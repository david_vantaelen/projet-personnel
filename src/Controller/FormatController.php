<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\FormatDTO;
use App\Entity\Format;
use App\Form\FormatEntityType;
use App\Form\Model\FormatFormModel;
use App\Service\FormatService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;


class FormatController extends BaseController
{
    /**
     * FormatController constructor.
     * @param ValidatorInterface $validator
     * @param FormatService $service
     */
    public function __construct(
        ValidatorInterface $validator,
        FormatService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/format",
     *      summary="Get all formats",
     *      description="Get all formats",
     *      tags={"Format"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="Format list",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#components/schemas/FormatDTO"),
     *              example={{ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "format": "Paper" }, { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "format": "Ebook" }}
     *          )
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/format")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return FormatDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function(Format $format) {
                return new FormatDTO($format);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/format/{id}",
     *     summary="Get one format by id",
     *     description="Get one format by id",
     *     tags={"Format"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Format",
     *          @OA\JsonContent(
     *              ref="#components/schemas/FormatDTO",
     *              example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "format": "Ebook" }
     *          )
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/format/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return FormatDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {
            /** @var Format $format */
            $format = $this->getService()->getOneById($id);
            return $format ? new FormatDTO($format) : null;

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/has_format/{format}",
     *     summary="Check if has format",
     *     description="Check if has format",
     *     tags={"Format"},
     *     @OA\Parameter(
     *         name="format",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Format",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="format",
     *                  type="boolean",
     *                  example="true"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/has_format/{format}")
     * @Rest\View()
     * @param string $format
     * @return array
     */
    public function hasEmail(string $format): array
    {
        try {
            return ["format" => $this->getService()->hasFormat($format)];

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/format",
     *     summary="Add a format",
     *     description="Add a format",
     *     tags={"Format"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/FormatForm"),
     *             example={"format": "Audio"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Format",
     *        @OA\JsonContent(
     *             ref="#components/schemas/FormatDTO",
     *             example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "format": "Ebook" }
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/format")
     * @Rest\View()
     * @param Request $request
     * @return FormatDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try {
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Format $format */
                    $format = $this->getService()->post($data);
                    return new FormatDTO($format);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/format/{id}",
     *     summary="Edit a format",
     *     description="Edit a format",
     *     tags={"Format"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/FormatForm"),
     *             example={"format": "Audio"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Format",
     *        @OA\JsonContent(
     *            ref="#components/schemas/FormatDTO",
     *            example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": "2021-06-03T21:44:20+02:00", "id": 2, "format": "Audio" }
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Put(path="api/format/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return FormatDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Format $format */
            $format = $this->getService()->getOneById($id);
            if(!$format) throw new Exception("No Format (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("format", $data)) $data["format"] = $format->getFormat();

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Format $format */
                    $format = $this->getService()->put($id, $data);
                    return new FormatDTO($format);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }

        return null;
    }

    /**
     * @OA\Delete(
     *     path="/api/format/{id}",
     *     summary="Delete a format",
     *     description="Delete a format",
     *     tags={"Format"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Format (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/format/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Format (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return FormatEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return FormatFormModel::class;
    }
}

<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\CommandDetailsDTO;
use App\DTO\CommandDTO;
use App\DTO\CommandEditionDTO;
use App\Entity\Command;
use App\Entity\CommandEdition;
use App\Form\CommandEditionEntityType;
use App\Form\CommandEntityType;
use App\Form\Model\CommandEditionFormModel;
use App\Form\Model\CommandFormModel;
use App\Service\CommandService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;

class CommandController extends BaseController
{
    public function __construct(
        ValidatorInterface $validator,
        CommandService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/command",
     *      summary="Get all commands",
     *      description="Get all commands",
     *      tags={"Command"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="Command list",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#components/schemas/CommandDTO"), example={{"created_at":"2020-05-10T05:03:36+02:00","updated_at":null,"id":1,"status":"READY"}, {"created_at":"2020-05-22T15:12:14+02:00","updated_at":null,"id":2,"status":"PENDING"}})
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/command")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return CommandDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function(Command $command) {
                return new CommandDTO($command);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/command/{id}",
     *     summary="Get one command by id",
     *     description="Get one command by id",
     *     tags={"Command"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Command",
     *          @OA\JsonContent(ref="#components/schemas/CommandDetailsDTO", example={"created_at":"2020-05-22T15:12:14+02:00","updated_at":null,"id":2,"status":"PENDING","user":{"created_at":"2020-05-22T15:12:13+02:00","updated_at":null,"id":8,"first_name":"Paul","last_name":"Albert","username":"user","email":"begue.maggie@sfr.fr","password":"$argon2i$v=19$m=65536,t=5,p=1$Y2NZSUFadFdXcnNsL09kbA$CsBVc\/x3\/QsMIAVeIZ8uF5gWmZxUshIomlq1hKf+oxU","roles":{"ROLE_USER","ROLE_DELIVERY","ROLE_ADMIN"},"group_name":"GRP_DELIVERY"},"financial":{"created_at":"2020-05-22T15:12:12+02:00","updated_at":null,"id":8,"name":"PayPal"},"courier":{"created_at":"2020-05-22T15:12:12+02:00","updated_at":null,"id":4,"name":"DHL"},"command_editions":{{"command":{"created_at":"2020-05-22T15:12:14+02:00","updated_at":null,"id":2,"status":"PENDING"},"edition":{"created_at":"2020-05-22T15:12:12+02:00","updated_at":null,"id":13,"title":"Python pour les Kids","author":"Jason R. Briggs","description":"Apprendre à programmer","price":20.9,"image_path":"assets\/uploads\/img1.jpg","stock":1,"release_at":"2015-03-19T00:00:00+01:00"},"price":209,"quantity":10}}})
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/command/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return CommandDetailsDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {
            /** @var Command $command */
            $command = $this->getService()->getOneById($id);
            if(!$command) return null;

            $commandEditions = $this->getService()->getCommandEditions($id);

            return new CommandDetailsDTO($command, array_map(function($commandEdition){
                /** @var CommandEdition $commandEdition */
                return new CommandEditionDTO($commandEdition);
            }, $commandEditions));

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/command",
     *     summary="Add a command",
     *     description="Add a command",
     *     tags={"Command"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CommandForm"),
     *             example={"userId":1, "courierId":3, "financialId":2, "status":"PENDING"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Command",
     *        @OA\JsonContent(ref="#components/schemas/CommandDTO", example={"created_at":"2020-05-10T05:03:36+02:00","updated_at":null,"id":1,"status":"PENDING"})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_USER")
     * @Rest\Post(path="api/command")
     * @Rest\View()
     * @param Request $request
     * @return CommandDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try {
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Command $command */
                    $command = $this->getService()->post($data);
                    return new CommandDTO($command);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }

    }

    /**
     * @OA\Put(
     *     path="/api/command/{id}",
     *     summary="Edit a command",
     *     description="Edit a command",
     *     tags={"Command"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CommandForm"),
     *             example={"userId":1, "courierId":3, "financialId":2, "status":"READY"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Command",
     *        @OA\JsonContent(ref="#components/schemas/CommandDTO", example={"created_at":"2020-05-10T05:03:36+02:00","updated_at":"2020-05-12T12:08:02+02:00","id":1,"status":"READY"})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_USER")
     * @Rest\Put(path="api/command/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return CommandDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Command $command */
            $command = $this->getService()->getOneById($id);
            if(!$command)  throw new Exception("No Command (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("userId", $data)) $data["userId"] = $command->getUser()->getId();
            if(!key_exists("financialId", $data)) $data["financialId"] = $command->getFinancial()->getId();
            if(!key_exists("courierId", $data)) $data["courierId"] = $command->getCourier() ? $command->getCourier()->getId() : null;

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Command $command */
                    $command =  $this->getService()->put($id, $data);
                    return new CommandDTO($command);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }

    }

    /**
     * @OA\Patch(
     *     path="/api/command/{id}/edition/{edition_id}",
     *     summary="Add an edition to a command",
     *     description="Add an edition to a command",
     *     tags={"Command"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="edition_id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CommandEditionForm"),
     *             example={"price": 3.95, "quantity": 1}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Command",
     *        @OA\JsonContent(ref="#components/schemas/CommandEditionDTO", example={"command":{"created_at":"2020-05-22T15:12:14+02:00","updated_at":null,"id":2,"status":"PENDING"},"edition":{"created_at":"2020-05-22T15:12:12+02:00","updated_at":null,"id":13,"title":"Python pour les Kids","author":"Jason R. Briggs","description":"Apprendre à programmer","price":20.9,"image_path":"assets/uploads/img1.jpg","stock":1,"release_at":"2015-03-19T00:00:00+01:00"},"price":209,"quantity":10})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_USER")
     * @Rest\Patch(path="api/command/{id<\d+>}/edition/{edition_id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return CommandEditionDTO
     */
    public function addEditionAction(Request $request): CommandEditionDTO
    {
        try {
            $id = $request->get("id");
            $edition_id = $request->get("edition_id");

            $form = $this->createForm(CommandEditionEntityType::class, new CommandEditionFormModel(), [
                "csrf_protection" => false
            ]);

            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()) {
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()) {
                    /** @var CommandEdition $commandEdition */
                    $commandEdition = $this->getService()->addEdition($id, $edition_id, $form->getData());
                    return new CommandEditionDTO($commandEdition);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            }

        } catch(Exception $e) {
            $this->jsonError($e);

        }

    }

    /**
     * @OA\Put(
     *     path="/api/command/{id}/edition/{edition_id}",
     *     summary="Edit an edition from a command",
     *     description="Edit an edition from a command",
     *     tags={"Command"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="edition_id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CommandEditionForm"),
     *             example={"price": 3.95, "quantity": 1}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Command",
     *        @OA\JsonContent(ref="#components/schemas/CommandEditionDTO", example={"command":{"created_at":"2020-05-22T15:12:14+02:00","updated_at":null,"id":2,"status":"PENDING"},"edition":{"created_at":"2020-05-22T15:12:12+02:00","updated_at":null,"id":13,"title":"Python pour les Kids","author":"Jason R. Briggs","description":"Apprendre à programmer","price":20.9,"image_path":"assets/uploads/img1.jpg","stock":1,"release_at":"2015-03-19T00:00:00+01:00"},"price":209,"quantity":10})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_USER")
     * @Rest\Put(path="api/command/{id<\d+>}/edition/{edition_id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return CommandEditionDTO
     */
    public function editEditionAction(Request $request): CommandEditionDTO
    {
        try {
            $id = $request->get("id");
            $edition_id = $request->get("edition_id");

            /** @var CommandEdition $commandEdition */
            $commandEdition = $this->getService()->getOneCommandEdition($id, $edition_id);
            if(!$commandEdition) throw new Exception("No Edition (id#$edition_id) in Command (id#$id) found");

            $form = $this->createForm(CommandEditionEntityType::class, new CommandEditionFormModel(), [
                "csrf_protection" => false
            ]);

            $data = json_decode($request->getContent(), true);

            if(!key_exists("quantity", $data)) $data["quantity"] = $commandEdition->getQuantity();
            if(!key_exists("price", $data))  $data["price"] = $commandEdition->getPrice();

            $form->submit($data);
            $form->handleRequest($request);
            if($form->isSubmitted()) {
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()) {
                    $commandEdition = $this->getService()->editEdition($id, $edition_id, $form->getData());
                    return new CommandEditionDTO($commandEdition);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            }

        } catch(Exception $e) {
            $this->jsonError($e);

        }

    }

    /**
     * @OA\Delete(
     *     path="/api/command/{id}/edition/{edition_id}",
     *     summary="Remove an edition from a command",
     *     description="Remove an edition from a command",
     *     tags={"Command"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="edition_id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Edition (id#5) has been removed from Command (id#6)")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_USER")
     * @Rest\Delete(path="api/command/{id<\d+>}/edition/{edition_id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @param int $edition_id > 0
     * @return JsonResponse
     */
    public function removeEditionAction(int $id, int $edition_id): JsonResponse
    {
        try{
            $this->getService()->removeEdition($id, $edition_id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Edition (id#$edition_id) has been removed from Command (id#$id)"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Delete(
     *     path="/api/command/{id}",
     *     summary="Delete a command",
     *     description="Delete a command",
     *     tags={"Command"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Command (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/command/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Command (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return CommandEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return CommandFormModel::class;
    }
}

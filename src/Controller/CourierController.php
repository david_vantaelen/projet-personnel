<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\CourierDTO;
use App\Entity\Courier;
use App\Form\CourierEntityType;
use App\Form\Model\CourierFormModel;
use App\Service\CourierService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;


class CourierController extends BaseController
{
    /**
     * CourierController constructor.
     * @param ValidatorInterface $validator
     * @param CourierService $service
     */
    public function __construct(
        ValidatorInterface $validator,
        CourierService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/courier",
     *      summary="Get all couriers",
     *      description="Get all couriers",
     *      tags={"Courier"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="Courier list",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#components/schemas/CourierDTO"),
     *              example={ { "created_at": "2020-06-19T11:25:07+02:00", "updated_at": null, "id": 1, "name": "DHL", "description": "[[p]]Livraison par [[b]]`DHL`[[/b]][[/p]]", "image": "dhl.png" }, { "created_at": "2020-06-19T11:25:07+02:00", "updated_at": null, "id": 2, "name": "GLS", "description": "[[p]]Livraison par [[b]]`GLS`[[/b]][[/p]]", "image": "gls.png" }, { "created_at": "2020-06-19T11:25:07+02:00", "updated_at": null, "id": 3, "name": "UPS", "description": "[[p]]Livraison par [[b]]`UPS`[[/b]][[/p]]", "image": "ups.png" } }
     *          )
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/courier")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return CourierDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function(Courier $courier) {
                return new CourierDTO($courier);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/courier/{id}",
     *     summary="Get one courier by id",
     *     description="Get one courier by id",
     *     tags={"Courier"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Courier",
     *          @OA\JsonContent(
     *              ref="#components/schemas/CourierDTO",
     *              example={"created_at":"2020-06-19T11:25:07+02:00","updated_at":null,"id":2,"name":"GLS","description":"[[p]]Livraison par [[b]]`GLS`[[\/b]][[\/p]]","image":"gls.png"}
     *          )
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/courier/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return CourierDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {
            /** @var Courier $courier */
            $courier = $this->getService()->getOneById($id);
            return $courier ? new CourierDTO($courier) : null;

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/courier",
     *     summary="Add a courier",
     *     description="Add a courier",
     *     tags={"Courier"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CourierForm"),
     *             example={"name":"Transport","description":"[[p]]Livraison par [[b]]`Transport`[[\/b]][[\/p]]","image":"transport.png"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Courier",
     *        @OA\JsonContent(
     *             ref="#components/schemas/CourierDTO",
     *             example={"created_at":"2020-06-19T11:30:08+02:00","updated_at":null,"id":4,"name":"Transport","description":"[[p]]Livraison par [[b]]`Transport`[[\/b]][[\/p]]","image":"transport.png"}
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/courier")
     * @Rest\View()
     * @param Request $request
     * @return CourierDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try {
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Courier $courier */
                    $courier = $this->getService()->post($data);
                    return new CourierDTO($courier);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/courier/{id}",
     *     summary="Edit a courier",
     *     description="Edit a courier",
     *     tags={"Courier"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CourierForm"),
     *             example={"name":"Transport","description":"[[p]]Livraison par [[b]]`Transport`[[\/b]][[\/p]]","image":"transport.png"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Courier",
     *        @OA\JsonContent(
     *            ref="#components/schemas/CourierDTO",
     *            example={"created_at":"2020-06-19T11:30:08+02:00","updated_at":"2020-06-19T11:38:02+02:00","id":4,"name":"Transport","description":"[[p]]Livraison par [[b]]`Transport`[[\/b]][[\/p]]","image":"transport.png"}
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Put(path="api/courier/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return CourierDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Courier $courier */
            $courier = $this->getService()->getOneById($id);
            if(!$courier) throw new Exception("No Courier (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("name", $data)) $data["name"] = $courier->getName();
            if(!key_exists("description", $data)) $data["description"] = $courier->getDescription();
            if(!key_exists("image", $data)) $data["image"] = $courier->getImage();

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Courier $courier */
                    $courier = $this->getService()->put($id, $data);
                    return new CourierDTO($courier);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }

        return null;
    }

    /**
     * @OA\Delete(
     *     path="/api/courier/{id}",
     *     summary="Delete a courier",
     *     description="Delete a courier",
     *     tags={"Courier"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Courier (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/courier/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Courier (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return CourierEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return CourierFormModel::class;
    }
}

<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\EditionDetailsDTO;
use App\DTO\EditionDTO;
use App\DTO\PageDTO;
use App\Entity\Edition;
use App\Form\EditionImageType;
use App\Form\EditionEntityType;
use App\Form\Model\EditionFormModel;
use App\Form\Model\EditionImageFormModel;
use App\Service\EditionService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;

class EditionController extends BaseController
{
    public function __construct(
        ValidatorInterface $validator,
        EditionService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/edition",
     *      summary="Get all editions",
     *      description="Get all editions",
     *      tags={"Edition"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Parameter(
     *          name="search",
     *          in="query",
     *          description=null,
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Edition list",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#components/schemas/EditionDTO"), example={{"created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 21, "title": "Nouvelle star : la méthode pour apprendre à chanter", "author": "Anne Peko", "description": "méthodes de chants", "price": 6.92, "image_path": "assets/uploads/img9.jpg", "stock": 1, "release_at": "2008-11-01T00:00:00+01:00" }, { "created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 22, "title": "Plus belle la vie Tome 5 L'inconnu du Mistral", "author": "Claude Lambesc", "description": "super livre", "price": 13, "image_path": "assets/uploads/img10.jpg", "stock": 1, "release_at": "2009-03-12T00:00:00+01:00"}})
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/edition")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="search", requirements="(\w|\s|\d)+")
     * @param ParamFetcherInterface $paramFetcher
     * @return EditionDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");
            $search = $paramFetcher->get("search");

            $data = $search ? $this->getService()->getSearch($search, $limit, $offset) : $this->getService()->getAll($limit, $offset);

            return array_map(function($edition) {
                /** @var Edition $edition */
                return new EditionDTO($edition);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *      path="/api/edition/page",
     *      summary="Get a page of editions",
     *      description="Get a page of editions",
     *      tags={"Edition"},
     *      @OA\Parameter(ref="#/components/parameters/page"),
     *      @OA\Parameter(ref="#/components/parameters/perPage"),
     *      @OA\Parameter(
     *          name="search",
     *          in="query",
     *          description=null,
     *          @OA\Schema(type="string")
     *      ),
     *      @OA\Response(
     *          response="200",
     *          description="Edition list",
     *          @OA\JsonContent(
     *              @OA\Property(type="array", property="data", @OA\Items(ref="#components/schemas/EditionDTO")),
     *              allOf={@OA\Schema(ref="#/components/schemas/PageDTO")},
     *              example={ "perPage": 2, "nbPages": 25, "page": 10, "count": 2, "total": 50, "data": { { "created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 21, "title": "Nouvelle star : la méthode pour apprendre à chanter", "author": "Anne Peko", "description": "méthodes de chants", "price": 6.92, "image_path": "assets/uploads/img9.jpg", "stock": 1, "release_at": "2008-11-01T00:00:00+01:00" }, { "created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 22, "title": "Plus belle la vie Tome 5 L'inconnu du Mistral", "author": "Claude Lambesc", "description": "super livre", "price": 13, "image_path": "assets/uploads/img10.jpg", "stock": 1, "release_at": "2009-03-12T00:00:00+01:00" } } }
     *          ),
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/edition/page")
     * @Rest\View()
     * @Rest\QueryParam(name="page", requirements="\d+", nullable=true, default=1)
     * @Rest\QueryParam(name="perPage", requirements="\d+", nullable=true, default=20)
     * @Rest\QueryParam(name="search", requirements="(\w|\s|\d)+")
     * @param ParamFetcherInterface $paramFetcher
     * @throws \InvalidArgumentException | $page < 1
     * @return PageDTO
     */
    public function getPageAction(ParamFetcherInterface $paramFetcher): PageDTO
    {
        try {
            $page = $paramFetcher->get("page");
            $perPage = $paramFetcher->get("perPage");
            $search = $paramFetcher->get("search");
            $total = $search ? $this->getService()->getCountBySearch($search) : $this->getService()->getCount();

            if($page < 1) throw new \InvalidArgumentException("Page begins by 1 and cannot be lower");

            $data = $search ? $this->getService()->getSearch($search, $perPage, ($page - 1) * $perPage) : $this->getService()->getAll($perPage, ($page - 1) * $perPage);
            $data = array_map(function($edition) {
                /** @var Edition $edition */
                return new EditionDTO($edition);
            }, $data);

            return new PageDTO($data, $perPage, $page, count($data), $total);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/edition/{id}",
     *     summary="Get one edition by id",
     *     description="Get one edition by id",
     *     tags={"Edition"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Edition",
     *          @OA\JsonContent(ref="#components/schemas/EditionDetailsDTO", example={"created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 13, "title": "Python pour les Kids", "author": "Jason R. Briggs", "description": "Apprendre à programmer", "price": 20.9, "image_path": "assets/uploads/img1.jpg", "stock": 1, "release_at": "2015-03-19T00:00:00+01:00", "book": { "created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 13, "title": "Python pour les Kids", "author_name": "Jason R. Briggs" }, "format": { "created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 3, "format": "Book" }, "editor": { "created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 11, "editor": "Librairie Eyerolles"}, "collection": {"created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 6, "collection": "Pour les Kids"}})
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/edition/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return EditionDetailsDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {
            /** @var Edition $edition */
            $edition = $this->getService()->getOneById($id);
            return $edition ? new EditionDetailsDTO($edition) : null;

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/edition",
     *     summary="Add an edition",
     *     description="Add an edition",
     *     tags={"Edition"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(ref="#/components/schemas/EditionForm")
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Edition",
     *        @OA\JsonContent(ref="#components/schemas/EditionDTO", example={"created_at": "2020-05-22T15:12:12+02:00", "updated_at": null, "id": 2, "title": "Nouvelle star : la méthode pour apprendre à chanter", "author": "Anne Peko", "description": "méthodes de chants", "price": 6.92, "image_path": "assets/uploads/img9.jpg", "stock": 1, "release_at": "2008-11-01T00:00:00+01:00"})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/edition")
     * @Rest\View()
     * @param Request $request
     * @return EditionDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try {
            $form = $this->_createForm();
            $data = $request->request->all();
            $data["image"] = $request->files->get("image");
            $form->submit($data);
            $form->handleRequest($request);
            if($form->isSubmitted()){
                /** @var EditionFormModel $data */
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);
                if(!count($errors) && $form->isValid()){
                    /** @var Edition $edition */
                    $edition = $this->getService()->post($data);
                    return new EditionDTO($edition);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/edition/{id}",
     *     summary="Edit an edition",
     *     description="Edit an edition",
     *     tags={"Edition"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/EditionForm"),
     *             example={"description": "Une description", "price": 20.9, "stock": 1, "releaseAt": "2015-03-19","bookId": 1, "formatId": 1, "editorId": 1, "collectionId": 1}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Edition",
     *        @OA\JsonContent(ref="#components/schemas/EditionDTO", example={"created_at": "2020-05-22T15:12:12+02:00", "updated_at": "2020-03-02T18:07:46+02:00", "id": 2, "title": "Nouvelle star : la méthode pour apprendre à chanter", "author": "Anne Peko", "description": "méthodes de chants", "price": 6.92, "image_path": "assets/uploads/img9.jpg", "stock": 1, "release_at": "2008-11-01T00:00:00+01:00"})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_USER")
     * @Rest\Put(path="api/edition/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return EditionDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Edition $edition */
            $edition = $this->getService()->getOneById($id);
            if(!$edition) throw new Exception("No Edition (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("description", $data)) $data["description"] = $edition->getDescription();
            if(!key_exists("stock", $data)) $data["stock"] = $edition->getStock();
            if(!key_exists("price", $data)) $data["price"] = $edition->getPrice();
            if(!key_exists("releaseAt", $data)) $data["releaseAt"] = $edition->getReleaseAt() ? $edition->getReleaseAt()->format("Y-m-d") : null;
            if(!key_exists("bookId", $data)) $data["bookId"] = $edition->getBook()->getId();
            if(!key_exists("editorId", $data)) $data["editorId"] = $edition->getEditor()->getId();
            if(!key_exists("formatId", $data)) $data["formatId"] = $edition->getFormat()->getId();
            if(!key_exists("collectionId", $data)) $data["collectionId"] = $edition->getCollection() ? $edition->getCollection()->getId() : null;

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Edition $edition */
                    $edition = $this->getService()->put($id, $data);
                    return new EditionDTO($edition);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/edition/{id}/image",
     *     summary="Change image on an edition",
     *     description="Change image on an edition",
     *     tags={"Edition"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(ref="#/components/schemas/EditionImageFormModel")
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Edition",
     *        @OA\JsonContent(ref="#components/schemas/EditionDTO", example={"created_at": "2020-05-22T15:12:12+02:00", "updated_at": "2020-03-02T18:07:46+02:00", "id": 2, "title": "Nouvelle star : la méthode pour apprendre à chanter", "author": "Anne Peko", "description": "méthodes de chants", "price": 6.92, "image_path": "assets/uploads/img9.jpg", "stock": 1, "release_at": "2008-11-01T00:00:00+01:00"})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/edition/{id<\d+>}/image")
     * @Rest\View()
     * @param Request $request
     * @return EditionDTO
     */
    public function changeImageAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Edition $edition */
            $edition = $this->getService()->getOneById($id);
            if(!$edition) throw new Exception("No Edition (id#$id)");

            $form = $this->createForm(EditionImageType::class, new EditionImageFormModel(), ["csrf_protection" => false]);
            $data = [];

            if(!$request->files->get("image")) throw new Exception("No Content");

            $data["image"] = $request->files->get("image");

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Edition $edition */
                    $edition = $this->getService()->changeImage($id, $data);
                    return new EditionDTO($edition);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Delete(
     *     path="/api/edition/{id}",
     *     summary="Delete an edition",
     *     description="Delete an edition",
     *     tags={"Edition"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Edition (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/edition/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Edition (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return EditionEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return EditionFormModel::class;
    }
}

<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\EditorDTO;
use App\Entity\Editor;
use App\Form\EditorEntityType;
use App\Form\Model\EditorFormModel;
use App\Service\EditorService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;


class EditorController extends BaseController
{
    /**
     * EditorController constructor.
     * @param ValidatorInterface $validator
     * @param EditorService $service
     */
    public function __construct(
        ValidatorInterface $validator,
        EditorService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/editor",
     *      summary="Get all editors",
     *      description="Get all editors",
     *      tags={"Editor"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="Editor list",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#components/schemas/EditorDTO"),
     *              example={{ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "editor": "Editor #1" }, { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "editor": "Editor #2" }}
     *          )
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/editor")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return EditorDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function(Editor $editor) {
                return new EditorDTO($editor);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/editor/{id}",
     *     summary="Get one editor by id",
     *     description="Get one editor by id",
     *     tags={"Editor"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Editor",
     *          @OA\JsonContent(
     *              ref="#components/schemas/EditorDTO",
     *              example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "editor": "Editor #2" }
     *          )
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/editor/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return EditorDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {
            /** @var Editor $editor */
            $editor = $this->getService()->getOneById($id);
            return $editor ? new EditorDTO($editor) : null;

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/editor",
     *     summary="Add a editor",
     *     description="Add a editor",
     *     tags={"Editor"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/EditorForm"),
     *             example={"editor": "Editor #3"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Editor",
     *        @OA\JsonContent(
     *             ref="#components/schemas/EditorDTO",
     *             example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "editor": "Editor #2" }
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/editor")
     * @Rest\View()
     * @param Request $request
     * @return EditorDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try {
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Editor $editor */
                    $editor = $this->getService()->post($data);
                    return new EditorDTO($editor);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/editor/{id}",
     *     summary="Edit a editor",
     *     description="Edit a editor",
     *     tags={"Editor"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/EditorForm"),
     *             example={"editor": "Editor #3"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Editor",
     *        @OA\JsonContent(
     *            ref="#components/schemas/EditorDTO",
     *            example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": "2021-06-03T21:44:20+02:00", "id": 2, "editor": "Editor #3" }
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Put(path="api/editor/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return EditorDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Editor $editor */
            $editor = $this->getService()->getOneById($id);
            if(!$editor) throw new Exception("No Editor (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("editor", $data)) $data["editor"] = $editor->getEditor();

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Editor $editor */
                    $editor = $this->getService()->put($id, $data);
                    return new EditorDTO($editor);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }

        return null;
    }

    /**
     * @OA\Delete(
     *     path="/api/editor/{id}",
     *     summary="Delete a editor",
     *     description="Delete a editor",
     *     tags={"Editor"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Editor (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/editor/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Editor (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return EditorEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return EditorFormModel::class;
    }
}

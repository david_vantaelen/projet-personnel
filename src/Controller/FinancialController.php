<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\FinancialDTO;
use App\Entity\Financial;
use App\Form\FinancialEntityType;
use App\Form\Model\FinancialFormModel;
use App\Service\FinancialService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;


class FinancialController extends BaseController
{
    /**
     * FinancialController constructor.
     * @param ValidatorInterface $validator
     * @param FinancialService $service
     */
    public function __construct(
        ValidatorInterface $validator,
        FinancialService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/financial",
     *      summary="Get all financials",
     *      description="Get all financials",
     *      tags={"Financial"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="Financial list",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#components/schemas/FinancialDTO"),
     *              example={ { "created_at": "2020-06-19T11:25:07+02:00", "updated_at": null, "id": 1, "name": "VISA", "description": "[[p]]Paiement par le moyen de [[b]]`VISA`[[/b]][[/p]]", "image": "visa.png" }, { "created_at": "2020-06-19T11:25:07+02:00", "updated_at": null, "id": 2, "name": "MasterCard", "description": "[[p]]Paiement par le moyen de [[b]]`MasterCard`[[/b]][[/p]]", "image": "mastercard.png" }, { "created_at": "2020-06-19T11:25:07+02:00", "updated_at": null, "id": 3, "name": "Virement Bancaire", "description": "[[p]]Paiement par le moyen de [[b]]`Virement Bancaire`[[/b]][[/p]]", "image": "virement_bancaire.png" }, { "created_at": "2020-06-19T11:25:07+02:00", "updated_at": null, "id": 4, "name": "PayPal", "description": "[[p]]Paiement par le moyen de [[b]]`PayPal`[[/b]][[/p]]", "image": "paypal.png" } }
     *          )
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/financial")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return FinancialDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function(Financial $financial) {
                return new FinancialDTO($financial);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/financial/{id}",
     *     summary="Get one financial by id",
     *     description="Get one financial by id",
     *     tags={"Financial"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Financial",
     *          @OA\JsonContent(
     *              ref="#components/schemas/FinancialDTO",
     *              example={"created_at":"2020-06-19T11:25:07+02:00","updated_at":null,"id":2,"name":"MasterCard","description":"[[p]]Paiement par le moyen de [[b]]`MasterCard`[[\/b]][[\/p]]","image":"mastercard.png"}
     *          )
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/financial/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return FinancialDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {
            /** @var Financial $financial */
            $financial = $this->getService()->getOneById($id);
            return $financial ? new FinancialDTO($financial) : null;

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/financial",
     *     summary="Add a financial",
     *     description="Add a financial",
     *     tags={"Financial"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/FinancialForm"),
     *             example={"name": "Ogone", "description": "[[p]]Paiement par le moyen de [[b]]`Ogone`[[/b]][[/p]]", "image": "ogone.png"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Financial",
     *        @OA\JsonContent(
     *             ref="#components/schemas/FinancialDTO",
     *             example={"created_at":"2020-06-19T11:32:09+02:00","updated_at":null,"id":5,"name":"Ogone","description":"[[p]]Paiement par le moyen de [[b]]`Ogone`[[\/b]][[\/p]]","image":"ogone.png"}
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/financial")
     * @Rest\View()
     * @param Request $request
     * @return FinancialDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try {
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Financial $financial */
                    $financial = $this->getService()->post($data);
                    return new FinancialDTO($financial);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/financial/{id}",
     *     summary="Edit a financial",
     *     description="Edit a financial",
     *     tags={"Financial"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/FinancialForm"),
     *             example={"name": "Ogone", "description": "[[p]]Paiement par le moyen de [[b]]`Ogone`[[/b]][[/p]]", "image": "ogone.png"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Financial",
     *        @OA\JsonContent(
     *            ref="#components/schemas/FinancialDTO",
     *            example={"created_at":"2020-06-19T11:32:09+02:00","updated_at":"2020-06-19T11:40:29+02:00","id":5,"name":"Ogone","description":"[[p]]Paiement par le moyen de [[b]]`Ogone`[[\/b]][[\/p]]","image":"ogone.png"}
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Put(path="api/financial/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return FinancialDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Financial $financial */
            $financial = $this->getService()->getOneById($id);
            if(!$financial) throw new Exception("No Financial (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("name", $data)) $data["name"] = $financial->getName();
            if(!key_exists("description", $data)) $data["description"] = $financial->getDescription();
            if(!key_exists("image", $data)) $data["image"] = $financial->getImage();

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Financial $financial */
                    $financial = $this->getService()->put($id, $data);
                    return new FinancialDTO($financial);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }

        return null;
    }

    /**
     * @OA\Delete(
     *     path="/api/financial/{id}",
     *     summary="Delete a financial",
     *     description="Delete a financial",
     *     tags={"Financial"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Financial (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/financial/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Financial (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return FinancialEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return FinancialFormModel::class;
    }
}

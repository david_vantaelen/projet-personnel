<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\AuthorDTO;
use App\Entity\Author;
use App\Form\AuthorEntityType;
use App\Form\Model\AuthorFormModel;
use App\Service\AuthorService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;


class AuthorController extends BaseController
{
    /**
     * AuthorController constructor.
     * @param ValidatorInterface $validator
     * @param AuthorService $service
     */
    public function __construct(
        ValidatorInterface $validator,
        AuthorService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/author",
     *      summary="Get all authors",
     *      description="Get all authors",
     *      tags={"Author"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="Author list",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#components/schemas/AuthorDTO"), example={{"created_at": "2020-05-20T16:23:52+02:00", "updated_at": null, "id": 1, "first_name": "Maria", "last_name": "Carre"}, {"created_at": "2020-05-20T16:23:52+02:00", "updated_at": null, "id": 2, "first_name": "John", "last_name": "Doe"}})
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/author")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return AuthorDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function(Author $author) {
                return new AuthorDTO($author);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/author/{id}",
     *     summary="Get one author by id",
     *     description="Get one author by id",
     *     tags={"Author"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Author",
     *          @OA\JsonContent(ref="#components/schemas/AuthorDTO", example={"created_at": "2020-05-20T16:23:52+02:00", "updated_at": null, "id": 2, "first_name": "John", "last_name": "Doe"})
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/author/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return AuthorDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {
            /** @var Author $author */
            $author = $this->getService()->getOneById($id);
            return $author ? new AuthorDTO($author) : null;

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/author",
     *     summary="Add an author",
     *     description="Add an author",
     *     tags={"Author"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/AuthorForm"),
     *             example={"firstName": "John", "lastName": "Doe"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Author",
     *        @OA\JsonContent(ref="#components/schemas/AuthorDTO", example={"created_at": "2020-05-20T16:23:52+02:00", "updated_at": null, "id": 2, "first_name": "John", "last_name": "Doe"})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/author")
     * @Rest\View()
     * @param Request $request
     * @return AuthorDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try {
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Author $author */
                    $author = $this->getService()->post($data);
                    return new AuthorDTO($author);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/author/{id}",
     *     summary="Edit an author",
     *     description="Edit an author",
     *     tags={"Author"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/AuthorForm"),
     *             example={"firstName": "Jane", "lastName": "Darc"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Author",
     *        @OA\JsonContent(ref="#components/schemas/AuthorDTO", example={"created_at": "2020-05-20T16:23:52+02:00", "updated_at": "2020-05-20T18:10:30+02:00", "id": 2, "first_name": "Jane", "last_name": "Darc"})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Put(path="api/author/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return AuthorDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Author $author */
            $author = $this->getService()->getOneById($id);
            if(!$author) throw new Exception("No Author (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("firstName", $data)) $data["firstName"] = $author->getFirstName();
            if(!key_exists("lastName", $data)) $data["lastName"] = $author->getLastName();

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Author $author */
                    $author = $this->getService()->put($id, $data);
                    return new AuthorDTO($author);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }

        return null;
    }

    /**
     * @OA\Delete(
     *     path="/api/author/{id}",
     *     summary="Delete an author",
     *     description="Delete an author",
     *     tags={"Author"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Author (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/author/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Author (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return AuthorEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return AuthorFormModel::class;
    }
}

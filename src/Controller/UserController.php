<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\AddressDTO;
use App\DTO\UserDetailsDTO;
use App\DTO\UserDTO;
use App\DTO\UserRoleDTOForUser;
use App\Entity\Address;
use App\Entity\User;
use App\Entity\UserRole;
use App\Form\Model\PutUserFormModel;
use App\Form\Model\UserRoleFormModel;
use App\Form\PutUserEntityType;
use App\Form\UserRoleEntityType;
use App\Form\UserEntityType;
use App\Form\Model\UserFormModel;
use App\Service\UserService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;


class UserController extends BaseController
{
    private $encoder;

    public function __construct(
        ValidatorInterface $validator,
        UserService $service,
        UserPasswordEncoderInterface $encoder
    )
    {
        parent::__construct($validator, $service);
        $this->encoder = $encoder;
    }

    /**
     * @OA\Get(
     *      path="/api/user",
     *      summary="Get all users",
     *      description="Get all users",
     *      tags={"User"},
     *      security={{"bearerAuth":{}}},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="User list",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#components/schemas/UserDTO"), example={{ "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY" }, { "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 2, "first_name": "Laurent", "last_name": "Allard", "username": "suser", "email": "elodie77@gmail.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$L3lXOUVyNGk2NWMvMlBELw$1RfnrAncVuKtE/sM6sUkxdAHoBdjej0TEV/Vf/HyfiA", "roles": { "ROLE_USER" }, "group_name": "GRP_USER" }})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Get(path="api/user")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return UserDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function($user) {
                /** @var User $user */
                return new UserDTO($user);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/user/{id}",
     *     summary="Get one user by id",
     *     description="Get one user by id",
     *     tags={"User"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="User",
     *          @OA\JsonContent(ref="#components/schemas/UserDetailsDTO", example={ "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY", "addresses": {{"created_at": "2020-06-11T14:58:29+02:00", "updated_at": null, "id": 5, "street": "1, street", "city": "city", "post_code": "1000", "country": "country", "is_main": true }}, "group": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "GRP_DELIVERY" }, "user_roles": { { "role": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "label": "ROLE_USER" }, "begin_at": "2020-05-25T16:35:54+02:00", "end_at": null }, { "role": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "label": "ROLE_ADMIN" }, "begin_at": "2020-05-25T16:35:54+02:00", "end_at": null } } })
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Get(path="api/user/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return UserDetailsDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {
            /** @var User $user */
            $user = $this->getService()->getOneById($id);
            if(!$user) return null;
            $addresses = $this->getService()->getAddresses($id);
            $userRoles = $this->getService()->getUserRoles($id);

            return new UserDetailsDTO($user, array_map(function($address){
                /** @var Address $address */
                return new AddressDTO($address);
            }, $addresses), array_map(function($userRole){
                /** @var UserRole $userRole */
                return new UserRoleDTOForUser($userRole);
            }, $userRoles));

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/username/{username}",
     *     summary="Get one user by username",
     *     description="Get one user by username",
     *     tags={"User"},
     *     @OA\Parameter(
     *         name="username",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="User",
     *          @OA\JsonContent(ref="#components/schemas/UserDetailsDTO", example={ "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY", "addresses": {{"created_at": "2020-06-11T14:58:29+02:00", "updated_at": null, "id": 5, "street": "1, street", "city": "city", "post_code": "1000", "country": "country", "is_main": true }}, "group": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "GRP_DELIVERY" }, "user_roles": { { "role": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "label": "ROLE_USER" }, "begin_at": "2020-05-25T16:35:54+02:00", "end_at": null }, { "role": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "label": "ROLE_ADMIN" }, "begin_at": "2020-05-25T16:35:54+02:00", "end_at": null } } })
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/username/{username}")
     * @Rest\View()
     * @param string $username
     * @return UserDetailsDTO|null
     */
    public function getOneByUsernameAction(string $username): ?AbstractDTO
    {
        try {
            /** @var User $user */
            $user = $this->getService()->getOneByUsername($username);
            if(!$user) return null;
            $addresses = $this->getService()->getAddresses($user->getId());
            $userRoles = $this->getService()->getUserRoles($user->getId());

            return new UserDetailsDTO($user, array_map(function($address){
                /** @var Address $address */
                return new AddressDTO($address);
            }, $addresses), array_map(function($userRole){
                /** @var UserRole $userRole */
                return new UserRoleDTOForUser($userRole);
            }, $userRoles));

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/email/{email}",
     *     summary="Get one user by email",
     *     description="Get one user by email",
     *     tags={"User"},
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="User",
     *          @OA\JsonContent(ref="#components/schemas/UserDetailsDTO", example={ "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY", "addresses": {{"created_at": "2020-06-11T14:58:29+02:00", "updated_at": null, "id": 5, "street": "1, street", "city": "city", "post_code": "1000", "country": "country", "is_main": true }}, "group": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "GRP_DELIVERY" }, "user_roles": { { "role": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "label": "ROLE_USER" }, "begin_at": "2020-05-25T16:35:54+02:00", "end_at": null }, { "role": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "label": "ROLE_ADMIN" }, "begin_at": "2020-05-25T16:35:54+02:00", "end_at": null } } })
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/email/{email}")
     * @Rest\View()
     * @param string $email
     * @return UserDetailsDTO|null
     */
    public function getOneByEmailAction(string $email): ?AbstractDTO
    {
        try {
            /** @var User $user */
            $user = $this->getService()->getOneByEmail($email);
            if(!$user) return null;
            $addresses = $this->getService()->getAddresses($user->getId());
            $userRoles = $this->getService()->getUserRoles($user->getId());

            return new UserDetailsDTO($user, array_map(function($address){
                /** @var Address $address */
                return new AddressDTO($address);
            }, $addresses), array_map(function($userRole){
                /** @var UserRole $userRole */
                return new UserRoleDTOForUser($userRole);
            }, $userRoles));

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/has_username/{username}",
     *     summary="Check if has username",
     *     description="Check if has username",
     *     tags={"User"},
     *     @OA\Parameter(
     *         name="username",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="User",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="username",
     *                  type="boolean",
     *                  example="true"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/has_username/{username}")
     * @Rest\View()
     * @param string $username
     * @return array
     */
    public function hasUsername(string $username): array
    {
        try {
            return ["username" => $this->getService()->hasUsername($username)];

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/has_email/{email}",
     *     summary="Check if has email",
     *     description="Check if has email",
     *     tags={"User"},
     *     @OA\Parameter(
     *         name="email",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="User",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="email",
     *                  type="boolean",
     *                  example="true"
     *              )
     *          )
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/has_email/{email}")
     * @Rest\View()
     * @param string $email
     * @return array
     */
    public function hasEmail(string $email): array
    {
        try {
            return ["email" => $this->getService()->hasEmail($email)];

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/user",
     *     summary="Add a user",
     *     description="Add a user",
     *     tags={"User"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/UserForm"),
     *             example={"username":"user", "password":"test", "firstName":"Théodore", "lastName":"Bourdon", "email":"wblanc@lefevre.com", "groupId":1, "address":{"street": "1, street", "city": "city", "postCode": "1000", "country": "country", "isMain": true}}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="User",
     *        @OA\JsonContent(ref="#components/schemas/UserDTO", example={ "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY" })
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Post(path="api/user")
     * @Rest\View()
     * @param Request $request
     * @return UserDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try{
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            if(key_exists("isMain", $data["address"])) $data["address"]["isMain"] = $data["address"]["isMain"] ? 1 : 0;
            $form->submit($data);
            $form->handleRequest($request);
            if($form->isSubmitted()){

                $data = $form->getData();
                dump($data);

                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var User $user */
                    $user = $this->getService()->post($data);
                    return new UserDTO($user);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/user/{id}",
     *     summary="Edit a user",
     *     description="Edit a user",
     *     tags={"User"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/UserForm"),
     *             example={"username":"user", "password":"test", "firstName":"Théodore", "lastName":"Bourdon", "email":"wblanc@lefevre.com", "groupId":1, "address":{"street": "1, street", "city": "city", "postCode": "1000", "country": "country", "isMain": true}}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="User",
     *        @OA\JsonContent(ref="#components/schemas/UserDTO", example={ "created_at": "2020-05-25T16:35:54+02:00", "updated_at": "2022-01-12T10:02:03+02:00", "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY" })
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Put(path="api/user/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return UserDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var User $user */
            $user = $this->getService()->getOneById($id);
            if(!$user)  throw new Exception("No User (id#$id)");

            /** @var Address $address */
            $address = $this->getService()->getMainAddress($id);
            if(!$address) throw new Exception("No main Address found in user");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("username", $data)) $data["username"] = $user->getUsername();
            if(!key_exists("password", $data)) $data["password"] = $user->getPassword();
            else $data["password"] = $this->encoder->encodePassword($user, $data["password"]);
            if(!key_exists("firstName", $data)) $data["firstName"] = $user->getFirstName();
            if(!key_exists("lastName", $data)) $data["lastName"] = $user->getLastName();
            if(!key_exists("email", $data)) $data["email"] = $user->getEmail();
            if(!key_exists("groupId", $data)) $data["groupId"] = $user->getGroup()->getId();

            if(key_exists("address", $data)) {
                if(!key_exists("street", $data["address"])) $data["address"]["street"] = $address->getStreet();
                if(!key_exists("city", $data["address"])) $data["address"]["city"] = $address->getCity();
                if(!key_exists("postCode", $data["address"])) $data["address"]["postCode"] = $address->getPostCode();
                if(!key_exists("country", $data["address"])) $data["address"]["country"] = $address->getCountry();
                if(key_exists("isMain", $data["address"])) $data["address"]["isMain"] = $data["address"]["isMain"] ? 1 : 0;

            } else {
                $data["address"] = [];
                $data["address"]["street"] = $address->getStreet();
                $data["address"]["city"] = $address->getStreet();
                $data["address"]["postCode"] = $address->getStreet();
                $data["address"]["country"] = $address->getStreet();
                $data["address"]["isMain"] = $address->getIsMain();
            }

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var User $user */
                    $user = $this->getService()->put($id, $data);
                    return new UserDTO($user);
                }
                else if(count($errors))  throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Patch(
     *     path="/api/user/{id}/role/{role_id}",
     *     summary="Add a role to a user",
     *     description="Add a role to a user",
     *     tags={"User"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="role_id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/UserRoleForm"),
     *             example={ "beginAt": "2020-10-05", "endAt": "2022-01-05" }
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="User",
     *        @OA\JsonContent(ref="#components/schemas/UserRoleDTOForUser", example={ "role": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "ROLE_DELIVERY" }, "begin_at": "2020-10-05T00:00:00+02:00", "end_at": "2022-01-05T00:00:00+01:00" })
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Patch(path="api/user/{id<\d+>}/role/{role_id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return UserRoleDTOForUser
     */
    public function addRoleAction(Request $request): UserRoleDTOForUser
    {
        try{
            $id = $request->get("id");
            $role_id = $request->get("role_id");

            $form = $this->createForm(UserRoleEntityType::class, new UserRoleFormModel(), [
                "csrf_protection" => false
            ]);

            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()) {
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()) {
                    /** @var UserRole $userRole */
                    $userRole = $this->getService()->addRole($id, $role_id, $form->getData());
                    return new UserRoleDTOForUser($userRole);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/user/{id}/role/{role_id}",
     *     summary="Edit a role from a user",
     *     description="Edit a role from a user",
     *     tags={"User"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="role_id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/UserRoleForm"),
     *             example={ "beginAt": "2021-10-05", "endAt": "2030-01-05" }
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="User",
     *        @OA\JsonContent(ref="#components/schemas/UserRoleDTOForUser", example={ "role": { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "ROLE_DELIVERY" }, "begin_at": "2021-10-05T00:00:00+02:00", "end_at": "2030-01-05T00:00:00+01:00" })
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Put(path="api/user/{id<\d+>}/role/{role_id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return UserRoleDTOForUser
     */
    public function editRoleAction(Request $request): UserRoleDTOForUser
    {
        try{
            $id = $request->get("id");
            $role_id = $request->get("role_id");

            /** @var UserRole $userRole */
            $userRole = $this->getService()->getOneUserRole($id, $role_id);
            if(!$userRole) throw new Exception( "No Role (id#$role_id) in User (id#$id) found");

            $form = $this->createForm(UserRoleEntityType::class, new UserRoleFormModel(), [
                "csrf_protection" => false
            ]);

            $data = json_decode($request->getContent(), true);

            if(!key_exists("beginAt", $data)) $data["beginAt"] = $userRole->getBeginAt() ? $userRole->getBeginAt()->format("Y-m-d") : null;
            if(!key_exists("endAt", $data))  $data["endAt"] = $userRole->getEndAt() ? $userRole->getEndAt()->format("Y-m-d") : null;

            $form->submit($data);
            $form->handleRequest($request);
            if($form->isSubmitted()) {
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()) {
                    /** @var UserRole $userRole */
                    $userRole = $this->getService()->editRole($id, $role_id, $form->getData());
                    return new UserRoleDTOForUser($userRole);
                }
                else if(count($errors))  throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }

        return null;
    }

    /**
     * @OA\Delete(
     *     path="/api/user/{id}/role/{role_id}",
     *     summary="Remove a role from a user",
     *     description="Remove a role from a user",
     *     tags={"User"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="role_id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Role (id#5) has been removed from User (id#6)")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/user/{id<\d+>}/role/{role_id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @param int $role_id > 0
     * @return JsonResponse
     */
    public function removeRoleAction(int $id, int $role_id): JsonResponse
    {
        try {
            $this->getService()->removeRole($id, $role_id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Role (id#$role_id) has been removed from User (id#$id)"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/user/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "User (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return UserEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return UserFormModel::class;
    }
}

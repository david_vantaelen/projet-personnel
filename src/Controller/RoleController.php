<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\GroupDTO;
use App\DTO\RoleDetailsDTO;
use App\DTO\RoleDTO;
use App\DTO\UserRoleDTOForRole;
use App\Entity\Group;
use App\Entity\Role;
use App\Entity\UserRole;
use App\Form\RoleEntityType;
use App\Form\Model\RoleFormModel;
use App\Service\RoleService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;

class RoleController extends BaseController
{
    public function __construct(
        ValidatorInterface $validator,
        RoleService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/role",
     *      summary="Get all roles",
     *      description="Get all roles",
     *      tags={"Role"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="Role list",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#components/schemas/RoleDTO"), example={{ "created_at": "2020-05-25T12:23:42+02:00", "updated_at": null, "id": 1, "label": "ROLE_USER" }, { "created_at": "2020-05-25T12:23:42+02:00", "updated_at": null, "id": 2, "label": "ROLE_ADMIN" }, { "created_at": "2020-05-25T12:23:42+02:00", "updated_at": null, "id": 3, "label": "ROLE_DELIVERY" }})
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/role")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return RoleDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function($role) {
                /** @var Role $role */
                return new RoleDTO($role);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/role/{id}",
     *     summary="Get one role by id",
     *     description="Get one role by id",
     *     tags={"Role"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Role",
     *          @OA\JsonContent(ref="#components/schemas/RoleDetailsDTO", example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "label": "ROLE_USER", "groups": { { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "label": "GRP_USER" }, { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "GRP_DELIVERY" }, { "created_at": "2020-05-27T09:27:47+02:00", "updated_at": "2020-05-27T09:30:24+02:00", "id": 4, "label": "GRP_NEW_USER" } }, "user_roles": { { "user": { "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY" }, "begin_at": "2020-05-25T16:35:54+02:00", "end_at": null }, { "user": { "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 2, "first_name": "Laurent", "last_name": "Allard", "username": "suser", "email": "elodie77@gmail.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$L3lXOUVyNGk2NWMvMlBELw$1RfnrAncVuKtE/sM6sUkxdAHoBdjej0TEV/Vf/HyfiA", "roles": { "ROLE_USER" }, "group_name": "GRP_USER" }, "begin_at": "2020-05-25T16:35:54+02:00", "end_at": null }, } })
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/role/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return RoleDetailsDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {

            /** @var Role $role */
            $role = $this->getService()->getOneById($id);
            $groups = $this->getService()->getGroups($id);
            $userRoles = $this->getService()->getUserRoles($id);

            return $role ? new RoleDetailsDTO($role, array_map(function($group){
                /** @var Group $group */
                return new GroupDTO($group);
            }, $groups),  array_map(function($userRole){
                /** @var UserRole $userRole */
                return new UserRoleDTOForRole($userRole);
            }, $userRoles)) : null;

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/role",
     *     summary="Add a role",
     *     description="Add a role",
     *     tags={"Role"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/RoleForm"),
     *             example={ "label": "ROLE_USER" }
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Role",
     *        @OA\JsonContent(ref="#components/schemas/RoleDTO", example={ "created_at": "2020-05-25T12:23:42+02:00", "updated_at": null, "id": 1, "label": "ROLE_USER" })
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/role")
     * @Rest\View()
     * @param Request $request
     * @return RoleDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try{
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()) {
                    /** @var Role $role */
                    $role = $this->getService()->post($data);
                    return new RoleDTO($role);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/role/{id}",
     *     summary="Edit a role",
     *     description="Edit a role",
     *     tags={"Role"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/RoleForm"),
     *             example={ "label": "ROLE_NEW_USER" }
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Role",
     *        @OA\JsonContent(ref="#components/schemas/RoleDTO", example={ "created_at": "2020-05-25T12:23:42+02:00", "updated_at": "2020-07-03T07:53:03+02:00", "id": 1, "label": "ROLE_OLD_USER" })
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Put(path="api/role/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return RoleDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try{
            $id = $request->get("id");
            /** @var Role $role */
            $role = $this->getService()->getOneById($id);
            if(!$role) throw new Exception("No Role (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("label", $data)) $data["label"] = $role->getLabel();

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Role $role */
                    $role = $this->getService()->put($id, $data);
                    return new RoleDTO($role);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Delete(
     *     path="/api/role/{id}",
     *     summary="Delete a role",
     *     description="Delete a role",
     *     tags={"Role"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Role (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/role/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Role (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return RoleEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return RoleFormModel::class;
    }
}

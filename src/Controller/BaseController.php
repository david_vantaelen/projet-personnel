<?php


namespace App\Controller;

use App\DTO\AbstractDTO;
use App\Service\AbstractService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;

/**
 * Class BaseController
 * @package App\Controller
 *
 *
 * @OA\Parameter(
 *     name="limit",
 *     in="query",
 *     description=null,
 *     required=false,
 *     @OA\Schema(type="integer")
 * )
 *
 * @OA\Parameter(
 *     name="offset",
 *     in="query",
 *     description=null,
 *     required=false,
 *     @OA\Schema(type="integer")
 * )
 *
 * @OA\Parameter(
 *     name="page",
 *     in="query",
 *     description=null,
 *     required=false,
 *     @OA\Schema(type="integer")
 * )
 *
 * @OA\Parameter(
 *     name="perPage",
 *     in="query",
 *     description=null,
 *     required=false,
 *     @OA\Schema(type="integer")
 * )
 *
 * @OA\Response(
 *     response="unauthorized",
 *     description="Unauthorized",
 *     @OA\JsonContent(
 *         @OA\Property(type="int", property="code", example="401"),
 *         @OA\Property(type="string", property="message", example="JWT Token not found")
 *     )
 * )
 *
 * @OA\Response(
 *     response="error",
 *     description="Error",
 *     @OA\JsonContent(
 *         @OA\Property(type="int", property="code", example="500"),
 *         @OA\Property(type="string", property="message", example="An error occurred")
 *     )
 * )
 *
 * @OA\Response(
 *     response="nocontent",
 *     description="No Content"
 * )
 *
 * @OA\SecurityScheme(securityScheme="bearerAuth", name="bearerAuth", scheme="bearer", in="header", bearerFormat="JWT", type="http")
 *
 */
abstract class BaseController extends AbstractFOSRestController
{
    /**
     * @var ValidatorInterface $validator
     */
    private $validator;

    /**
     * @var AbstractService $service
     */
    private $service;

    /**
     * BaseController constructor.
     * @param ValidatorInterface $validator
     * @param AbstractService $service
     */
    public function __construct(
        ValidatorInterface $validator,
        AbstractService $service
    )
    {
        $this->validator = $validator;
        $this->service = $service;
    }

    /**
     * @param Exception $exception
     * @throws HttpException
     */
    protected function jsonError(Exception $exception) {
        if($exception instanceof \InvalidArgumentException) throw new HttpException(Response::HTTP_BAD_REQUEST, $exception->getMessage());
        else throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $exception->getMessage());
    }

    /**
     * @return FormInterface
     */
    protected function _createForm() {
        $model = $this->getFormModel();
        return  $this->createForm($this->getType(), new $model, ["csrf_protection" => false]);
    }

    /**
     * @return string
     */
    abstract public function getType(): string;

    /**
     * @return string
     */
    abstract public function getFormModel(): string;

    /**
     * @param ParamFetcherInterface $paramFetcher
     * @return array
     */
    abstract public function getAllAction(ParamFetcherInterface $paramFetcher): array;

    /**
     * @param int $id > 0
     * @return AbstractDTO|null
     */
    abstract public function getOneByIdAction(int $id): ?AbstractDTO;

    /**
     * @param Request $request
     * @return AbstractDTO
     */
    abstract function postAction(Request $request): AbstractDTO;

    /**
     * @param Request $request
     * @return AbstractDTO
     */
    abstract public function putAction(Request $request): AbstractDTO;

    /**
     * @param int $id > 0
     * @return JsonResponse
     */
    abstract public function deleteAction(int $id): JsonResponse;

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @return AbstractService
     */
    public function getService(): AbstractService
    {
        return $this->service;
    }
}
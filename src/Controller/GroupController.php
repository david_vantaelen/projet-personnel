<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\GroupDetailsDTO;
use App\DTO\GroupDTO;
use App\DTO\RoleDTO;
use App\DTO\UserDTO;
use App\Entity\Group;
use App\Entity\Role;
use App\Entity\User;
use App\Form\GroupEntityType;
use App\Form\Model\GroupFormModel;
use App\Service\GroupService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;

class GroupController extends BaseController
{
    public function __construct(
        ValidatorInterface $validator,
        GroupService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/group",
     *      summary="Get all groups",
     *      description="Get all groups",
     *      tags={"Group"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="Group list",
     *          @OA\JsonContent(type="array", @OA\Items(ref="#components/schemas/GroupDTO"), example={{ "created_at": "2020-05-25T12:23:42+02:00", "updated_at": null, "id": 1, "label": "GRP_USER" }, { "created_at": "2020-05-25T12:23:42+02:00", "updated_at": null, "id": 2, "label": "GRP_ADMIN" }, { "created_at": "2020-05-25T12:23:42+02:00", "updated_at": null, "id": 3, "label": "GRP_DELIVERY" }})
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/group")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return GroupDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function($group) {
                /** @var Group $group */
                return new GroupDTO($group);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/group/{id}",
     *     summary="Get one group by id",
     *     description="Get one group by id",
     *     tags={"Group"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Group",
     *          @OA\JsonContent(ref="#components/schemas/GroupDetailsDTO", example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "GRP_DELIVERY", "roles": {{ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "label": "ROLE_USER" }, { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "ROLE_DELIVERY" }}, "users": {{ "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY" }}})
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/group/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return GroupDetailsDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {

            /** @var Group $group */
            $group = $this->getService()->getOneById($id);

            if(!$group) return null;

            $roles = array_values(
                array_filter(
                    $group->getRoles()->toArray(), function($role) {
                    /** @var Role $role */
                    return $role->getIsActive();
                })
            );

            $users = $this->getService()->getUsers($id);

            return new GroupDetailsDTO($group, array_map(function($role){
                /** @var Role $role */
                return new RoleDTO($role);
            }, $roles), array_map(function($user){
                /** @var User $user */
                return new UserDTO($user);
            }, $users));

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/group/{value}",
     *     summary="Get one group by name",
     *     description="Get one group by name",
     *     tags={"Group"},
     *     @OA\Parameter(
     *         name="value",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Group",
     *          @OA\JsonContent(ref="#components/schemas/GroupDetailsDTO", example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "GRP_DELIVERY", "roles": {{ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "label": "ROLE_USER" }, { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "ROLE_DELIVERY" }}, "users": {{ "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY" }}})
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/group/{value}")
     * @Rest\View()
     * @param string $value
     * @return GroupDetailsDTO|null
     */
    public function getOneByGroupAction(string $value): ?AbstractDTO
    {
        try {
            /** @var Group $group */
            $group = $this->getService()->getOneByGroup($value);

            if(!$group) return null;

            $roles = array_values(
                array_filter(
                    $group->getRoles()->toArray(), function($role) {
                    /** @var Role $role */
                    return $role->getIsActive();
                })
            );

            $users = $this->getService()->getUsers($group->getId());

            return $group ? new GroupDetailsDTO($group, array_map(function($role){
                /** @var Role $role */
                return new RoleDTO($role);
            }, $roles), array_map(function($user){
                /** @var User $user */
                return new UserDTO($user);
            }, $users)) : null;

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/group",
     *     summary="Add a group",
     *     description="Add a group",
     *     tags={"Group"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/GroupForm"),
     *             example={ "label": "GRP_USER" }
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Group",
     *        @OA\JsonContent(ref="#components/schemas/GroupDTO", example={ "created_at": "2020-05-25T12:23:42+02:00", "updated_at": null, "id": 1, "label": "GRP_USER" })
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/group")
     * @Rest\View()
     * @param Request $request
     * @return GroupDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try{
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()) {
                    /** @var Group $group */
                    $group = $this->getService()->post($data);
                    return new GroupDTO($group);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/group/{id}",
     *     summary="Edit a group",
     *     description="Edit a group",
     *     tags={"Group"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/GroupForm"),
     *             example={ "label": "GRP_NEW_USER" }
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Group",
     *        @OA\JsonContent(ref="#components/schemas/GroupDTO", example={ "created_at": "2020-05-25T12:23:42+02:00", "updated_at": "2020-07-03T07:53:03+02:00", "id": 1, "label": "GRP_OLD_USER" })
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Put(path="api/group/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return GroupDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Group $group */
            $group = $this->getService()->getOneById($id);
            if(!$group)  throw new Exception("No Group (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("label", $data)) $data["label"] = $group->getLabel();

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()) {
                    /** @var Group $group */
                    $group = $this->getService()->put($id, $data);
                    return new GroupDTO($group);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Patch(
     *     path="/api/group/{id}/role/{role_id}",
     *     summary="Add an role to a group",
     *     description="Add an role to a group",
     *     tags={"Group"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="role_id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Group",
     *        @OA\JsonContent(ref="#components/schemas/GroupDetailsDTO", example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "GRP_DELIVERY", "roles": {{ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "label": "ROLE_USER" }, { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 3, "label": "ROLE_DELIVERY" }}, "users": {{ "created_at": "2020-05-25T16:35:54+02:00", "updated_at": null, "id": 1, "first_name": "Théodore", "last_name": "Bourdon", "username": "user", "email": "wblanc@lefevre.com", "password": "$argon2i$v=19$m=65536,t=5,p=1$TmtpbEp6Si5sQVFZRy44Rg$Eu86prBn4qGyf7VOWOPh6bU7jJetOwQ/JcYKdW/8yc8", "roles": { "ROLE_USER", "ROLE_DELIVERY", "ROLE_ADMIN" }, "group_name": "GRP_DELIVERY" }}})
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Patch(path="api/group/{id<\d+>}/role/{role_id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @param int $role_id > 0
     * @return GroupDetailsDTO
     */
    public function addRoleAction(int $id, int $role_id): GroupDetailsDTO
    {
        try{
            /** @var Group $group */
            $group = $this->getService()->addRole($id, $role_id);

            $roles = array_values(
                array_filter(
                    $group->getRoles()->toArray(), function($role) {
                    /** @var Role $role */
                    return $role->getIsActive();
                })
            );

            $users = $this->getService()->getUsers($id);

            return new GroupDetailsDTO($group, array_map(function($role){
                /** @var Role $role */
                return new RoleDTO($role);
            }, $roles), array_map(function($user){
                /** @var User $user */
                return new UserDTO($user);
            }, $users));

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Delete(
     *     path="/api/group/{id}/role/{role_id}",
     *     summary="Remove an role from a group",
     *     description="Remove an role from a group",
     *     tags={"Group"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Parameter(
     *         name="role_id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Role (id#5) has been removed from Group (id#6)")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/group/{id<\d+>}/role/{role_id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @param int $role_id > 0
     * @return JsonResponse
     */
    public function removeRoleAction(int $id, int $role_id): JsonResponse
    {
        try {
            $this->getService()->removeRole($id, $role_id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Role (id#$role_id) has been removed from Group (id#$id)"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Delete(
     *     path="/api/group/{id}",
     *     summary="Delete a group",
     *     description="Delete a group",
     *     tags={"Group"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Group (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/group/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Group (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return GroupEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return GroupFormModel::class;
    }
}

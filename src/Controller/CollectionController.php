<?php

namespace App\Controller;

use App\DTO\AbstractDTO;
use App\DTO\CollectionDTO;
use App\Entity\Collection;
use App\Form\CollectionEntityType;
use App\Form\Model\CollectionFormModel;
use App\Service\CollectionService;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use OpenApi\Annotations as OA;


class CollectionController extends BaseController
{
    /**
     * CollectionController constructor.
     * @param ValidatorInterface $validator
     * @param CollectionService $service
     */
    public function __construct(
        ValidatorInterface $validator,
        CollectionService $service
    )
    {
        parent::__construct($validator, $service);
    }

    /**
     * @OA\Get(
     *      path="/api/collection",
     *      summary="Get all collections",
     *      description="Get all collections",
     *      tags={"Collection"},
     *      @OA\Parameter(ref="#/components/parameters/limit"),
     *      @OA\Parameter(ref="#/components/parameters/offset"),
     *      @OA\Response(
     *          response="200",
     *          description="Collection list",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#components/schemas/CollectionDTO"),
     *              example={{ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 1, "collection": "Pour les Kids" }, { "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "collection": "Pour les nuls" }}
     *          )
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/collection")
     * @Rest\View()
     * @Rest\QueryParam(name="limit", requirements="\d+", nullable=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return CollectionDTO[]
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher): array
    {
        try {
            $limit = $paramFetcher->get("limit");
            $offset = $paramFetcher->get("offset");

            $data = $this->getService()->getAll($limit, $offset);

            return array_map(function(Collection $collection) {
                return new CollectionDTO($collection);
            }, $data);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Get(
     *     path="/api/collection/{id}",
     *     summary="Get one collection by id",
     *     description="Get one collection by id",
     *     tags={"Collection"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Collection",
     *          @OA\JsonContent(
     *              ref="#components/schemas/CollectionDTO",
     *              example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "collection": "Pour les nuls" }
     *          )
     *     ),
     *     @OA\Response(
     *        response=204, ref="#/components/responses/nocontent"
     *     ),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @Rest\Get(path="api/collection/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return CollectionDTO|null
     */
    public function getOneByIdAction(int $id): ?AbstractDTO
    {
        try {
            /** @var Collection $collection */
            $collection = $this->getService()->getOneById($id);
            return $collection ? new CollectionDTO($collection) : null;

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Post(
     *     path="/api/collection",
     *     summary="Add a collection",
     *     description="Add a collection",
     *     tags={"Collection"},
     *     security={{"bearerAuth":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CollectionForm"),
     *             example={"collection": "Pour les nuls"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Collection",
     *        @OA\JsonContent(
     *             ref="#components/schemas/CollectionDTO",
     *             example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": null, "id": 2, "collection": "Pour les nuls" }
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Post(path="api/collection")
     * @Rest\View()
     * @param Request $request
     * @return CollectionDTO
     */
    public function postAction(Request $request): AbstractDTO
    {
        try {
            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Collection $collection */
                    $collection = $this->getService()->post($data);
                    return new CollectionDTO($collection);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @OA\Put(
     *     path="/api/collection/{id}",
     *     summary="Edit a collection",
     *     description="Edit a collection",
     *     tags={"Collection"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/CollectionForm"),
     *             example={"collection": "Pour les nuls"}
     *         )
     *     ),
     *     @OA\Response(
     *        response=200,
     *        description="Collection",
     *        @OA\JsonContent(
     *            ref="#components/schemas/CollectionDTO",
     *            example={ "created_at": "2020-05-25T16:35:53+02:00", "updated_at": "2021-06-03T21:44:20+02:00", "id": 2, "collection": "Pour les nuls" }
     *        )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Put(path="api/collection/{id<\d+>}")
     * @Rest\View()
     * @param Request $request
     * @return CollectionDTO
     */
    public function putAction(Request $request): AbstractDTO
    {
        try {
            $id = $request->get("id");
            /** @var Collection $collection */
            $collection = $this->getService()->getOneById($id);
            if(!$collection) throw new Exception("No Collection (id#$id)");

            $form = $this->_createForm();
            $data = json_decode($request->getContent(), true);

            if(!($data && count($data))) throw new Exception("No Content");
            if(!key_exists("collection", $data)) $data["collection"] = $collection->getCollection();

            $form->submit($data);
            $form->handleRequest($request);

            if($form->isSubmitted()){
                $data = $form->getData();
                $errors = $this->getValidator()->validate($data);

                if(!count($errors) && $form->isValid()){
                    /** @var Collection $collection */
                    $collection = $this->getService()->put($id, $data);
                    return new CollectionDTO($collection);
                }
                else if(count($errors)) throw new Exception((string) $errors);
            } else throw new Exception('An error occurred. Unable to submit the form.');

        } catch(Exception $e) {
            $this->jsonError($e);

        }

        return null;
    }

    /**
     * @OA\Delete(
     *     path="/api/collection/{id}",
     *     summary="Delete a collection",
     *     description="Delete a collection",
     *     tags={"Collection"},
     *     security={{"bearerAuth":{}}},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description=null,
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description=null,
     *          @OA\JsonContent(
     *              @OA\Property(type="int", property="code", example="200"),
     *              @OA\Property(type="string", property="message", example="Collection (id#5) deleted")
     *          )
     *     ),
     *     @OA\Response(response=401, ref="#/components/responses/unauthorized"),
     *     @OA\Response(response=500, ref="#/components/responses/error")
     * )
     * @IsGranted("ROLE_ADMIN")
     * @Rest\Delete(path="api/collection/{id<\d+>}")
     * @Rest\View()
     * @param int $id > 0
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        try {
            $this->getService()->delete($id);
            return (new JsonResponse())->setData(["code" => 200, "message" => "Collection (id#$id) deleted"]);

        } catch(Exception $e) {
            $this->jsonError($e);

        }
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return CollectionEntityType::class;
    }

    /**
     * @return string
     */
    public function getFormModel(): string
    {
        return CollectionFormModel::class;
    }
}

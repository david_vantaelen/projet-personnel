<?php

namespace App\DataFixtures;

use App\Entity\Financial;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FinancialFixtures extends Fixture
{
    public const FINANCIAL_REFERENCES = ["VISA", "MasterCard", "Virement Bancaire", "PayPal"];
    public function load(ObjectManager $manager)
    {
        foreach(self::FINANCIAL_REFERENCES as $f) {
            ($financial = new Financial())
                ->setName($f)
                ->setDescription("[[p]]Paiement par le moyen de [[b]]`$f`[[/b]][[/p]]")
                ->setImage(str_replace(" ", "_", strtolower($f)).".png");
            $this->addReference($f, $financial);
            $manager->persist($financial);
        }

        $manager->flush();
    }
}

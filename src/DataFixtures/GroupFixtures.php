<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class GroupFixtures extends Fixture implements DependentFixtureInterface
{
    public const GROUP_REFERENCES = ["GRP_USER", "GRP_ADMIN", "GRP_DELIVERY"];
    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < count(self::GROUP_REFERENCES); $i++) {
            /** @var Role $role */
            $role = $this->getReference(RoleFixtures::ROLE_REFERENCES[0]);
            ($group = new Group())
                ->setLabel(self::GROUP_REFERENCES[$i])
                ->addRole($role);

            if($i > 0) {
                /** @var Role $add_role */
                $add_role = $this->getReference(RoleFixtures::ROLE_REFERENCES[$i]);
                $group->addRole($add_role);
            }
            $this->addReference(self::GROUP_REFERENCES[$i], $group);
            $manager->persist($group);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [RoleFixtures::class];
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Collection;
use App\Entity\Edition;
use App\Entity\Editor;
use App\Entity\Format;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EditionFixtures extends Fixture implements DependentFixtureInterface
{
    public const EDITION_REFERENCE = "edition";

    public function load(ObjectManager $manager)
    {
        /** @var Format $format */
        $format = $this->getReference(FormatFixtures::FORMAT_REFERENCES[0]);
        
        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Python pour les Kids")
                ->setAuthor((new Author())
                    ->setFirstName("Jason")
                    ->setLastName("R. Briggs")))
            ->setEditor(($editor = new Editor())
                ->setEditor("Librairie Eyerolles"))
            ->setCollection(($collection = new Collection())
                ->setCollection("Pour les Kids"))
            ->setReleaseAt(new \DateTime("19-03-2015"))
            ->setImagePath("img1.jpg")
            ->setPrice(20.9)
            ->setStock(8)
            ->setFormat($format)
            ->setDescription("[[b]]La programmation accessible à tous ![[/b]][[p]]Python est un langage de programmation puissant, expressif, facile à apprendre et amusant. Il est compatible avec Mac, Windows et Linux.[[/p]][[p]][[i]]Python pour les kids[[/i]] donne vie à Python et t'emmène, ainsi que tes parents, dans l'univers de la programmation. Avec des trésors de patience, Jason R. Briggs te guidera parmi les bases, à mesure que tu t'essaieras à des exemples de programmes uniques et parfois hilarants, qui mettent en lumière des monstres voraces, des sorciers, des agents secrets, des corbeaux voleurs et d'autres curiosités du genre. Les définitions des termes utilisés, le code colorisé et expliqué en détail, ainsi que des illustrations en couleurs agrémentent l'apprentissage et le rendent plus aisé.[[/p]][[p]]Les fins de chapitres proposent des puzzles de programmation pour t'entraîner. À la fin du livre, tu auras programmé deux jeux complets : un clone du fameux jeu de pong (balle bondissante et raquette) et \"M. Filiforme court vers la sortie\", un jeu de plates- formes avec des sauts, des animations et bien plus.[[/p]][[p]]A partir de 10 ans[[/p]][[b]]Tout au long de cette aventure, tu apprendras à :[[/b]][[l]][[li]]te servir des structures de données fondamentales comme les listes, les tuples et les dictionnaires ;[[/li]][[li]]organiser et réutiliser ton code à l'aide de fonctions, de classes et de modules ;[[/li]][[li]]utiliser les structures de contrôle comme les boucles et les instructions conditionnelles ;[[/li]][[li]]dessiner des formes et des motifs à l'aide du module de la tortue de Python ;[[/li]][[li]]créer des jeux, des animations et d'autres merveilles avec tkinter.[[/li]][[/l]][[p]]Pourquoi les adultes seraient-ils seuls à s'amuser ? [[i]]Python pour les kids[[/i]] est ton ticket d'entrée dans le monde merveilleux de la programmation.[[/p]]");

        $this->addReference(self::EDITION_REFERENCE, $edition);

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Le Droit pour les nuls")
                ->setAuthor((new Author())
                    ->setFirstName("Nicolas")
                    ->setLastName("Guerrero")))
            ->setEditor(($editor = new Editor())
                ->setEditor("First"))
            ->setCollection(($collection = new Collection())
                ->setCollection("Pour les nuls"))
            ->setReleaseAt(new \DateTime("2015-01-22"))
            ->setImagePath("img2.jpg")
            ->setPrice(22.95)
            ->setStock(3)
            ->setFormat($format)
            ->setDescription("[[p]]Le droit, en fait, c'est simple ![[/p]][[p]]Le droit vous semble aride ? Il est passionnant ! Et il irrigue l'ensemble des aspects de notre vie. Plus que jamais, il est utile de le maîtriser.[[/p]][[p]]Quelle est la différence entre le droit civil et le droit pénal ? Qu'est-ce que la jurisprudence ? Comment distinguer un décret d'un arrêté ? À quoi sert le juge administratif ?[[/p]][[p]]Cet ouvrage clair et synthétique vous présente les grandes branches du droit : droit privé, droit pénal, droit public. Il détaille également les sources du droit en France : Constitution, traités internationaux, droit de l'Union européenne, lois, règlements. Enfin, le lecteur peut se faire une idée précise des différentes carrières possibles dans le domaine juridique : magistrat, avocat, huissier, notaire, commissaire-priseur ou encore officier de police.[[/p]][[p]]Découvrez dans cet ouvrage :[[/p]][[p]]Les différentes branches du droit[[/p]][[p]]Les sources du droit en France et leur hiérarchie[[/p]][[p]]L'organisation administrative et judiciaire française[[/p]][[p]]Dix droits fondamentaux, dix grandes lois, dix traités internationaux majeurs.[[/p]]"
            );

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Je débute le chant pour les nuls - kit avec logiciel d'apprentissage + cd-audio + manuel")
                ->setAuthor((new Author())
                    ->setFirstName("Collectif")
                    ->setLastName("")))
            ->setEditor($editor)
            ->setCollection($collection)
            ->setReleaseAt(new \DateTime("2014-11-06"))
            ->setImagePath("img3.jpg")
            ->setPrice(29.95)
            ->setStock(5)
            ->setFormat($format)
            ->setDescription("[[p]]Travaillez avec un coach vocal personnalisé, avec ce kit complet interactif ![[/p]][[p]]Vous voulez vous mettre au chant pour de bon, mais vous ne savez pas par où commencer : Comment placer votre voix ? Quelle posture adopter en chantant ? Comment savoir si vous chantez juste ? Quels exercices faire pour progresser vite et bien ?[[/p]][[p]]Avec ce kit complet, vous trouverez toutes les réponses à vos questions. Vous développerez le potentiel de votre voix et améliorerez vos performances à coup sûr.[[/p]][[p]]Dans ce kit :[[/p]][[l]][[li]]Un [[b]]livre[[/b]] pour vous guider dans l'apprentissage[[/li]][[li]]Le [[b]]CD audio[[/b]] d'accompagnement pour écouter les exercices proposés dans le guide[[/li]][[li]]Un [[b]]logiciel interactif[[/b]] pour vous entraîner et vérifier vos progrès jour après jour[[/li]][[/l]][[p]]Avec un programme dédié aux voix masculines et un autre dédié aux voix féminines, découvrez un programme d'apprentissage personnalisé unique et efficace ![[/p]]");

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Élever le lama")
                ->setAuthor((new Author())
                    ->setFirstName("Christian")
                    ->setLastName("Giudicelli")))
            ->setEditor(($editor = new Editor())
                ->setEditor("Crépin-Leblond"))
            ->setCollection(($collection = new Collection())
                ->setCollection("Beaux Livres"))
            ->setReleaseAt(new \DateTime("1994-03-02"))
            ->setImagePath("img4.jpg")
            ->setPrice(148.76)
            ->setStock(6)
            ->setFormat($format);

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Plus belle la vie Tome 3 Le trésor du Mistral")
                ->setAuthor(($author_ClaudeLambesc = new Author())
                    ->setFirstName("Claude")
                    ->setLastName("Lambesc")))
            ->setEditor(($editor_fleuveNoir = new Editor())
                ->setEditor("Fleuve Noir"))
            ->setReleaseAt(new \DateTime("2008-06-05"))
            ->setImagePath("img5.jpg")
            ->setPrice(13)
            ->setStock(2)
            ->setFormat($format)
            ->setDescription("[[p]][[b]]Description résumé[[/b]][[/p]][[p]]Luna, la jolie Luna, fait tourner bien des têtes. Quand Guillaume Leserman débarque au Mistral, c'est le coup de foudre : ces deux-là sont faits l'un pour l'autre. Mais est-il vraiment le dévoué neveu de Rachel qu'il prétend être ? Et comment Charles Frémont, dévoré par la jalousie, va-t-il réagir ? Il y a aussi Nathan, le fils de Guillaume, qui trouve en Juliette Frémont l'écoute qu'il n'a pas chez lui. Entraînés, à la suite de leurs pères, dans la quête de manuscrits perdus, les deux jeunes gens auront l'occasion de mettre à l'épreuve leur amitié. Jusqu'où ira leur sens du sacrifice ?[[/p]]");

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Bridget Jones - L'âge de raison")
                ->setAuthor((new Author())
                    ->setFirstName("Helen")
                    ->setLastName("Fielding")))
            ->setEditor(($editor = new Editor())
                ->setEditor("Albin Michel"))
            ->setReleaseAt(new \DateTime("2004-11-17"))
            ->setImagePath("img6.jpg")
            ->setPrice(20.15)
            ->setStock(4)
            ->setFormat($format)
            ->setDescription("[[p]]Vous pensiez peut-être qu’elle s’était assagie ? Qu’elle avait finalement atteint son « équilibre intérieur » ? Et bien vous aviez tout faux…[[/p]][[p]]Bridget Jones est de retour. La seule, l’unique, la vraie. Avec ses angoisses (perdre au moins cinq kilos), ses rêves (rencontrer l’âme sœur), ses utopies (que l’âme sœur ne la plaque pas au bout de deux jours) et ses obsessions (arrêter de fumer et de boire du chardonnay).[[/p]]");

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Le Chant pour les Nuls")
                ->setAuthor((new Author())
                    ->setFirstName("Collectif")
                    ->setLastName("")))
            ->setEditor(($editor = new Editor())
                ->setEditor("eMedia"))
            ->setReleaseAt(new \DateTime("2020-04-01"))
            ->setImagePath("img7.jpg")
            ->setPrice(28)
            ->setStock(1)
            ->setFormat($format)
            ->setDescription("[[b]]Logiciel méthode de chant interactive - Version téléchargement[[/b]][[l]][[li]]Un moyen amusant et facile pour apprendre à chanter étape par étape avec plus de 80 leçons[[/li]][[li]]Vidéos haute résolution, morceaux enregistrés en live ou pistes MIDI avec tempo réglable[[/li]][[li]]Idéal comme accompagnement pour les chansons et les exercices inclus[[/li]][[li]]Les leçons facilement compréhensibles montrent comment chanter dans le tempo et en harmonie, utiliser une technique de chant appropriée et développer et projeter la voix[[/li]][[li]]Retour interactif de la hauteur de votre voix en temps réel[[/li]][[li]]Une analyse personnalisée indique sur le progrès de l'apprentissage[[/li]][[li]]Fonctionnalités intégrées: métronome et enregistreur[[/li]][[li]]Plus de 30 vidéos et 35 chansons[[/li]][[li]]Langue: français[[/li]][[/l]][[b]]Spécifications:[[/b]][[l]][[li]]Système requis: MAC OSX 10.5 min, microphone, connexion internet[[/li]][[/l]]");

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Secret Story 3 - L'envers du décor")
                ->setAuthor((new Author())
                    ->setFirstName("Elisabeth")
                    ->setLastName("Fanger")))
            ->setEditor(($editor = new Editor())
                ->setEditor(" L'Archipel "))
            ->setReleaseAt(new \DateTime("2009-11-04"))
            ->setImagePath("img8.jpg")
            ->setPrice(11.99)
            ->setStock(12)
            ->setFormat($format)
            ->setDescription("[[p]]Depuis le 20 juin, 12 candidats de la nouvelle saison de \" Secret Story 3 \", l'émission de téléréalité de TF1, sont reclus dans une villa coupée du monde, la \" Maison des secrets \", où ils sont filmés en permanence. Chacun d'entre eux détient un secret. La \" Voix \", seul maître à bord, rythme leur quotidien au fil d'instructions et de défis à relever. Chaque émission animée par Benjamin Castaldi est diffusée quotidiennement à 18h30 avec, chaque vendredi, l'élimination d'un des candidats en direct à 22h30. La production diffuse sur Internet des passages inédits (scènes de douche..) pour entretenir l'intérêt...Ce livre raconte ce qu'on ne voit pas à l'écran : les méthodes de la production, les pressions que subissent les candidats, les engagements auxquels ils sont tenus. Il répond à nombre de questions que se posent les téléspectateurs : Comment les candidats sont-ils rémunérés ? Les départs des candidats sont-ils vraiment décidés par les téléspectateurs ? Les votes sont-ils orientés ? Que se passe-t-il vraiment derrière la scène ? Il évoque aussi les événements hors du plateau.[[/p]]");

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Nouvelle star : la méthode pour apprendre à chanter")
                ->setAuthor((new Author())
                    ->setFirstName("Anne")
                    ->setLastName("Peko")))
            ->setEditor(($editor = new Editor())
                ->setEditor("Hachette Pratique"))
            ->setCollection(($collection = new Collection())
                ->setCollection("Loisirs / Sports/ Passions"))
            ->setReleaseAt(new \DateTime("2008-11-01"))
            ->setImagePath("img9.jpg")
            ->setPrice(6.92)
            ->setStock(2)
            ->setFormat($format)
            ->setDescription("[[p]]Une méthode de chant progressive et interactive, acompagnée d'un CD audio, pour apprendre à chanter et maîtriser les difficultés techniques de l'art. Une partie d'exercices illustrés permet de se mettre en condition et se s'échauffer la voix avant d'attaquer la méthode proprement dite : 10 leçons articulées autour de 10 chansons (parmi les plus connues du répertoire français et étranger) expliquent au lecteur tout ce qu'il faut savoir pour se faire plaisir en les interprétant. Outil indispensable, le CD sur lequel la chanson est interprétée par l'auteur accompagnée au piano, puis le piano seul... À vous de chanter ! En bonus sur le CD : des exercices de vocalises pour s'échauffer comme un pro.[[/p]]");

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Plus belle la vie Tome 5 L'inconnu du Mistral")
                ->setAuthor($author_ClaudeLambesc))
            ->setEditor($editor_fleuveNoir)

            ->setReleaseAt(new \DateTime("2009-03-12"))
            ->setImagePath("img10.jpg")
            ->setPrice(13)
            ->setStock(3)
            ->setFormat($format)
            ->setDescription("[[p]][[b]]Description résumé[[/b]][[/p]][[p]]Arrivé d'Australie pour trouver au Mistral une inspiration nouvelle, Marc Vernet, un bel artiste peintre, a tout pour séduire Ninon. Au grand désespoir de son père, Vincent, qui se méfie de ce type trop mystérieux à son goût. Très vite, Marc tombe le masque et avoue son seul but : venger la mort de la femme qu'il aimait ! Mais qui est le véritable coupable ? À quoi rime ce complot qui semble se dessiner et qui seront les prochaines victimes ? Car si l'amour de Ninon et de Marc résiste aux tempêtes, ce sont leurs vies qui sont en jeu...[[/p]]");

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Mélancolique anonyme")
                ->setAuthor((new Author())
                    ->setFirstName("Soprano")
                    ->setLastName("")))
            ->setEditor(($editor = new Editor())
                ->setEditor("Don Quichotte"))
            ->setCollection(($collection = new Collection())
                ->setCollection("Non fiction"))
            ->setReleaseAt(new \DateTime("2014-05-21"))
            ->setImagePath("img11.jpg")
            ->setPrice(18.5)
            ->setStock(80)
            ->setFormat($format)
            ->setDescription("[[p]]Saïd M'Roumbaba, a.k.a. Soprano, est né de parents émigrés comoriens dans une HLM des quartiers nord de Marseille, en 1979. Dans la cité phocéenne, il partage son temps entre l'école, les parties de foot sur les terrains vagues et les sessions de rap dans la chambre de ses potes. Il enregistre ses premiers freestyles sur les K7 que sa famille s'échange en guise de lettres, faute de pouvoir écrire. Remarqué par Akhenaton, Soprano connaît ses premiers succès avec ses complices des Psy 4 de la rime, et dans la foulée, il crée avec plusieurs de ses amis le label Street Skillz. Il nous raconte cette route vers la professionnalisation, se remémorant des souvenirs cocasses, comme les premiers albums aux stickers mal collés qui détruisent les lecteurs CD des copains, ou les piles de disques qui s'entassent dans leurs salons et qu'il faut distribuer tant bien que mal. Mais il revient aussi sur ce qu'il appelle sa « mélancolie », qu'il ressent depuis l'adolescence (timidité maladive, manque de confiance en soi et en son talent, désillusions amoureuses...). En 2007 cependant, c'est le premier album solo, Puisqu'il faut vivre, et la deuxième partie de sa brillante carrière. Consacré par trois trophées à l'année du hip hop, il est l'unique rappeur français à participer aux prestigieuses BET Cypher américaines. À travers ces pages, Soprano revient sur son parcours, son histoire, mais aussi sur ses racines africaines : le mariage grandiose de sa soeur aux Comores, son engagement en Afrique auprès des populations les plus démunies, ses concerts de près de 100 000 personnes, et ses rencontres avec les Magic System qui vont forger sa conception du rôle de l'artiste dans la société. Mélancolique anonyme, c'est donc le récit d'une aventure humaine, la victoire d'un homme sur la dépression. C'est aussi les chroniques de Marseille, où, loin des règlements de compte et des trafics en tous genres, se côtoient Français, Irakiens, Turcs, Marocains, Tunisiens, Italiens, Sénégalais, Arméniens, Comoriens... dans une joie de vivre et une belle solidarité. C'est un panorama du rap et du hip hop des vingt-cinq dernières années. C'est surtout l'histoire d'une joyeuse réussite, entre amis, un éloge du rêve et de la persévérance pour tous les jeunes de quartiers défavorisés, que rien ne condamne à gâcher leur vie.[[/p]][[p]]Soprano se livre pour la première fois dans ce récit autobiographique. Il prépare son quatrième album solo, Cosmopolitanie (automne 2014).[[/p]]");

        $manager->persist($edition);

        ($edition = new Edition())
            ->setBook((new Book())
                ->setTitle("Chaque jour, j'écoute battre mon coeur")
                ->setAuthor((new Author())
                    ->setFirstName("Charlotte")
                    ->setLastName("Valandrey")))
            ->setEditor(($editor = new Editor())
                ->setEditor("J'ai lu"))

            ->setReleaseAt(new \DateTime("2019-06-05"))
            ->setImagePath("img12.jpg")
            ->setPrice(18)
            ->setStock(18450)
            ->setFormat($format)
            ->setDescription("[[p]]Comment peut-on expliquer qu'une femme dont l'espérance de vie à dix-sept ans était de six mois s'apprête aujourd'hui à fêter ses cinquante ans ? Quelle force permet de se relever, de voir une opportunité dans la difficulté ?[[/p]][[p]]Charlotte Valandrey, séropositive et greffée cardiaque, s'est construit au fil des épreuves une philosophie de vie unique : l'optimisme vrai. Son principe ? Se concentrer sur la réalité de l'instant présent, sur tout le potentiel que l'on a en soi, et aimer vraiment la personne que l'on est. En alliant optimisme, bienveillance et vérité thérapeutique, Charlotte a appris à dépasser ses émotions négatives, à mobiliser ses ressources intérieures, à motiver son corps et son esprit vers un seul but : jouir de la vie ici et maintenant.Cet ouvrage de \"dévoilement de soi\" offre toutes les clés pour savourer le présent, croire en sa volonté, en son pouvoir d'action, se libérer de ses peurs et devenir le meilleur de soi-même. Charlotte y partage son expérience et livre en toute sincérité ses secrets, ses exercices et ses techniques – ses mots aidants, ses plans d'action, sa méthode ADIVA...Avec Chaque jour, j'écoute battre mon cœur, devenez, vous aussi, un optimiste vrai.[[/p]]");

        $manager->persist($edition);
        $manager->flush();

    }

    public function getDependencies()
    {
        return [FormatFixtures::class];
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Group;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserRole;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Exception;
use Faker;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFixtures
 * @package App\DataFixtures
 */
class UserFixtures extends Fixture implements DependentFixtureInterface
{
    public const USER_REFERENCES = ["user_1", "user_2", "user_3", "user_4", "user_5"];

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @param ObjectManager $manager
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create("fr_FR");

        for ($i = 0; $i < count(self::USER_REFERENCES); $i++) {
            /** @var Group $group */
            $group = $this->getReference(GroupFixtures::GROUP_REFERENCES[0]);
            /** @var Group $group2 */
            $group2 = $this->getReference(GroupFixtures::GROUP_REFERENCES[2]);

            ($user = new User())
                ->setUsername($i === 0 ? "user" : ($i === 1 ? "suser" : $faker->userName))
                ->setPassword($this->encoder->encodePassword($user, "test"))
                ->setEmail($faker->email)
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setGroup($group);

            ($address = new Address())
                ->setStreet($faker->streetAddress)
                ->setCity($faker->city)
                ->setPostCode($faker->postcode)
                ->setCountry($faker->country)
                ->setIsMain(1)
                ->setUser($user);


            if($i === 0) $user->setGroup($group2);

            $this->addReference(self::USER_REFERENCES[$i], $user);

            $manager->persist($user);
            $manager->persist($address);

            /** @var Role $role */
            $role = $this->getReference(RoleFixtures::ROLE_REFERENCES[0]);

            ($userRole = new UserRole())
                ->setUser($user)
                ->setRole($role)
                ->setBeginAt(new \DateTime());

            $manager->persist($userRole);

            if($i === 0 ){
                /** @var Role $roleAdmin */
                $roleAdmin = $this->getReference(RoleFixtures::ROLE_REFERENCES[1]);
                ($userRoleAdmin = new UserRole())
                    ->setUser($user)
                    ->setRole($roleAdmin)
                    ->setBeginAt(new \DateTime());

                $manager->persist($userRoleAdmin);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            GroupFixtures::class,
            RoleFixtures::class
        ];
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\BookType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BookTypeFixtures extends Fixture
{
    public const TYPE_REFERENCES = ["Informatique", "Savoir-faire"];
    public function load(ObjectManager $manager)
    {
        foreach(self::TYPE_REFERENCES as $t) {
            ($type = new BookType())->setType($t);
            $this->addReference($t, $type);
            $manager->persist($type);
        }

        $manager->flush();
    }
}

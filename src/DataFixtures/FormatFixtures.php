<?php

namespace App\DataFixtures;

use App\Entity\Format;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class FormatFixtures extends Fixture
{
    public const FORMAT_REFERENCES = ["Book", "Ebook"];
    public function load(ObjectManager $manager)
    {
        foreach(self::FORMAT_REFERENCES as $f) {
            ($format = new Format())->setFormat($f);
            $this->addReference($f, $format);
            $manager->persist($format);
        }

        $manager->flush();
    }
}

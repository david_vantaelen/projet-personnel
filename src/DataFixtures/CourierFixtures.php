<?php

namespace App\DataFixtures;

use App\Entity\Courier;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CourierFixtures extends Fixture
{
    public const COURIER_REFERENCES = ["DHL", "GLS", "UPS"];
    public function load(ObjectManager $manager)
    {
        foreach(self::COURIER_REFERENCES as $c) {
            ($courier = new Courier())
                ->setName($c)
                ->setDescription("[[p]]Livraison par [[b]]`$c`[[/b]][[/p]]")
                ->setImage(str_replace(" ", "_", strtolower($c)).".png");
            $this->addReference($c, $courier);
            $manager->persist($courier);
        }

        $manager->flush();
    }
}
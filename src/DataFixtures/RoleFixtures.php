<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    public const ROLE_REFERENCES = ["ROLE_USER", "ROLE_ADMIN", "ROLE_DELIVERY"];

    public function load(ObjectManager $manager)
    {
        foreach(self::ROLE_REFERENCES as $r) {
            ($role = new Role())->setLabel($r);
            $this->addReference($r, $role);
            $manager->persist($role);
        }

        $manager->flush();
    }
}

<?php


namespace App\Form;


use App\Form\Model\AddressFormModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressEntityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("street", TextType::class, ["required" => true])
            ->add("city", PasswordType::class, ["required" => true])
            ->add("postCode", EmailType::class, ["required" => true])
            ->add("country", TextType::class, ["required" => true])
            ->add("isMain", IntegerType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => AddressFormModel::class
        ]);
    }
}
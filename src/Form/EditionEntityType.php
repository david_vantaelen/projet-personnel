<?php


namespace App\Form;


use App\Form\Model\EditionFormModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditionEntityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("description", TextType::class)
            ->add("price", NumberType::class, ["required" => true])
            ->add("image", FileType::class)
            ->add("stock", IntegerType::class)
            ->add("releaseAt", DateType::class, ["widget" => "single_text", "format" => "yyyy-MM-dd", "html5" => false])
            ->add("bookId", IntegerType::class, ["required" => true])
            ->add("editorId", IntegerType::class, ["required" => true])
            ->add("formatId", IntegerType::class, ["required" => true])
            ->add("collectionId", IntegerType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => EditionFormModel::class
        ]);
    }
}
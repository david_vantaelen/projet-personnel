<?php


namespace App\Form\Model;


use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AuthorFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="AuthorForm")
 */
class AuthorFormModel extends AbstractFormModel
{
    /**
     * @var string|null $firstName
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $firstName;

    /**
     * @var string|null $lastName
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $lastName;

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return AuthorFormModel
     */
    public function setFirstName(?string $firstName): AuthorFormModel
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return AuthorFormModel
     */
    public function setLastName(?string $lastName): AuthorFormModel
    {
        $this->lastName = $lastName;
        return $this;
    }

}
<?php


namespace App\Form\Model;


use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class BookFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="BookForm")
 */
class BookFormModel extends AbstractFormModel
{
    /**
     * @var string|null $title
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $title;

    /**
     * @var integer|null $authorId
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="integer")
     */
    private $authorId;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return BookFormModel
     */
    public function setTitle(?string $title): BookFormModel
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAuthorId(): ?int
    {
        return $this->authorId;
    }

    /**
     * @param int|null $authorId
     * @return BookFormModel
     */
    public function setAuthorId(?int $authorId): BookFormModel
    {
        $this->authorId = $authorId;
        return $this;
    }
}
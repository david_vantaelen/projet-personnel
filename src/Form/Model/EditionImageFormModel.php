<?php


namespace App\Form\Model;


use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class EditionImageFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="EditionImageFormModel")
 */
class EditionImageFormModel extends AbstractFormModel
{
    /**
     * @var mixed|null $image
     * @Assert\File(maxSize = "500k", mimeTypes={"image/jpg", "image/jpeg", "image/gif", "image/png"}, mimeTypesMessage="Please upload a valid image")
     * @OA\Property(type="string", format="binary", description=null)
     */
    private $image;

    /**
     * @return mixed|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed|null $image
     * @return EditionImageFormModel
     */
    public function setImage($image): EditionImageFormModel
    {
        $this->image = $image;
        return $this;
    }

}
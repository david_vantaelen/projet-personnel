<?php


namespace App\Form\Model;


use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class BookFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="CommandForm")
 */
class CommandFormModel extends AbstractFormModel
{
    /**
     * @var integer|null $userId
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="integer")
     */
    private $userId;

    /**
     * @var integer|null $courierId
     * @OA\Property(type="integer")
     */
    private $courierId;

    /**
     * @var integer|null $financialId
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="integer")
     */
    private $financialId;

    /**
     * @var string|null $status
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $status;

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     * @return CommandFormModel
     */
    public function setUserId(?int $userId): CommandFormModel
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCourierId(): ?int
    {
        return $this->courierId;
    }

    /**
     * @param int|null $courierId
     * @return CommandFormModel
     */
    public function setCourierId(?int $courierId): CommandFormModel
    {
        $this->courierId = $courierId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFinancialId(): ?int
    {
        return $this->financialId;
    }

    /**
     * @param int|null $financialId
     * @return CommandFormModel
     */
    public function setFinancialId(?int $financialId): CommandFormModel
    {
        $this->financialId = $financialId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return CommandFormModel
     */
    public function setStatus(?string $status): CommandFormModel
    {
        $this->status = $status;
        return $this;
    }
}
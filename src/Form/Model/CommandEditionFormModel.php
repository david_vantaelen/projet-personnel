<?php


namespace App\Form\Model;


use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class CommandEditionFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="CommandEditionForm")
 */
class CommandEditionFormModel extends AbstractFormModel
{
    /**
     * @var integer|null $quantity
     * @OA\Property(type="integer")
     */
    private $quantity;

    /**
     * @var float|null $price
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="float")
     */
    private $price;

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     * @return CommandEditionFormModel
     */
    public function setQuantity(?int $quantity): CommandEditionFormModel
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     * @return CommandEditionFormModel
     */
    public function setPrice(?float $price): CommandEditionFormModel
    {
        $this->price = $price;
        return $this;
    }
}
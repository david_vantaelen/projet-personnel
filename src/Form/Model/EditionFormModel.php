<?php


namespace App\Form\Model;


use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class EditionFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="EditionForm")
 */
class EditionFormModel extends AbstractFormModel
{
    /**
     * @var integer|null $bookId
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @OA\Property(type="integer", description=null, example="1")
     */
    private $bookId;
    /**
     * @var integer|null $editorId
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @OA\Property(type="integer", description=null, example="1")
     */
    private $editorId;

    /**
     * @var string|null $description
     * @OA\Property(type="string", description=null, example="Une description")
     */
    private $description;

    /**
     * @var float|null $price
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @OA\Property(type="float", description=null, example="20.99")
     */
    private $price;
    /**
     * @var integer|null $formatId
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @OA\Property(type="integer", description=null, example="1")
     */
    private $formatId;
    /**
     * @var integer|null $collectionId
     * @OA\Property(type="integer", description=null, example="1")
     */
    private $collectionId;
    /**
     * @var mixed|null $image
     * @Assert\File(maxSize = "500k", mimeTypes={"image/jpg", "image/jpeg", "image/gif", "image/png"}, mimeTypesMessage="Please upload a valid image")
     * @OA\Property(type="string", format="binary", description=null)
     */
    private $image;
    /**
     * @var integer|null $stock
     * @OA\Property(type="integer", description=null, example="1")
     */
    private $stock;
    /**
     * @var DateTime|null $releaseAt
     * @OA\Property(type="string", format="date-time", description=null, example="2019-08-24")
     */
    private $releaseAt;

    /**
     * @return int|null
     */
    public function getBookId(): ?int
    {
        return $this->bookId;
    }

    /**
     * @param int|null $bookId
     * @return EditionFormModel
     */
    public function setBookId(?int $bookId): EditionFormModel
    {
        $this->bookId = $bookId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getEditorId(): ?int
    {
        return $this->editorId;
    }

    /**
     * @param int|null $editorId
     * @return EditionFormModel
     */
    public function setEditorId(?int $editorId): EditionFormModel
    {
        $this->editorId = $editorId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return EditionFormModel
     */
    public function setDescription(?string $description): EditionFormModel
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     * @return EditionFormModel
     */
    public function setPrice(?float $price): EditionFormModel
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFormatId(): ?int
    {
        return $this->formatId;
    }

    /**
     * @param int|null $formatId
     * @return EditionFormModel
     */
    public function setFormatId(?int $formatId): EditionFormModel
    {
        $this->formatId = $formatId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCollectionId(): ?int
    {
        return $this->collectionId;
    }

    /**
     * @param int|null $collectionId
     * @return EditionFormModel
     */
    public function setCollectionId(?int $collectionId): EditionFormModel
    {
        $this->collectionId = $collectionId;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed|null $image
     * @return EditionFormModel
     */
    public function setImage($image): EditionFormModel
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStock(): ?int
    {
        return $this->stock;
    }

    /**
     * @param int|null $stock
     * @return EditionFormModel
     */
    public function setStock(?int $stock): EditionFormModel
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getReleaseAt(): ?DateTime
    {
        return $this->releaseAt;
    }

    /**
     * @param DateTime|null $releaseAt
     * @return EditionFormModel
     */
    public function setReleaseAt(?DateTime $releaseAt): EditionFormModel
    {
        $this->releaseAt = $releaseAt;
        return $this;
    }
}
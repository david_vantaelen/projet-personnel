<?php


namespace App\Form\Model;


use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class RoleFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="RoleForm")
 */
class RoleFormModel extends AbstractFormModel
{
    /**
     * @var string|null $label
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $label;

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     * @return RoleFormModel
     */
    public function setLabel(?string $label): RoleFormModel
    {
        $this->label = $label;
        return $this;
    }
}
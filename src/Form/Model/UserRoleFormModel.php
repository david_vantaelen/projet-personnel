<?php


namespace App\Form\Model;


use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class UserRoleFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="UserRoleForm")
 */
class UserRoleFormModel extends AbstractFormModel
{
    /**
     * @var DateTime|null $beginAt
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string", format="date-time")
     */
    private $beginAt;

    /**
     * @var DateTime|null $endAt
     * @OA\Property(type="string", format="date-time")
     */
    private $endAt;

    /**
     * @return DateTime|null
     */
    public function getBeginAt(): ?DateTime
    {
        return $this->beginAt;
    }

    /**
     * @param DateTime|null $beginAt
     * @return UserRoleFormModel
     */
    public function setBeginAt(?DateTime $beginAt): UserRoleFormModel
    {
        $this->beginAt = $beginAt;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getEndAt(): ?DateTime
    {
        return $this->endAt;
    }

    /**
     * @param DateTime|null $endAt
     * @return UserRoleFormModel
     */
    public function setEndAt(?DateTime $endAt): UserRoleFormModel
    {
        $this->endAt = $endAt;
        return $this;
    }
}
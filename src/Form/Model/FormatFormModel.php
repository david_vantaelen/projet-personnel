<?php


namespace App\Form\Model;


use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class FormatFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="FormatForm")
 */
class FormatFormModel extends AbstractFormModel
{
    /**
     * @var string|null $format
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $format;

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     * @return FormatFormModel
     */
    public function setFormat(?string $format): FormatFormModel
    {
        $this->format = $format;
        return $this;
    }
}
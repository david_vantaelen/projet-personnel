<?php


namespace App\Form\Model;


use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class EditorFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="CourierForm")
 */
class CourierFormModel extends AbstractFormModel
{
    /**
     * @var string|null $name
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $name;

    /**
     * @var string|null $description
     * @OA\Property(type="string")
     */
    private $description;

    /**
     * @var string|null $image
     * @OA\Property(type="string")
     */
    private $image;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return CourierFormModel
     */
    public function setName(?string $name): CourierFormModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return CourierFormModel
     */
    public function setDescription(?string $description): CourierFormModel
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     * @return CourierFormModel
     */
    public function setImage(?string $image): CourierFormModel
    {
        $this->image = $image;
        return $this;
    }
}
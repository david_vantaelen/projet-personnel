<?php


namespace App\Form\Model;


use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class EditorFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="EditorForm")
 */
class EditorFormModel extends AbstractFormModel
{
    /**
     * @var string|null $editor
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $editor;

    /**
     * @return string|null
     */
    public function getEditor(): ?string
    {
        return $this->editor;
    }

    /**
     * @param string|null $editor
     * @return EditorFormModel
     */
    public function setEditor(?string $editor): EditorFormModel
    {
        $this->editor = $editor;
        return $this;
    }
}
<?php


namespace App\Form\Model;


use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

/**
 * Class RoleFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="AddressForm")
 */
class AddressFormModel extends AbstractFormModel
{
    /**
     * @var string|null $street
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $street;

    /**
     * @var string|null $city
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $city;

    /**
     * @var string|null $postCode
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $postCode;

    /**
     * @var string|null $country
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $country;

    /**
     * @var boolean|null $isMain
     * @OA\Property(type="boolean")
     */
    private $isMain;

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     * @return AddressFormModel
     */
    public function setStreet(?string $street): AddressFormModel
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return AddressFormModel
     */
    public function setCity(?string $city): AddressFormModel
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPostCode(): ?string
    {
        return $this->postCode;
    }

    /**
     * @param string|null $postCode
     * @return AddressFormModel
     */
    public function setPostCode(?string $postCode): AddressFormModel
    {
        $this->postCode = $postCode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return AddressFormModel
     */
    public function setCountry(?string $country): AddressFormModel
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsMain(): ?bool
    {
        return $this->isMain;
    }

    /**
     * @param bool|null $isMain
     * @return AddressFormModel
     */
    public function setIsMain(?bool $isMain): AddressFormModel
    {
        $this->isMain = $isMain;
        return $this;
    }
}
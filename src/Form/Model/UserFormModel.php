<?php


namespace App\Form\Model;


use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;


/**
 * Class UserFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="UserForm")
 */
class UserFormModel extends AbstractFormModel
{

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var string|null $username
     * @OA\Property(type="string")
     */
    private $username;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var string|null $password
     * @OA\Property(type="string")
     */
    private $password;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var string|null $firstName
     * @OA\Property(type="string")
     */
    private $firstName;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var string|null $lastName
     * @OA\Property(type="string")
     */
    private $lastName;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var string|null $email
     * @OA\Property(type="string")
     */
    private $email;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var integer|null $groupId
     * @OA\Property(type="integer")
     */
    private $groupId;

    /**
     * @Assert\Type(type="App\Form\Model\AddressFormModel")
     * @Assert\Valid()
     * @var AddressFormModel|null $address
     * @OA\Property(ref="#/components/schemas/AddressForm")
     */
    private $address;

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return UserFormModel
     */
    public function setUsername(?string $username): UserFormModel
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return UserFormModel
     */
    public function setPassword(?string $password): UserFormModel
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return UserFormModel
     */
    public function setFirstName(?string $firstName): UserFormModel
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return UserFormModel
     */
    public function setLastName(?string $lastName): UserFormModel
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return UserFormModel
     */
    public function setEmail(?string $email): UserFormModel
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGroupId(): ?int
    {
        return $this->groupId;
    }

    /**
     * @param int|null $groupId
     * @return UserFormModel
     */
    public function setGroupId(?int $groupId): UserFormModel
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return AddressFormModel|null
     */
    public function getAddress(): ?AddressFormModel
    {
        return $this->address;
    }

    /**
     * @param AddressFormModel|null $address
     * @return UserFormModel
     */
    public function setAddress(?AddressFormModel $address): UserFormModel
    {
        $this->address = $address;
        return $this;
    }
}
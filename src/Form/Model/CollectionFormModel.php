<?php


namespace App\Form\Model;


use OpenApi\Annotations as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AuthorFormModel
 * @package App\Form\Model
 * @OA\Schema(schema="CollectionForm")
 */
class CollectionFormModel extends AbstractFormModel
{
    /**
     * @var string|null $collection
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @OA\Property(type="string")
     */
    private $collection;

    /**
     * @return string|null
     */
    public function getCollection(): ?string
    {
        return $this->collection;
    }

    /**
     * @param string|null $collection
     * @return CollectionFormModel
     */
    public function setCollection(?string $collection): CollectionFormModel
    {
        $this->collection = $collection;
        return $this;
    }
}
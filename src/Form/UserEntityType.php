<?php


namespace App\Form;


use App\Form\Model\UserFormModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEntityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("username", TextType::class, ["required" => true])
            ->add("password", PasswordType::class, ["required" => true])
            ->add("email", EmailType::class, ["required" => true])
            ->add("firstName", TextType::class, ["required" => true])
            ->add("lastName", TextType::class, ["required" => true])
            ->add("groupId", IntegerType::class, ["required" => true])
            ->add("address", AddressEntityType::class, ["required" => true]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => UserFormModel::class
        ]);
    }
}
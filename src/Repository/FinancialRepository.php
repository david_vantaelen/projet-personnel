<?php

namespace App\Repository;

use App\Entity\Financial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Financial|null find($id, $lockMode = null, $lockVersion = null)
 * @method Financial|null findOneBy(array $criteria, array $orderBy = null)
 * @method Financial|null findOneByName(string $name, array $orderBy = null)
 * @method Financial[]    findAll()
 * @method Financial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FinancialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Financial::class);
    }

    // /**
    //  * @return Financial[] Returns an array of Financial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Financial
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

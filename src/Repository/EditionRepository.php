<?php

namespace App\Repository;

use App\Entity\Edition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Edition|null find($id, $lockMode = null, $lockVersion = null)
 * @method Edition|null findOneBy(array $criteria, array $orderBy = null)
 * @method Edition[]    findAll()
 * @method Edition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Edition::class);
    }

    public function findBySearch(string $search, ?int $limit, ?int $offset)
    {
        $qb = $this->createQueryBuilder('e')
            ->join('e.book', 'b')
            ->where('b.title LIKE :val')
            ->orWhere('e.description LIKE :val')
            ->andWhere('e.isActive = 1')
            ->setParameters(["val" => "%$search%"])
            ->orderBy('e.id', 'ASC');

        if(!is_null($offset)) $qb->setFirstResult($offset);
        if(!is_null($limit)) $qb->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    public function getCount()
    {
        ($qb = $this->createQueryBuilder('e'))
            ->select($qb->expr()->count('e'))
            ->where('e.isActive = 1');

        return $qb->getQuery()->getResult();
    }

    public function getCountBySearch(string $search)
    {
        ($qb = $this->createQueryBuilder('e'))
            ->select($qb->expr()->count('e'))
            ->join('e.book', 'b')
            ->where('b.title LIKE :val')
            ->orWhere('e.description LIKE :val')
            ->andWhere('e.isActive = 1')
            ->setParameters(["val" => "%$search%"]);

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return Edition[] Returns an array of Edition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Edition
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

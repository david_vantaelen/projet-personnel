<?php

namespace App\Repository;

use App\Entity\CommandEdition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CommandEdition|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommandEdition|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommandEdition[]    findAll()
 * @method CommandEdition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandEditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommandEdition::class);
    }

    // /**
    //  * @return CommandEdition[] Returns an array of CommandEdition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommandEdition
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

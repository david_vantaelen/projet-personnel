<?php


namespace App\DTO;


use App\Entity\Author;
use OpenApi\Annotations as OA;

/**
 * Class AuthorDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="AuthorDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="AuthorDTO"
 * )
 */
class AuthorDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string $firstName
     * @OA\Property(type="string", property="first_name")
     */
    private $firstName;
    /**
     * @var string $lastName
     * @OA\Property(type="string", property="last_name")
     */
    private $lastName;

    /**
     * AuthorDTO constructor.
     * @param Author $author
     */
    public function __construct(Author $author)
    {
        parent::__construct($author);
        $this->id = $author->getId();
        $this->firstName = $author->getFirstName();
        $this->lastName = $author->getLastName();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return AuthorDTO
     */
    public function setId(int $id): AuthorDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return AuthorDTO
     */
    public function setFirstName(string $firstName): AuthorDTO
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return AuthorDTO
     */
    public function setLastName(string $lastName): AuthorDTO
    {
        $this->lastName = $lastName;
        return $this;
    }
}
<?php


namespace App\DTO;


use App\Entity\Courier;
use OpenApi\Annotations as OA;


/**
 * Class CourierDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="CourierDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="CourierDTO"
 * )
 */
class CourierDTO extends BaseEntity
{
    /**
     * @var int
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string
     * @OA\Property(type="string")
     */
    private $name;
    /**
     * @var string $description
     * @OA\Property(type="string")
     */
    private $description;
    /**
     * @var string
     * @OA\Property(type="string")
     */
    private $image;

    /**
     * CourierDTO constructor.
     * @param Courier $courier
     */
    public function __construct(Courier $courier)
    {
        parent::__construct($courier);
        $this->id = $courier->getId();
        $this->name = $courier->getName();
        $this->description = $courier->getDescription();
        $this->image = $courier->getImage();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CourierDTO
     */
    public function setId(int $id): CourierDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CourierDTO
     */
    public function setName(string $name): CourierDTO
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return CourierDTO
     */
    public function setDescription(string $description): CourierDTO
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return CourierDTO
     */
    public function setImage(string $image): CourierDTO
    {
        $this->image = $image;
        return $this;
    }
}
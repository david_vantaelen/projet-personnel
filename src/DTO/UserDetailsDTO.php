<?php


namespace App\DTO;


use App\Entity\User;
use OpenApi\Annotations as OA;

/**
 * Class UserDetailsDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="UserDetailsDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/UserDTO")},
 *     description="UserDetailsDTO"
 * )
 */
class UserDetailsDTO extends UserDTO
{

    /**
     * @var AddressDTO[]
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/AddressDTO"))
     */
    private $addresses = [];

    /**
     * @var GroupDTO $group
     * @OA\Property(ref="#/components/schemas/GroupDTO")
     */
    private $group;

    /**
     * @var UserRoleDTOForUser[]
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/UserRoleDTOForUser"))
     */
    private $userRoles = [];

    /**
     * UserDetailsDTO constructor.
     * @param User $user
     * @param array|null $addresses
     * @param array|null $userRoles
     */
    public function __construct(User $user, ?array $addresses, ?array $userRoles)
    {
        parent::__construct($user);
        if(!is_null($user->getId())){
            $this->group = new GroupDTO($user->getGroup());
        }
        if(!is_null($addresses)) $this->addresses = $addresses;
        if(!is_null($userRoles)) $this->userRoles = $userRoles;
    }

    /**
     * @return GroupDTO
     */
    public function getGroup(): GroupDTO
    {
        return $this->group;
    }

    /**
     * @param GroupDTO $group
     * @return UserDetailsDTO
     */
    public function setGroup(GroupDTO $group): UserDetailsDTO
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return AddressDTO[]
     */
    public function getAddresses(): array
    {
        return $this->addresses;
    }

    /**
     * @param AddressDTO[] $addresses
     * @return UserDetailsDTO
     */
    public function setAddresses(array $addresses): UserDetailsDTO
    {
        $this->addresses = $addresses;
        return $this;
    }

    /**
     * @return UserRoleDTOForUser[]
     */
    public function getUserRoles(): array
    {
        return $this->userRoles;
    }

    /**
     * @param UserRoleDTOForUser[] $userRoles
     * @return UserDetailsDTO
     */
    public function setUserRoles(array $userRoles): UserDetailsDTO
    {
        $this->userRoles = $userRoles;
        return $this;
    }
}
<?php


namespace App\DTO;


use App\Entity\Command;
use OpenApi\Annotations as OA;


/**
 * Class CommandDetailsDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="CommandDetailsDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/CommandDTO")},
 *     description="CommandDetailsDTO"
 * )
 */
class CommandDetailsDTO extends CommandDTO
{
    /**
     * @var UserDTO
     * @OA\Property(ref="#/components/schemas/UserDTO")
     */
    private $user;
    /**
     * @var FinancialDTO
     * @OA\Property(ref="#/components/schemas/FinancialDTO")
     */
    private $financial;
    /**
     * @var CourierDTO|null
     * @OA\Property(ref="#/components/schemas/CourierDTO")
     */
    private $courier;
    /**
     * @var CommandEditionDTO[]
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/CommandEditionDTO"))
     */
    private $commandEditions = [];

    /**
     * CommandDetailsDTO constructor.
     * @param Command $command
     * @param CommandEditionDTO[]|null $commandEditions
     */
    public function __construct(Command $command, ?array $commandEditions)
    {
        parent::__construct($command);
        if(!is_null($command->getId())){
            $this->user = new UserDTO($command->getUser());
            $this->financial = new FinancialDTO($command->getFinancial());
            $this->courier = $command->getCourier() ? new CourierDTO($command->getCourier()) : null;
        }

        if(!is_null($commandEditions)) $this->commandEditions = $commandEditions;
    }

    /**
     * @return UserDTO
     */
    public function getUser(): UserDTO
    {
        return $this->user;
    }

    /**
     * @param UserDTO $user
     * @return CommandDetailsDTO
     */
    public function setUser(UserDTO $user): CommandDetailsDTO
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return FinancialDTO
     */
    public function getFinancial(): FinancialDTO
    {
        return $this->financial;
    }

    /**
     * @param FinancialDTO $financial
     * @return CommandDetailsDTO
     */
    public function setFinancial(FinancialDTO $financial): CommandDetailsDTO
    {
        $this->financial = $financial;
        return $this;
    }

    /**
     * @return CourierDTO|null
     */
    public function getCourier(): ?CourierDTO
    {
        return $this->courier;
    }

    /**
     * @param CourierDTO|null $courier
     * @return CommandDetailsDTO
     */
    public function setCourier(?CourierDTO $courier): CommandDetailsDTO
    {
        $this->courier = $courier;
        return $this;
    }

    /**
     * @return CommandEditionDTO[]
     */
    public function getCommandEditions(): array
    {
        return $this->commandEditions;
    }

    /**
     * @param CommandEditionDTO[] $commandEditions
     * @return CommandDetailsDTO
     */
    public function setCommandEditions(array $commandEditions): CommandDetailsDTO
    {
        $this->commandEditions = $commandEditions;
        return $this;
    }
}
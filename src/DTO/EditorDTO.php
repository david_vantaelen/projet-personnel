<?php


namespace App\DTO;


use App\Entity\Editor;
use OpenApi\Annotations as OA;

/**
 * Class EditorDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="EditorDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="EditorDTO"
 * )
 */
class EditorDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string $editor
     * @OA\Property(type="string")
     */
    private $editor;

    /**
     * EditorDTO constructor.
     * @param Editor $editor
     */
    public function __construct(Editor $editor)
    {
        parent::__construct($editor);
        $this->id = $editor->getId();
        $this->editor = $editor->getEditor();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return EditorDTO
     */
    public function setId(int $id): EditorDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getEditor(): string
    {
        return $this->editor;
    }

    /**
     * @param string $editor
     * @return EditorDTO
     */
    public function setEditor(string $editor): EditorDTO
    {
        $this->editor = $editor;
        return $this;
    }
}
<?php


namespace App\DTO;


use App\Entity\Role;
use OpenApi\Annotations as OA;


/**
 * Class RoleDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="RoleDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="RoleDTO"
 * )
 */
class RoleDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string|null
     * @OA\Property(type="string")
     */
    private $label;

    /**
     * RoleDTO constructor.
     * @param Role $role
     */
    public function __construct(Role $role)
    {
        parent::__construct($role);
        $this->id = $role->getId();
        $this->label = $role->getLabel();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return RoleDTO
     */
    public function setId(int $id): RoleDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     * @return RoleDTO
     */
    public function setLabel(?string $label): RoleDTO
    {
        $this->label = $label;
        return $this;
    }
}
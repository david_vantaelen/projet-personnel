<?php


namespace App\DTO;


use App\Entity\UserRole;
use DateTime;
use OpenApi\Annotations as OA;

/**
 * Class UserRoleDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="UserRoleDTOForRole",
 *     description="UserRoleDTOForRole"
 * )
 */
class UserRoleDTOForRole extends AbstractDTO
{
    /**
     * @var UserDTO
     * @OA\Property(ref="#/components/schemas/UserDTO")
     */
    private $user;

    /**
     * @var DateTime $beginAt
     * @OA\Property(type="string", format="date-time", property="begin_at")
     */
    private $beginAt;

    /**
     * @var DateTime $endAt
     * @OA\Property(type="string", format="date-time", property="end_at")
     */
    private $endAt;

    /**
     * UserRoleDTO constructor.
     * @param UserRole $userRole
     */
    public function __construct(UserRole $userRole)
    {
        $this->user = new UserDTO($userRole->getUser());
        $this->beginAt = $userRole->getBeginAt();
        $this->endAt = $userRole->getEndAt();
    }

    /**
     * @return UserDTO
     */
    public function getUser(): UserDTO
    {
        return $this->user;
    }

    /**
     * @param UserDTO $user
     * @return UserRoleDTOForRole
     */
    public function setUser(UserDTO $user): UserRoleDTOForRole
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getBeginAt(): DateTime
    {
        return $this->beginAt;
    }

    /**
     * @param DateTime $beginAt
     * @return UserRoleDTOForRole
     */
    public function setBeginAt(DateTime $beginAt): UserRoleDTOForRole
    {
        $this->beginAt = $beginAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEndAt(): DateTime
    {
        return $this->endAt;
    }

    /**
     * @param DateTime $endAt
     * @return UserRoleDTOForRole
     */
    public function setEndAt(DateTime $endAt): UserRoleDTOForRole
    {
        $this->endAt = $endAt;
        return $this;
    }
}
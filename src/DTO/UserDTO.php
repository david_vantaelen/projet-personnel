<?php


namespace App\DTO;


use App\Entity\User;
use OpenApi\Annotations as OA;


/**
 * Class UserDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="UserDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="UserDTO"
 * )
 */
class UserDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string $firstName
     * @OA\Property(type="string", property="first_name")
     */
    private $firstName;
    /**
     * @var string $lastName
     * @OA\Property(type="string", property="last_name")
     */
    private $lastName;
    /**
     * @var string $username
     * @OA\Property(type="string", property="username")
     */
    private $username;
    /**
     * @var string $email
     * @OA\Property(type="string", property="email")
     */
    private $email;
    /**
     * @var string $password
     * @OA\Property(type="string", property="password")
     */
    private $password;
    /**
     * @var array $roles
     * @OA\Property(type="array", @OA\Items(@OA\Schema(type="string")))
     */
    private $roles;
    /**
     * @var string|null
     * @OA\Property(type="string", property="group_name")
     */
    private $groupName;


    /**
     * UserDTO constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->id = $user->getId();
        $this->firstName = $user->getFirstName();
        $this->lastName = $user->getLastName();
        $this->username = $user->getUsername();
        $this->email = $user->getEmail();
        $this->password = $user->getPassword();
        $this->roles = $user->getRoles();

        if(!is_null($user->getId())){
            $this->groupName = $user->getGroup()->getLabel();
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserDTO
     */
    public function setId(int $id): UserDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return UserDTO
     */
    public function setFirstName(string $firstName): UserDTO
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return UserDTO
     */
    public function setLastName(string $lastName): UserDTO
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return UserDTO
     */
    public function setUsername(string $username): UserDTO
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return UserDTO
     */
    public function setEmail(string $email): UserDTO
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return UserDTO
     */
    public function setPassword(string $password): UserDTO
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     * @return UserDTO
     */
    public function setRoles(array $roles): UserDTO
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGroupName(): ?string
    {
        return $this->groupName;
    }

    /**
     * @param string|null $groupName
     * @return UserDTO
     */
    public function setGroupName(?string $groupName): UserDTO
    {
        $this->groupName = $groupName;
        return $this;
    }
}
<?php


namespace App\DTO;


use App\Entity\Book;
use OpenApi\Annotations as OA;

/**
 * Class BookDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="BookDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="BookDTO"
 * )
 */
class BookDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string $title
     * @OA\Property(type="string")
     */
    private $title;
    /**
     * @var string|null
     * @OA\Property(type="string", property="author_name")
     */
    private $authorName;


    /**
     * BookDTO constructor.
     * @param Book $book
     */
    public function __construct(Book $book)
    {
        parent::__construct($book);
        $this->id = $book->getId();
        $this->title = $book->getTitle();
        if(!is_null($book->getId())) {
            $this->authorName = $book->getAuthor()->getFirstName()." ".$book->getAuthor()->getLastName();

        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return BookDTO
     */
    public function setId(int $id): BookDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return BookDTO
     */
    public function setTitle(string $title): BookDTO
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorName(): ?string
    {
        return $this->$authorName;
    }

    /**
     * @param string|null $author
     * @return BookDTO
     */
    public function setAuthorName(?string $authorName): BookDTO
    {
        $this->$authorName = $authorName;
        return $this;
    }
}
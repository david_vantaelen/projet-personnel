<?php

namespace App\DTO;


use DateTime;
use OpenApi\Annotations as OA;

/**
 * Class BaseEntity
 * @package App\DTO
 * @OA\Schema()
 */
abstract class BaseEntity extends AbstractDTO
{
    /**
     * @var DateTime $createdAt
     * @OA\Property(type="string", format="date-time", property="created_at")
     */
    private $createdAt;
    /**
     * @var DateTime $updatedAt
     * @OA\Property(type="string", format="date-time", property="updated_at")
     */
    private $updatedAt;

    /**
     * BaseEntity constructor.
     * @param \App\Entity\BaseEntity $base
     */
    public function __construct(\App\Entity\BaseEntity $base)
    {
        $this->createdAt = $base->getCreatedAt();
        $this->updatedAt = $base->getUpdatedAt();
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return BaseEntity
     */
    public function setCreatedAt(DateTime $createdAt): BaseEntity
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return BaseEntity
     */
    public function setUpdatedAt(DateTime $updatedAt): BaseEntity
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
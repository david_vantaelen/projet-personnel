<?php


namespace App\DTO;


use App\Entity\UserRole;
use DateTime;
use OpenApi\Annotations as OA;

/**
 * Class UserRoleDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="UserRoleDTOForUser",
 *     description="UserRoleDTOForUser"
 * )
 */
class UserRoleDTOForUser extends AbstractDTO
{
    /**
     * @var RoleDTO
     * @OA\Property(ref="#/components/schemas/RoleDTO")
     */
    private $role;

    /**
     * @var DateTime $beginAt
     * @OA\Property(type="string", format="date-time", property="begin_at")
     */
    private $beginAt;

    /**
     * @var DateTime $endAt
     * @OA\Property(type="string", format="date-time", property="end_at")
     */
    private $endAt;

    /**
     * UserRoleDTO constructor.
     * @param UserRole $userRole
     */
    public function __construct(UserRole $userRole)
    {
        $this->role = new RoleDTO($userRole->getRole());
        $this->beginAt = $userRole->getBeginAt();
        $this->endAt = $userRole->getEndAt();
    }

    /**
     * @return RoleDTO
     */
    public function getRole(): RoleDTO
    {
        return $this->role;
    }

    /**
     * @param RoleDTO $role
     * @return UserRoleDTOForUser
     */
    public function setRole(RoleDTO $role): UserRoleDTOForUser
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getBeginAt(): DateTime
    {
        return $this->beginAt;
    }

    /**
     * @param DateTime $beginAt
     * @return UserRoleDTOForUser
     */
    public function setBeginAt(DateTime $beginAt): UserRoleDTOForUser
    {
        $this->beginAt = $beginAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEndAt(): DateTime
    {
        return $this->endAt;
    }

    /**
     * @param DateTime $endAt
     * @return UserRoleDTOForUser
     */
    public function setEndAt(DateTime $endAt): UserRoleDTOForUser
    {
        $this->endAt = $endAt;
        return $this;
    }
}
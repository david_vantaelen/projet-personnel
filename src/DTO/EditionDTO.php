<?php


namespace App\DTO;


use App\Entity\Edition;
use DateTime;
use OpenApi\Annotations as OA;


/**
 * Class EditionDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="EditionDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="EditionDTO"
 * )
 */
class EditionDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string $title
     * @OA\Property(type="string")
     */
    private $title;
    /**
     * @var string $author
     * @OA\Property(type="string")
     */
    private $author;
    /**
     * @var string $description
     * @OA\Property(type="string")
     */
    private $description;
    /**
     * @var float $price
     * @OA\Property(type="float")
     */
    private $price;
    /**
     * @var string $imagePath
     * @OA\Property(type="string", property="image_path")
     */
    private $imagePath;
    /**
     * @var int $stock
     * @OA\Property(type="integer")
     */
    private $stock;
    /**
     * @var DateTime $releaseAt
     * @OA\Property(type="string", format="date-time", property="release_at")
     */
    private $releaseAt;

    /**
     * EditionDTO constructor.
     * @param Edition $edition
     */
    public function __construct(Edition $edition)
    {
        parent::__construct($edition);
        $this->id = $edition->getId();
        $this->description = $edition->getDescription();
        $this->price = $edition->getPrice();
        $this->imagePath = $edition->getImagePath();
        $this->stock = $edition->getStock();
        $this->releaseAt = $edition->getReleaseAt();

        if(!is_null($edition->getId())){
            $this->title = $edition->getBook()->getTitle();
            $this->author = $edition->getBook()->getAuthor()->getFirstName()." ".$edition->getBook()->getAuthor()->getLastName();
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return EditionDTO
     */
    public function setId(int $id): EditionDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return EditionDTO
     */
    public function setTitle(string $title): EditionDTO
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return EditionDTO
     */
    public function setAuthor(string $author): EditionDTO
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return EditionDTO
     */
    public function setDescription(string $description): EditionDTO
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return EditionDTO
     */
    public function setPrice(float $price): EditionDTO
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagePath(): string
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     * @return EditionDTO
     */
    public function setImagePath(string $imagePath): EditionDTO
    {
        $this->imagePath = $imagePath;
        return $this;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     * @return EditionDTO
     */
    public function setStock(int $stock): EditionDTO
    {
        $this->stock = $stock;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getReleaseAt(): DateTime
    {
        return $this->releaseAt;
    }

    /**
     * @param DateTime $releaseAt
     * @return EditionDTO
     */
    public function setReleaseAt(DateTime $releaseAt): EditionDTO
    {
        $this->releaseAt = $releaseAt;
        return $this;
    }
}
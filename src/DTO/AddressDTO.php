<?php


namespace App\DTO;


use App\Entity\Address;
use OpenApi\Annotations as OA;

/**
 * Class AddressDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="AddressDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="AddressDTO"
 * )
 */
class AddressDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;

    /**
     * @var string $street
     * @OA\Property(type="string")
     */
    private $street;

    /**
     * @var string|null $city
     * @OA\Property(type="string")
     */
    private $city;

    /**
     * @var string $postCode
     * @OA\Property(type="string", property="post_code")
     */
    private $postCode;

    /**
     * @var string $country
     * @OA\Property(type="string")
     */
    private $country;

    /**
     * @var bool $isMain
     * @OA\Property(type="boolean", property="is_main")
     */
    private $isMain;

    /**
     * AddressDTO constructor.
     * @param Address $address
     */
    public function __construct(Address $address)
    {
        parent::__construct($address);
        $this->id = $address->getId();
        $this->street = $address->getStreet();
        $this->city = $address->getCity();
        $this->postCode = $address->getPostCode();
        $this->country = $address->getCountry();
        $this->isMain = $address->getIsMain();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return AddressDTO
     */
    public function setId(int $id): AddressDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return AddressDTO
     */
    public function setStreet(string $street): AddressDTO
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return AddressDTO
     */
    public function setCity(?string $city): AddressDTO
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostCode(): string
    {
        return $this->postCode;
    }

    /**
     * @param string $postCode
     * @return AddressDTO
     */
    public function setPostCode(string $postCode): AddressDTO
    {
        $this->postCode = $postCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return AddressDTO
     */
    public function setCountry(string $country): AddressDTO
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMain(): bool
    {
        return $this->isMain;
    }

    /**
     * @param bool $isMain
     * @return AddressDTO
     */
    public function setIsMain(bool $isMain): AddressDTO
    {
        $this->isMain = $isMain;
        return $this;
    }
}
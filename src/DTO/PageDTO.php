<?php


namespace App\DTO;


use OpenApi\Annotations as OA;

/**
 * Class PageDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="PageDTO",
 *     description="PageDTO"
 * )
 */
class PageDTO
{

    /**
     * @var AbstractDTO[] $data
     */
    private $data;
    /**
     * @var int $perPage
     * @OA\Property(type="integer")
     */
    private $perPage;
    /**
     * @var int $nbPages
     * @OA\Property(type="integer")
     */
    private $nbPages;
    /**
     * @var int $page
     * @OA\Property(type="integer")
     */
    private $page;
    /**
     * @var int $count
     * @OA\Property(type="integer")
     */
    private $count;
    /**
     * @var int $total
     * @OA\Property(type="integer")
     */
    private $total;

    /**
     * AuthorDTO constructor.
     * @param AbstractDTO[] $data
     * @param int $perPage
     * @param int $page
     * @param int $count
     * @param int $total
     */
    public function __construct(
        array $data,
        int $perPage,
        int $page,
        int $count,
        int $total
    )
    {
        $this->data = $data;
        $this->perPage = $perPage;
        $this->page = $page;
        $this->count = $count;
        $this->total = $total;
        $this->nbPages = ceil($this->total / $this->perPage = $perPage);
    }

    /**
     * @return AbstractDTO[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param AbstractDTO[] $data
     * @return PageDTO
     */
    public function setData(array $data): PageDTO
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     * @return PageDTO
     */
    public function setPerPage(int $perPage): PageDTO
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return PageDTO
     */
    public function setPage(int $page): PageDTO
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return PageDTO
     */
    public function setCount(int $count): PageDTO
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     * @return PageDTO
     */
    public function setTotal(int $total): PageDTO
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbPages(): int
    {
        return $this->nbPages;
    }

    /**
     * @param int $nbPages
     * @return PageDTO
     */
    public function setNbPages(int $nbPages): PageDTO
    {
        $this->nbPages = $nbPages;
        return $this;
    }
}
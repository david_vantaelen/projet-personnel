<?php


namespace App\DTO;


use App\Entity\Delivery;

/**
 * Class DeliveryDTO
 * @package App\DTO
 */
class DeliveryDTO extends BaseEntity
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $status;

    /**
     * DeliveryDTO constructor.
     * @param Delivery $delivery
     */
    public function __construct(Delivery $delivery)
    {
        parent::__construct($delivery);
        $this->id = $delivery->getId();
        $this->status = $delivery->getStatus();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DeliveryDTO
     */
    public function setId(int $id): DeliveryDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return DeliveryDTO
     */
    public function setStatus(string $status): DeliveryDTO
    {
        $this->status = $status;
        return $this;
    }
}
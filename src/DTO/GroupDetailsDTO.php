<?php


namespace App\DTO;


use App\Entity\Group;
use OpenApi\Annotations as OA;



/**
 * Class GroupDetailsDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="GroupDetailsDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/GroupDTO")},
 *     description="GroupDetailsDTO"
 * )
 */
class GroupDetailsDTO extends GroupDTO
{
    /**
     * @var RoleDTO[]
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/RoleDTO"))
     */
    private $roles = [];
    /**
     * @var UserDTO[]
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/UserDTO"))
     */
    private $users = [];

    /**
     * GroupDetailsDTO constructor.
     * @param Group $group
     * @param RoleDTO[]|null $roles
     * @param UserDTO[]|null $users
     */
    public function __construct(Group $group, ?array $roles, ?array $users)
    {
        parent::__construct($group);
        if(!is_null($roles)) $this->roles = $roles;
        if(!is_null($users)) $this->users = $users;
    }

    /**
     * @return RoleDTO[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param RoleDTO[] $roles
     * @return GroupDetailsDTO
     */
    public function setRoles(array $roles): GroupDetailsDTO
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return UserDTO[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @param UserDTO[] $users
     * @return GroupDetailsDTO
     */
    public function setUsers(array $users): GroupDetailsDTO
    {
        $this->users = $users;
        return $this;
    }
}
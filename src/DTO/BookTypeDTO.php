<?php


namespace App\DTO;


use App\Entity\BookType;

/**
 * Class BookTypeDTO
 * @package App\DTO
 */
class BookTypeDTO extends BaseEntity
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $type
     */
    private $type;

    /**
     * BookTypeDTO constructor.
     * @param BookType $bookType
     */
    public function __construct(BookType $bookType)
    {
        parent::__construct($bookType);
        $this->id = $bookType->getId();
        $this->type = $bookType->getType();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return BookTypeDTO
     */
    public function setId(int $id): BookTypeDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return BookTypeDTO
     */
    public function setType(string $type): BookTypeDTO
    {
        $this->type = $type;
        return $this;
    }
}
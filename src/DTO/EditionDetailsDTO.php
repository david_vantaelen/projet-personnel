<?php


namespace App\DTO;


use App\Entity\Edition;
use OpenApi\Annotations as OA;

/**
 * Class EditionDetailsDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="EditionDetailsDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/EditionDTO")},
 *     description="EditionDetailsDTO"
 * )
 */
class EditionDetailsDTO extends EditionDTO
{
    /**
     * @var BookDTO
     * @OA\Property(ref="#/components/schemas/BookDTO")
     */
    private $book;
    /**
     * @var FormatDTO
     * @OA\Property(ref="#/components/schemas/FormatDTO")
     */
    private $format;
    /**
     * @var EditorDTO
     * @OA\Property(ref="#/components/schemas/EditorDTO")
     */
    private $editor;
    /**
     * @var CollectionDTO|null
     * @OA\Property(ref="#/components/schemas/CollectionDTO")
     */
    private $collection;


    /**
     * EditionDetailsDTO constructor.
     * @param Edition $edition
     */
    public function __construct(Edition $edition)
    {
        parent::__construct($edition);

        if(!is_null($edition->getId())){
            $this->book = new BookDTO($edition->getBook());
            $this->editor = new EditorDTO($edition->getEditor());
            $this->format = new FormatDTO($edition->getFormat());
            $this->collection = $edition->getCollection() ? new CollectionDTO($edition->getCollection()) : null;
        }
    }

    /**
     * @return BookDTO
     */
    public function getBook(): BookDTO
    {
        return $this->book;
    }

    /**
     * @param BookDTO $book
     * @return EditionDetailsDTO
     */
    public function setBook(BookDTO $book): EditionDetailsDTO
    {
        $this->book = $book;
        return $this;
    }

    /**
     * @return FormatDTO
     */
    public function getFormat(): FormatDTO
    {
        return $this->format;
    }

    /**
     * @param FormatDTO $format
     * @return EditionDetailsDTO
     */
    public function setFormat(FormatDTO $format): EditionDetailsDTO
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return EditorDTO
     */
    public function getEditor(): EditorDTO
    {
        return $this->editor;
    }

    /**
     * @param EditorDTO $editor
     * @return EditionDetailsDTO
     */
    public function setEditor(EditorDTO $editor): EditionDetailsDTO
    {
        $this->editor = $editor;
        return $this;
    }

    /**
     * @return CollectionDTO|null
     */
    public function getCollection(): ?CollectionDTO
    {
        return $this->collection;
    }

    /**
     * @param CollectionDTO|null $collection
     * @return EditionDetailsDTO
     */
    public function setCollection(?CollectionDTO $collection): EditionDetailsDTO
    {
        $this->collection = $collection;
        return $this;
    }
}
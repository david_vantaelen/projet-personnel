<?php


namespace App\DTO;


use App\Entity\Role;
use OpenApi\Annotations as OA;


/**
 * Class RoleDetailsDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="RoleDetailsDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/RoleDTO")},
 *     description="RoleDetailsDTO"
 * )
 */
class RoleDetailsDTO extends RoleDTO
{
    /**
     * @var GroupDTO[]
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/GroupDTO"))
     */
    private $groups = [];
    /**
     * @var UserRoleDTOForRole[]
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/UserRoleDTOForRole"))
     */
    private $userRoles = [];

    /**
     * RoleDetailsDTO constructor.
     * @param Role $role
     * @param GroupDTO[]|null $groups
     * @param UserRoleDTOForRole[]|null $userRoles
     */
    public function __construct(Role $role, ?array $groups, ?array $userRoles)
    {
        parent::__construct($role);
        if(!is_null($groups)) $this->groups = $groups;
        if(!is_null($userRoles)) $this->userRoles = $userRoles;
    }

    /**
     * @return GroupDTO[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param GroupDTO[] $groups
     * @return RoleDetailsDTO
     */
    public function setGroups(array $groups): RoleDetailsDTO
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @return UserRoleDTOForRole[]
     */
    public function getUserRoles(): array
    {
        return $this->userRoles;
    }

    /**
     * @param UserRoleDTOForRole[] $userRoles
     * @return RoleDetailsDTO
     */
    public function setUserRoles(array $userRoles): RoleDetailsDTO
    {
        $this->userRoles = $userRoles;
        return $this;
    }
}
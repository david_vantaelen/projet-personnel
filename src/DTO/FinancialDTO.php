<?php


namespace App\DTO;


use App\Entity\Financial;
use OpenApi\Annotations as OA;


/**
 * Class FinancialDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="FinancialDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="FinancialDTO"
 * )
 */
class FinancialDTO extends BaseEntity
{
    /**
     * @var int
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string
     * @OA\Property(type="string")
     */
    private $name;
    /**
     * @var string $description
     * @OA\Property(type="string")
     */
    private $description;
    /**
     * @var string
     * @OA\Property(type="string")
     */
    private $image;

    /**
     * FinancialDTO constructor.
     * @param Financial $financial
     */
    public function __construct(Financial $financial)
    {
        parent::__construct($financial);
        $this->id = $financial->getId();
        $this->name = $financial->getName();
        $this->description = $financial->getDescription();
        $this->image = $financial->getImage();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FinancialDTO
     */
    public function setId(int $id): FinancialDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FinancialDTO
     */
    public function setName(string $name): FinancialDTO
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return FinancialDTO
     */
    public function setDescription(string $description): FinancialDTO
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return FinancialDTO
     */
    public function setImage(string $image): FinancialDTO
    {
        $this->image = $image;
        return $this;
    }
}
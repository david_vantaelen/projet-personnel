<?php


namespace App\DTO;


use App\Entity\Collection;
use OpenApi\Annotations as OA;

/**
 * Class CollectionDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="CollectionDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="CollectionDTO"
 * )
 */
class CollectionDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string $collection
     * @OA\Property(type="string")
     */
    private $collection;

    /**
     * CollectionDTO constructor.
     * @param Collection $collection
     */
    public function __construct(Collection $collection)
    {
        parent::__construct($collection);
        $this->id = $collection->getId();
        $this->collection = $collection->getCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CollectionDTO
     */
    public function setId(int $id): CollectionDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCollection(): string
    {
        return $this->collection;
    }

    /**
     * @param string $collection
     * @return CollectionDTO
     */
    public function setCollection(string $collection): CollectionDTO
    {
        $this->collection = $collection;
        return $this;
    }
}
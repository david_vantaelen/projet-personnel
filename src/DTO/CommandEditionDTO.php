<?php


namespace App\DTO;


use App\Entity\CommandEdition;
use OpenApi\Annotations as OA;


/**
 * Class CommandEditionDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="CommandEditionDTO",
 *     description="CommandEditionDTO"
 * )
 */
class CommandEditionDTO extends AbstractDTO
{

    /**
     * @var CommandDTO
     * @OA\Property(ref="#/components/schemas/CommandDTO")
     */
    private $command;

    /**
     * @var EditionDTO
     * @OA\Property(ref="#/components/schemas/EditionDTO")
     */
    private $edition;

    /**
     * @var  float $price
     * @OA\Property(type="float")
     */
    private $price;

    /**
     * @var int $quantity
     * @OA\Property(type="integer")
     */
    private $quantity;


    /**
     * CommandEditionDTO constructor.
     * @param CommandEdition $commandEdition
     */
    public function __construct(CommandEdition $commandEdition)
    {
        $this->command = new CommandDTO($commandEdition->getCommand());
        $this->edition = new EditionDTO($commandEdition->getEdition());
        $this->price = $commandEdition->getPrice();
        $this->quantity = $commandEdition->getQuantity();
    }

    /**
     * @return CommandDTO
     */
    public function getCommand(): CommandDTO
    {
        return $this->command;
    }

    /**
     * @param CommandDTO $command
     * @return CommandEditionDTO
     */
    public function setCommand(CommandDTO $command): CommandEditionDTO
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @return EditionDTO
     */
    public function getEdition(): EditionDTO
    {
        return $this->edition;
    }

    /**
     * @param EditionDTO $edition
     * @return CommandEditionDTO
     */
    public function setEdition(EditionDTO $edition): CommandEditionDTO
    {
        $this->edition = $edition;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return CommandEditionDTO
     */
    public function setPrice(float $price): CommandEditionDTO
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return CommandEditionDTO
     */
    public function setQuantity(int $quantity): CommandEditionDTO
    {
        $this->quantity = $quantity;
        return $this;
    }
}
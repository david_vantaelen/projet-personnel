<?php


namespace App\DTO;


use App\Entity\Group;
use OpenApi\Annotations as OA;


/**
 * Class GroupDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="GroupDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="GroupDTO"
 * )
 */
class GroupDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string|null
     * @OA\Property(type="string")
     */
    private $label;

    /**
     * GroupDTO constructor.
     * @param Group $group
     */
    public function __construct(Group $group)
    {
        parent::__construct($group);
        $this->id = $group->getId();
        $this->label = $group->getLabel();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return GroupDTO
     */
    public function setId(int $id): GroupDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     * @return GroupDTO
     */
    public function setLabel(?string $label): GroupDTO
    {
        $this->label = $label;
        return $this;
    }
}
<?php


namespace App\DTO;


use App\Entity\Command;
use OpenApi\Annotations as OA;


/**
 * Class CommandDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="CommandDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="CommandDTO"
 * )
 */
class CommandDTO extends BaseEntity
{
    /**
     * @var int|null
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string|null
     * @OA\Property(type="string")
     */
    private $status;

    /**
     * CommandDTO constructor.
     * @param Command $command
     */
    public function __construct(Command $command)
    {
        parent::__construct($command);
        $this->id = $command->getId();
        $this->status = $command->getStatus();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return CommandDTO
     */
    public function setId(?int $id): CommandDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return CommandDTO
     */
    public function setStatus(?string $status): CommandDTO
    {
        $this->status = $status;
        return $this;
    }
}
<?php


namespace App\DTO;


use App\Entity\Format;
use OpenApi\Annotations as OA;

/**
 * Class FormatDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="FormatDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BaseEntity")},
 *     description="FormatDTO"
 * )
 */
class FormatDTO extends BaseEntity
{
    /**
     * @var int $id
     * @OA\Property(type="integer")
     */
    private $id;
    /**
     * @var string $format
     * @OA\Property(type="string")
     */
    private $format;

    /**
     * FormatDTO constructor.
     * @param Format $format
     */
    public function __construct(Format $format)
    {
        parent::__construct($format);
        $this->id = $format->getId();
        $this->format = $format->getFormat();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return FormatDTO
     */
    public function setId(int $id): FormatDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     * @return FormatDTO
     */
    public function setFormat(string $format): FormatDTO
    {
        $this->format = $format;
        return $this;
    }
}
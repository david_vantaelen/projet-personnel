<?php


namespace App\DTO;


use App\Entity\Book;
use OpenApi\Annotations as OA;

/**
 * Class BookDetailsDTO
 * @package App\DTO
 * @OA\Schema(
 *     schema="BookDetailsDTO",
 *     allOf={@OA\Schema(ref="#/components/schemas/BookDTO")},
 *     description="BookDetailsDTO"
 * )
 */
class BookDetailsDTO extends BookDTO
{
    /**
     * @var GroupDTO $author
     * @OA\Property(ref="#/components/schemas/AuthorDTO")
     */
    private $author;

    /**
     * BookDetailsDTO constructor.
     * @param Book $book
     */
    public function __construct(Book $book)
    {
        parent::__construct($book);
        if(!is_null($book->getId())){
            $this->author = new AuthorDTO($book->getAuthor());
        }
    }

    /**
     * @return AuthorDTO
     */
    public function getGroup(): AuthorDTO
    {
        return $this->author;
    }

    /**
     * @param AuthorDTO $author
     * @return BookDetailsDTO
     */
    public function setGroup(AuthorDTO $author): BookDetailsDTO
    {
        $this->author = $author;
        return $this;
    }
}
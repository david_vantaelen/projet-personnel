<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseEntity implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string $firstName
     * @ORM\Column(name="last_name", type="string", length=63)
     */
    private $firstName;

    /**
     * @var string $lastName
     * @ORM\Column(name="first_name", type="string", length=63)
     */
    private $lastName;

    /**
     * @var string $email
     * @ORM\Column(name="email", type="string", length=127, unique=true)
     */
    private $email;

    /**
     * @var Address $address
     * @ORM\OneToMany(targetEntity="App\Entity\Address", mappedBy="user", fetch="EAGER")
     */
    private $addresses;

    /**
     * @var Group $group
     * @ORM\ManyToOne(targetEntity="App\Entity\Group")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=true)
     */
    private $group;

    /**
     * @var Collection|UserRole[] $userRoles
     * @ORM\OneToMany(targetEntity="App\Entity\UserRole", mappedBy="user", fetch="EAGER")
     */
    private $userRoles;

    public function __construct()
    {
        $this->userRoles = new ArrayCollection();
        $this->addresses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     * @return array - Renvoie au moins ROLE_USER sans doublons
     */
    public function getRoles(): array
    {
        $group_roles = $this->getRolesFromGroup();
        $user_roles = $this->getRolesFromUserRoles();
        return array_values(
            array_unique(
                array_merge($group_roles, $user_roles, ["ROLE_USER"])
            )
        );
    }

    public function getRolesFromGroup(): array {
        if(!$this->group) return [];
        return array_map(function($role){
            /** @var Role $role */
            return $role->getLabel();
        }, $this->group->getRoles()->toArray());
    }

    public function getRolesFromUserRoles(): array {
        if(!$this->userRoles || count($this->userRoles) < 1) return [];
        return array_map(function($userRole){
            /** @var UserRole $userRole */
            return $userRole->getRole()->getLabel();
        }, array_values(
            array_filter(
                $this->getUserRoles()->toArray(), function($userRole){
                    /** @var UserRole $userRole */
                    $endAt = $userRole->getEndAt();
                    return is_null($endAt) || $endAt > new \DateTime();
                }))
        );
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setUser($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
            // set the owning side to null (unless already changed)
            if ($address->getUser() === $this) {
                $address->setUser(null);
            }
        }

        return $this;
    }

    public function getGroup(): ?Group
    {
        return $this->group;
    }

    public function setGroup(?Group $group): self
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return Collection|UserRole[]
     */
    public function getUserRoles(): Collection
    {
        return $this->userRoles;
    }

    public function addUserRole(UserRole $userRole): self
    {
        if (!$this->userRoles->contains($userRole)) {
            $this->userRoles[] = $userRole;
            $userRole->setUser($this);
        }

        return $this;
    }

    public function removeUserRole(UserRole $userRole): self
    {
        if ($this->userRoles->contains($userRole)) {
            $this->userRoles->removeElement($userRole);
            // set the owning side to null (unless already changed)
            if ($userRole->getUser() === $this) {
                $userRole->setUser(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandEditionRepository")
 */
class CommandEdition
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Command", fetch="EAGER")
     * @ORM\JoinColumn(name="command_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $command;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Edition", fetch="EAGER")
     * @ORM\JoinColumn(name="edition_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $edition;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="integer", options={"default": 1})
     */
    private $quantity = 1;

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getCommand(): ?Command
    {
        return $this->command;
    }

    public function setCommand(?Command $command): self
    {
        $this->command = $command;

        return $this;
    }

    public function getEdition(): ?Edition
    {
        return $this->edition;
    }

    public function setEdition(?Edition $edition): self
    {
        $this->edition = $edition;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}

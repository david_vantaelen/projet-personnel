<?php


namespace App\Service;


use App\Entity\Courier;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\CourierFormModel;
use App\Repository\CourierRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class CourierService
 * @package App\Service
 */
class CourierService extends AbstractService
{
    /**
     * CourierService constructor.
     * @param EntityManagerInterface $manager
     * @param CourierRepository $repository
     */
    public function __construct(
        EntityManagerInterface $manager,
        CourierRepository $repository
    )
    {
        parent::__construct($manager, $repository);
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Courier[] f.length > 0 | one or more couriers in array else f.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return  $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Courier|null e !== null | courier exists with id else e = null
     */
    public function getOneById(int $id): ?Courier
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param AbstractFormModel $data !== null && $data instanceof CourierFormModel
     * @throws InvalidArgumentException !($data instanceof CourierFormModel)
     * @throws Exception courier with name already exists
     * @return Courier
     */
    public function post(AbstractFormModel $data): Courier
    {
        if(!($data instanceof CourierFormModel)) throw new InvalidArgumentException('$data must an instance of '.CourierFormModel::class);

        if($this->getRepository()->findOneByName($data->getName())) throw new Exception("name exists");

        ($courier = new Courier())
            ->setName($data->getName())
            ->setDescription($data->getDescription())
            ->setImage($data->getImage());

        $this->getManager()->persist($courier);
        $this->getManager()->flush();

        return $courier;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof CourierFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException !($data instanceof CourierFormModel)
     * @throws Exception courier not found by $id
     * @throws Exception courier with name already exists
     * @return Courier
     */
    public function put(int $id, AbstractFormModel $data): Courier
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof CourierFormModel)) throw new InvalidArgumentException('$data must an instance of '.CourierFormModel::class);

        /** @var Courier $courier */
        $courier = $this->getFromRepository($id);
        if(!$courier) throw new Exception("No Courier (id#$id) found");

        if($courier->getName() !== $data->getName())
            if($this->getRepository()->findOneByName($data->getName())) throw new Exception("name exists");

        $courier
            ->setName($data->getName())
            ->setDescription($data->getDescription())
            ->setImage($data->getImage());

        $this->getManager()->flush();
        return $courier;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception courier not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Courier $courier */
        $courier = $this->getFromRepository($id);
        if(!$courier) throw new Exception("No Courier (id#$id) found");
        $courier->setIsActive(false);
        $courier->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}
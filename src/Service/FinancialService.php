<?php


namespace App\Service;


use App\Entity\Financial;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\FinancialFormModel;
use App\Repository\FinancialRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class FinancialService
 * @package App\Service
 */
class FinancialService extends AbstractService
{
    /**
     * FinancialService constructor.
     * @param EntityManagerInterface $manager
     * @param FinancialRepository $repository
     */
    public function __construct(
        EntityManagerInterface $manager,
        FinancialRepository $repository
    )
    {
        parent::__construct($manager, $repository);
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Financial[] f.length > 0 | one or more financials in array else f.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return  $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Financial|null e !== null | financial exists with id else e = null
     */
    public function getOneById(int $id): ?Financial
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param AbstractFormModel $data !== null && $data instanceof FinancialFormModel
     * @throws InvalidArgumentException !($data instanceof FinancialFormModel)
     * @throws Exception financial with name already exists
     * @return Financial
     */
    public function post(AbstractFormModel $data): Financial
    {
        if(!($data instanceof FinancialFormModel)) throw new InvalidArgumentException('$data must an instance of '.FinancialFormModel::class);

        if($this->getRepository()->findOneByName($data->getName())) throw new Exception("name exists");

        ($financial = new Financial())
            ->setName($data->getName())
            ->setDescription($data->getDescription())
            ->setImage($data->getImage());

        $this->getManager()->persist($financial);
        $this->getManager()->flush();

        return $financial;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof FinancialFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException !($data instanceof FinancialFormModel)
     * @throws Exception financial not found by $id
     * @throws Exception financial with name already exists
     * @return Financial
     */
    public function put(int $id, AbstractFormModel $data): Financial
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof FinancialFormModel)) throw new InvalidArgumentException('$data must an instance of '.FinancialFormModel::class);

        /** @var Financial $financial */
        $financial = $this->getFromRepository($id);
        if(!$financial) throw new Exception("No Financial (id#$id) found");

        if($financial->getName() !== $data->getName())
            if($this->getRepository()->findOneByName($data->getName())) throw new Exception("name exists");

        $financial
            ->setName($data->getName())
            ->setDescription($data->getDescription())
            ->setImage($data->getImage());

        $this->getManager()->flush();
        return $financial;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception financial not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Financial $financial */
        $financial = $this->getFromRepository($id);
        if(!$financial) throw new Exception("No Financial (id#$id) found");
        $financial->setIsActive(false);
        $financial->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}
<?php

namespace App\Service;

use App\Entity\Collection;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\CollectionFormModel;
use App\Repository\CollectionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class CollectionService
 * @package App\Service
 */
class CollectionService extends AbstractService
{
    /**
     * CollectionService constructor.
     * @param EntityManagerInterface $manager
     * @param CollectionRepository $repository
     */
    public function __construct(
        EntityManagerInterface $manager,
        CollectionRepository $repository
    )
    {
        parent::__construct($manager, $repository);
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Collection[] c.length > 0 | one or more collections in array else c.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return  $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Collection|null c !== null | collection exists with id else c = null
     */
    public function getOneById(int $id): ?Collection
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param AbstractFormModel $data !== null && $data instanceof CollectionFormModel
     * @throws InvalidArgumentException !($data instanceof CollectionFormModel)
     * @return Collection
     */
    public function post(AbstractFormModel $data): Collection
    {
        if(!($data instanceof CollectionFormModel)) throw new InvalidArgumentException('$data must an instance of '.CollectionFormModel::class);

        ($collection = new Collection())->setCollection($data->getCollection());

        $this->getManager()->persist($collection);
        $this->getManager()->flush();

        return $collection;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof CollectionFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException  !($data instanceof CollectionFormModel)
     * @throws Exception collection not found by $id
     * @return Collection
     */
    public function put(int $id, AbstractFormModel $data): Collection
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof CollectionFormModel)) throw new InvalidArgumentException('$data must an instance of '.CollectionFormModel::class);

        /** @var Collection $collection */
        $collection = $this->getFromRepository($id);
        if(!$collection) throw new Exception("No Collection (id#$id) found");

        $collection->setCollection($data->getCollection());

        $this->getManager()->flush();
        return $collection;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception collection not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Collection $collection */
        $collection = $this->getFromRepository($id);
        if(!$collection) throw new Exception("No Collection (id#$id) found");
        $collection->setIsActive(false);
        $collection->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}

<?php

namespace App\Service;

use App\Entity\Edition;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\EditionFormModel;
use App\Form\Model\EditionImageFormModel;
use App\Repository\BookRepository;
use App\Repository\CollectionRepository;
use App\Repository\EditionRepository;
use App\Repository\EditorRepository;
use App\Repository\FormatRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class EditionService
 * @package App\Service
 */
class EditionService extends AbstractService
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var BookRepository
     */
    private $bookRepository;

    /**
     * @var EditorRepository
     */
    private $editorRepository;

    /**
     * @var FormatRepository
     */
    private $formatRepository;

    /**
     * @var CollectionRepository
     */
    private $collectionRepository;

    /**
     * EditionService constructor.
     * @param ContainerInterface $container
     * @param EntityManagerInterface $manager
     * @param EditionRepository $repository
     * @param BookRepository $bookRepository
     * @param EditorRepository $editorRepository
     * @param FormatRepository $formatRepository
     * @param CollectionRepository $collectionRepository
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $manager,
        EditionRepository $repository,
        BookRepository $bookRepository,
        EditorRepository $editorRepository,
        FormatRepository $formatRepository,
        CollectionRepository $collectionRepository
    )
    {
        parent::__construct($manager, $repository);
        $this->container = $container;
        $this->bookRepository = $bookRepository;
        $this->editorRepository = $editorRepository;
        $this->formatRepository = $formatRepository;
        $this->collectionRepository = $collectionRepository;
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Edition[] e.length > 0 | one or more editions in array else e.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");
        return $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @return int number of editions
     */
    public function getCount(): int
    {
        return $this->getRepository()->getCount()[0][1];
    }

    /**
     * @param string $search != null && trim($search) !== ""
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @throws InvalidArgumentException | $search is empty
     * @return Edition[] e.length > 0 | one or more editions in array else e.length = 0
     */
    public function getSearch(string $search, ?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");
        if(trim($search) === "") throw new InvalidArgumentException("search mustn't be empty");
        return $this->getRepository()->findBySearch($search, $limit, $offset);
    }

    /**
     * @param string $search != null && trim($search) !== ""
     * @throws InvalidArgumentException | $search is empty
     * @return int number of editions
     */
    public function getCountBySearch(string $search): int
    {
        if(trim($search) === "") throw new InvalidArgumentException("search mustn't be empty");
        return $this->getRepository()->getCountBySearch($search)[0][1];
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Edition|null e !== null | edition exists with id else e = null
     */
    public function getOneById(int $id): ?Edition
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param AbstractFormModel $data instanceof EditionFormModel
     * @throws InvalidArgumentException !($data instanceof EditionFormModel)
     * @throws Exception book not found by bookId in $data
     * @throws Exception editor not found by editorId in $data
     * @throws Exception format not found by formatId in $data
     * @throws Exception collection not found by collectionId in $data
     * @return Edition
     */
    public function post(AbstractFormModel $data): Edition
    {
        if(!($data instanceof EditionFormModel)) throw new InvalidArgumentException('$data must an instance of '.EditionFormModel::class);

        $book = $this->bookRepository->findOneBy(["id" => $data->getBookId()]);
        if(!$book) throw new Exception("No Book (id#".$data->getBookId().") found");

        $editor = $this->editorRepository->findOneBy(["id" => $data->getEditorId()]);
        if(!$editor) throw new Exception("No Editor (id#".$data->getEditorId().") found");

        $format = $this->formatRepository->findOneBy(["id" => $data->getFormatId()]);
        if(!$format) throw new Exception("No Format (id#".$data->getFormatId().") found");
        if(!is_null($data->getCollectionId())){
            $collection = $this->collectionRepository->findOneBy(["id" => $data->getCollectionId()]);
            if(!$collection) throw new Exception("No Collection (id#".$data->getCollectionId().") found");
        }

        if ($data->getImage()) {
            /** @var UploadedFile $file */
            $file = $data->getImage();
            $filename = md5(uniqid()).".".$file->guessExtension();

            $file->move(
                $this->container->getParameter('editions_directory'),
                $filename
            );

            $data->setImage($filename);
        }
        ($edition = new Edition())
            ->setDescription($data->getDescription())
            ->setStock($data->getStock())
            ->setPrice($data->getPrice())
            ->setReleaseAt($data->getReleaseAt())
            ->setImagePath($data->getImage())
            ->setBook($book)
            ->setEditor($editor)
            ->setFormat($format);

        if(isset($collection)) $edition->setCollection($collection);
        $this->getManager()->persist($edition);
        $this->getManager()->flush();
        return $edition;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof EditionFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException !($data instanceof EditionFormModel)
     * @throws Exception edition not found by $id
     * @throws Exception book not found by bookId in $data
     * @throws Exception editor not found by editorId in $data
     * @throws Exception format not found by formatId in $data
     * @throws Exception collection not found by collectionId in $data
     * @return Edition
     */
    public function put(int $id, AbstractFormModel $data): Edition
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof EditionFormModel)) throw new InvalidArgumentException('$data must an instance of '.EditionFormModel::class);

        /** @var Edition $edition */
        $edition = $this->getFromRepository($id);
        if(!$edition) throw new Exception("No Edition (id#$id) found");

        $book = $this->bookRepository->findOneBy(["id" => $data->getBookId()]);
        if(!$book) throw new Exception("No Book (id#".$data->getBookId().") found");

        $editor = $this->editorRepository->findOneBy(["id" => $data->getEditorId()]);
        if(!$editor) throw new Exception("No Editor (id#".$data->getEditorId().") found");

        $format = $this->formatRepository->findOneBy(["id" => $data->getFormatId()]);
        if(!$format) throw new Exception("No Format (id#".$data->getFormatId().") found");

        if(!is_null($data->getCollectionId())){
            $collection = $this->collectionRepository->findOneBy(["id" => $data->getCollectionId()]);
            if(!$collection) throw new Exception("No Collection (id#".$data->getCollectionId().") found");
        }

        $edition
            ->setDescription($data->getDescription())
            ->setStock($data->getStock())
            ->setPrice($data->getPrice())
            ->setReleaseAt($data->getReleaseAt())
            ->setBook($book)
            ->setEditor($editor)
            ->setFormat($format);

        if(isset($collection)) $edition->setCollection($collection);
        else if(is_null($data->getCollectionId()))  $edition->setCollection(null);

        $this->getManager()->flush();
        return $edition;
    }

    /**
     * @param int $id != null && $id > 0
     * @param EditionImageFormModel $data
     * @throws InvalidArgumentException $id < 1
     * @throws Exception edition not found by $id
     * @return Edition
     */
    public function changeImage(int $id, EditionImageFormModel $data): Edition
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Edition $edition */
        $edition = $this->getFromRepository($id);
        if(!$edition) throw new Exception("No Edition (id#$id) found");

        /** @var UploadedFile $image */
        $image = $data->getImage();
        $filename = md5(uniqid()).".".$image->guessExtension();

        $image->move(
            $this->container->getParameter('editions_directory'),
            $filename
        );

        $edition->setImagePath($filename);

        $this->getManager()->flush();
        return $edition;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception edition not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Edition $edition */
        $edition = $this->getFromRepository($id);
        if(!$edition) throw new Exception("No Edition (id#$id) found");
        $edition->setIsActive(false);
        $edition->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}

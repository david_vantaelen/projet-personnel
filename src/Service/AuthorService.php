<?php

namespace App\Service;

use App\Entity\Author;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\AuthorFormModel;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class AuthorService
 * @package App\Service
 */
class AuthorService extends AbstractService
{
    /**
     * AuthorService constructor.
     * @param EntityManagerInterface $manager
     * @param AuthorRepository $repository
     */
    public function __construct(
        EntityManagerInterface $manager,
        AuthorRepository $repository
    )
    {
        parent::__construct($manager, $repository);
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Author[] a.length > 0 | one or more authors in array else a.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return  $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Author|null a !== null | author exists with id else a = null
     */
    public function getOneById(int $id): ?Author
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param AbstractFormModel $data !== null && $data instanceof AuthorFormModel
     * @throws InvalidArgumentException !($data instanceof AuthorFormModel)
     * @return Author
     */
    public function post(AbstractFormModel $data): Author
    {
        if(!($data instanceof AuthorFormModel)) throw new InvalidArgumentException('$data must an instance of '.AuthorFormModel::class);

        ($author = new Author())
            ->setFirstName($data->getFirstName())
            ->setLastName($data->getLastName());

        $this->getManager()->persist($author);
        $this->getManager()->flush();

        return $author;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof AuthorFormModel
     * @throws InvalidArgumentException $id < 1
     * @throws InvalidArgumentException !($data instanceof AuthorFormModel)
     * @throws Exception author not found by $id
     * @return Author
     */
    public function put(int $id, AbstractFormModel $data): Author
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof AuthorFormModel)) throw new InvalidArgumentException('$data must an instance of '.AuthorFormModel::class);

        /** @var Author $author */
        $author = $this->getFromRepository($id);
        if(!$author) throw new Exception("No Author (id#$id) found");

        $author
            ->setFirstName($data->getFirstName())
            ->setLastName($data->getLastName());

        $this->getManager()->flush();
        return $author;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception author not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Author $author */
        $author = $this->getFromRepository($id);
        if(!$author) throw new Exception("No Author (id#$id) found");
        $author->setIsActive(false);
        $author->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}

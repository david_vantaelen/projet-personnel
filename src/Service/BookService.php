<?php

namespace App\Service;

use App\Entity\Book;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\BookFormModel;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class BookService
 * @package App\Service
 */
class BookService extends AbstractService
{
    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    /**
     * BookService constructor.
     * @param EntityManagerInterface $manager
     * @param BookRepository $repository
     * @param AuthorRepository $authorRepository
     */
    public function __construct(
        EntityManagerInterface $manager,
        BookRepository $repository,
        AuthorRepository $authorRepository)
    {
        parent::__construct($manager, $repository);
        $this->authorRepository = $authorRepository;
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Book[] b.length > 0 | one or more books in array else b.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Book|null b !== null | book exists with id else b = null
     */
    public function getOneById(int $id): ?Book
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param AbstractFormModel $data !== null && $data instanceof BookFormModel
     * @throws InvalidArgumentException !($data instanceof BookFormModel)
     * @throws Exception author not found by authorId in $data
     * @return Book
     */
    public function post(AbstractFormModel $data): Book
    {
        if(!($data instanceof BookFormModel)) throw new InvalidArgumentException('$data must an instance of '.BookFormModel::class);

        $author = $this->authorRepository->findOneBy(["id" => $data->getAuthorId(), "isActive" => true]);
        if(!$author) throw new Exception("No Author (id#".$data->getAuthorId().") found");

        ($book = new Book())
            ->setTitle($data->getTitle())
            ->setAuthor($author);

        $this->getManager()->persist($book);
        $this->getManager()->flush();

        return $book;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof BookFormModel
     * @throws InvalidArgumentException $id < 1
     * @throws InvalidArgumentException !($data instanceof BookFormModel)
     * @throws Exception book not found by $id
     * @throws Exception author not found by authorId in $data
     * @return Book
     */
    public function put(int $id, AbstractFormModel $data): Book
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof BookFormModel)) throw new InvalidArgumentException('$data must an instance of '.BookFormModel::class);

        /** @var Book $book */
        $book = $this->getFromRepository($id);
        if(!$book) throw new Exception("No Book (id#$id) found");

        $author = $this->authorRepository->findOneBy(["id" => $data->getAuthorId()]);
        if(!$author) throw new Exception("No Author (id#".$data->getAuthorId().") found");

        $book
            ->setTitle($data->getTitle())
            ->setAuthor($author);

        $this->getManager()->flush();
        return $book;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception book not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Book $book */
        $book = $this->getFromRepository($id);
        if(!$book) throw new Exception("No Book (id#$id) found");
        $book->setIsActive(false);
        $book->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}

<?php

namespace App\Service;

use App\Entity\Address;
use App\Entity\User;
use App\Entity\UserRole;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\UserFormModel;
use App\Form\Model\UserRoleFormModel;
use App\Repository\AddressRepository;
use App\Repository\GroupRepository;
use App\Repository\RoleRepository;
use App\Repository\UserRoleRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Exception;

/**
 * Class UserService
 * @package App\Service
 */
class UserService extends AbstractService
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var UserRoleRepository
     */
    private $userRoleRepository;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $manager
     * @param UserRepository $repository
     * @param UserPasswordEncoderInterface $encoder
     * @param AddressRepository $addressRepository
     * @param GroupRepository $groupRepository
     * @param RoleRepository $roleRepository
     * @param UserRoleRepository $userRoleRepository
     */
    public function __construct(
        EntityManagerInterface $manager,
        UserRepository $repository,
        UserPasswordEncoderInterface $encoder,
        AddressRepository $addressRepository,
        GroupRepository $groupRepository,
        RoleRepository $roleRepository,
        UserRoleRepository $userRoleRepository
    )
    {
        parent::__construct($manager, $repository);
        $this->encoder = $encoder;
        $this->addressRepository = $addressRepository;
        $this->groupRepository = $groupRepository;
        $this->roleRepository = $roleRepository;
        $this->userRoleRepository = $userRoleRepository;
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return User[] u.length > 0 | one or more users in array else u.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return User|null u !== null | user exists with id else u = null
     */
    public function getOneById(int $id): ?User
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var User $user */
        return $this->getFromRepository($id);
    }

    /**
     * @param string $value
     * @return User|null u !== null | user exists with id else u = null
     */
    public function getOneByUsername(string $value): ?User
    {
        /** @var User $user */
        return $this->getRepository()->findOneBy(["isActive" => true, "username" => $value]);
    }

    /**
     * @param string $value
     * @return bool
     */
    public function hasUsername(string $value): bool
    {
        return $this->getRepository()->findOneByUsername($value) ? true : false;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function hasEmail(string $value): bool
    {
        return $this->getRepository()->findOneByEmail($value) ? true : false;
    }

    /**
     * @param string $value
     * @return User|null u !== null | user exists with id else u = null
     */
    public function getOneByEmail(string $value): ?User
    {
        /** @var User $user */
        return $this->getRepository()->findOneBy(["isActive" => true, "email" => $value]);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return UserRole[] ur.length > 0 | one or more userRoles in array else ur.length = 0
     */
    public function getUserRoles(int $id): array
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return array_values(
            array_filter($this->userRoleRepository->findBy(["user" => $id]), function($userRole) {
                /** @var UserRole $userRole */
                return $userRole->getRole()->getIsActive() && $userRole->getUser()->getIsActive();
            })
        );
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Address[] a.length > 0 | one or more addresses in array else a.length = 0
     */
    public function getAddresses(int $id): array
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return array_values(
            array_filter($this->addressRepository->findBy(["user" => $id]), function($address) {
                /** @var Address $address */
                return $address->getIsActive();
            })
        );
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $role_id != null && $id > 0
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $role_id < 1
     * @return UserRole|null ur !== null | userRole exists with ids else ur = null
     */
    public function getOneUserRole(int $id, int $role_id): ?UserRole
    {
        if($id < 1 || $role_id < 1) throw new InvalidArgumentException("id must be greater than 0");
        $userRole = $this->userRoleRepository->findOneBy(["user" => $id, "role" => $role_id]);
        return $userRole && $userRole->getRole()->getIsActive()
        && $userRole->getUser()->getIsActive() ? $userRole : null;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Address|null a !== null | main address exists with id else a = null
     */
    public function getMainAddress(int $id): ?Address
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->addressRepository->findOneBy(["isActive" => true, "isMain" => true, "user" => $id]);
    }

    /**
     * @param AbstractFormModel $data instanceof UserFormModel
     * @throws InvalidArgumentException !($data instanceof UserFormModel)
     * @throws Exception group not found by groupId in $data
     * @throws Exception username exists
     * @throws Exception email exists
     * @return User
     */
    public function post(AbstractFormModel $data): User
    {
        if(!($data instanceof UserFormModel)) throw new InvalidArgumentException('$data must an instance of '.UserFormModel::class);

        $group = $this->groupRepository->findOneBy(["id" => $data->getGroupId(), "isActive" => true]);
        if(!$group) throw new Exception("No Group (id#".$data->getGroupId().") found");

        if($this->getRepository()->findOneByUsername($data->getUsername())) throw new Exception("Username exists");
        if($this->getRepository()->findOneByEmail($data->getEmail())) throw new Exception("Email exists");

        ($user = new User())
            ->setUsername($data->getUsername())
            ->setEmail($data->getEmail())
            ->setPassword($this->encoder->encodePassword($user, $data->getPassword()))
            ->setFirstName($data->getFirstName())
            ->setLastName($data->getLastName())
            ->setGroup($group);

        $this->getManager()->persist($user);

        ($address = new Address())
            ->setStreet($data->getAddress()->getStreet())
            ->setCity($data->getAddress()->getCity())
            ->setPostCode($data->getAddress()->getPostCode())
            ->setCountry($data->getAddress()->getCountry())
            ->setIsMain($data->getAddress()->getIsMain())
            ->setUser($user);

        $this->getManager()->persist($address);
        $this->getManager()->flush();

        return $user;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof UserFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException !($data instanceof PutUserFormModel)
     * @throws Exception user not found by $id
     * @throws Exception group not found by groupId in $data
     * @throws Exception no main address
     * @throws Exception username exists
     * @throws Exception email exists
     * @return User
     */
    public function put(int $id, AbstractFormModel $data): User
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof UserFormModel)) throw new InvalidArgumentException('$data must an instance of '.UserFormModel::class);

        /** @var User $user */
        $user = $this->getFromRepository($id);
        if(!$user) throw new Exception("No User (id#$id) found");

        $group = $this->groupRepository->findOneBy(["id" => $data->getGroupId(), "isActive" => true]);
        if(!$group) throw new Exception("No Group (id#".$data->getGroupId().") found");

        $address = $this->getMainAddress($id);
        if(!$address) throw new Exception("No main Address found in user");

        if($user->getUsername() !== $data->getUsername())
            if($this->getRepository()->findOneByUsername($data->getUsername())) throw new Exception("Username exists");

        if($user->getEmail() !== $data->getEmail())
            if($this->getRepository()->findOneByEmail($data->getEmail())) throw new Exception("Email exists");

        $user
            ->setUsername($data->getUsername())
            ->setEmail($data->getEmail())
            ->setPassword($data->getPassword())
            ->setFirstName($data->getFirstName())
            ->setLastName($data->getLastName())
            ->setGroup($group);

        $address
            ->setStreet($data->getAddress()->getStreet())
            ->setCity($data->getAddress()->getCity())
            ->setPostCode($data->getAddress()->getPostCode())
            ->setCountry($data->getAddress()->getCountry())
            ->setIsMain($data->getAddress()->getIsMain());

        $this->getManager()->flush();
        return $user;
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $role_id != null && $role_id > 0
     * @param AbstractFormModel $data instanceof UserRoleFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $role_id < 1
     * @throws InvalidArgumentException !($data instanceof UserRoleFormModel)
     * @throws Exception user not found by $id
     * @throws Exception role not found by $role_id
     * @throws Exception user has already the role
     * @return UserRole
     */
    public function addRole(int $id, int $role_id, AbstractFormModel $data): UserRole
    {
        if($id < 1 || $role_id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof UserRoleFormModel)) throw new InvalidArgumentException('$data must an instance of '.UserRoleFormModel::class);

        /** @var User $user */
        $user = $this->getFromRepository($id);
        if(!$user) throw new Exception("No User (id#$id) found");

        $role = $this->roleRepository->findOneBy(["id" => $role_id, "isActive" => true]);
        if(!$role) throw new Exception("No Role (id#$role_id) found");

        $userRole = $this->userRoleRepository->findOneBy(["user" => $id, "role" => $role_id]);
        if($userRole) throw new Exception("User (id#$id) has already Role (id#$role_id)");

        $userRole = (new UserRole())
            ->setUser($user)
            ->setRole($role)
            ->setBeginAt($data->getBeginAt())
            ->setEndAt($data->getEndAt());

        $user->addUserRole($userRole);

        $this->getManager()->persist($userRole);
        $this->getManager()->flush();
        return $userRole;
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $role_id != null && $role_id > 0
     * @param AbstractFormModel $data instanceof UserRoleFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $role_id < 1
     * @throws InvalidArgumentException !($data instanceof UserRoleFormModel)
     * @throws Exception user not found by $id
     * @throws Exception role not found by $role_id
     * @throws Exception user hasn't the role
     * @return UserRole
     */
    public function editRole(int $id, int $role_id, AbstractFormModel $data): UserRole
    {
        if($id < 1 || $role_id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof UserRoleFormModel)) throw new InvalidArgumentException('$data must an instance of '.UserRoleFormModel::class);

        /** @var User $user */
        $user = $this->getFromRepository($id);
        if(!$user) throw new Exception("No User (id#$id) found");

        $role = $this->roleRepository->findOneBy(["id" => $role_id, "isActive" => true]);
        if(!$role) throw new Exception("No Role (id#$role_id) found");

        $userRole = $this->userRoleRepository->findOneBy(["user" => $id, "role" => $role_id]);
        if(!$userRole) throw new Exception("No Role (id#$role_id) in User (id#$id) found");

        $userRole
            ->setBeginAt($data->getBeginAt())
            ->setEndAt($data->getEndAt());

        $this->getManager()->flush();
        return $userRole;
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $role_id != null && $role_id > 0
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $role_id < 1
     * @throws Exception user not found by $id
     * @throws Exception role not found by $role_id
     * @throws Exception user hasn't the role
     */
    public function removeRole(int $id, int $role_id)
    {
        if($id < 1 || $role_id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var User $user */
        $user = $this->getFromRepository($id);
        if(!$user) throw new Exception("No User (id#$id) found");

        $role = $this->roleRepository->findOneBy(["id" => $role_id, "isActive" => true]);
        if(!$role) throw new Exception("No Role (id#$role_id) found");

        $userRole = $this->userRoleRepository->findOneBy(["user" => $id, "role" => $role_id]);
        if(!$userRole) throw new Exception("No Role (id#".$role_id.") in User (id#".$id.")");

        $this->getManager()->remove($userRole);
        $this->getManager()->flush();
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception user not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var User $user */
        $user = $this->getFromRepository($id);
        if(!$user) throw new Exception("No User (id#$id) found");
        $user->setIsActive(false);
        $user->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}

<?php

namespace App\Service;

use App\Entity\Group;
use App\Entity\Role;
use App\Entity\User;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\GroupFormModel;
use App\Repository\GroupRepository;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class GroupService
 * @package App\Service
 */
class GroupService extends AbstractService
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * GroupService constructor.
     * @param EntityManagerInterface $manager
     * @param GroupRepository $repository
     * @param RoleRepository $roleRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        EntityManagerInterface $manager,
        GroupRepository $repository,
        RoleRepository $roleRepository,
        UserRepository $userRepository)
    {
        parent::__construct($manager, $repository);
        $this->roleRepository = $roleRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Group[] g.length > 0 | one or more groups in array else g.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Group|null g !== null | group exists with id else g = null
     */
    public function getOneById(int $id): ?Group
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param string $value
     * @return Group|null g !== null | group exists with name else g = null
     */
    public function getOneByGroup(string $value): ?Group
    {
        return $this->getRepository()->findOneBy(["isActive" => true, "label" => $value]);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return User[] u.length > 0 | one or more users in array else u.length = 0
     */
    public function getUsers(int $id): array
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->userRepository->findBy(["group" => $id, "isActive" => true]);
    }

    /**
     * @param AbstractFormModel $data instanceof GroupFormModel
     * @throws InvalidArgumentException !($data instanceof GroupFormModel)
     * @throws Exception group with label already exists
     * @return Group
     */
    public function post(AbstractFormModel $data): Group
    {
        if(!($data instanceof GroupFormModel)) throw new InvalidArgumentException('$data must an instance of '.GroupFormModel::class);

        if($this->getRepository()->findOneByLabel($data->getLabel())) throw new Exception("label exists");

        ($group = new Group())
            ->setLabel($data->getLabel());

        $this->getManager()->persist($group);
        $this->getManager()->flush();

        return $group;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof GroupFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException !($data instanceof GroupFormModel)
     * @throws Exception group not found by $id
     * @throws Exception group with label already exists
     * @return Group
     */
    public function put(int $id, AbstractFormModel $data): Group
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof GroupFormModel)) throw new InvalidArgumentException('$data must an instance of '.GroupFormModel::class);

        /** @var Group $group */
        $group = $this->getFromRepository($id);
        if(!$group) throw new Exception("No Group (id#$id) found");

        if($group->getLabel() !== $data->getLabel())
            if($this->getRepository()->findOneByLabel($data->getLabel())) throw new Exception("label exists");

        $group
            ->setLabel($data->getLabel());

        $this->getManager()->flush();
        return $group;
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $role_id != null && $role_id > 0
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $role_id < 1
     * @throws Exception group not found by $id
     * @throws Exception role not found by $role_id
     * @throws Exception group has already role
     * @return Group
     */
    public function addRole(int $id, int $role_id): Group
    {
        if($id < 1 || $role_id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Group $group */
        $group = $this->getFromRepository($id);
        if(!$group) throw new Exception("No Group (id#$id) found");

        $role = $this->roleRepository->findOneBy(["id" => $role_id, "isActive" => true]);
        if(!$role) throw new Exception("No Role (id#$role_id) found");

        foreach($group->getRoles()->toArray() as $r) {
            /** @var Role $r */
            if($r->getId() === $role->getId()) throw new Exception("Group (id#$id) has already Role (id#$role_id)");
        }

        $group->addRole($role);
        $this->getManager()->flush();

        return $this->getOneById($id);
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $role_id != null && $role_id > 0
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $role_id < 1
     * @throws Exception group not found by $id
     * @throws Exception role not found by $role_id
     * @throws Exception group hasn't the role
     */
    public function removeRole(int $id, int $role_id)
    {
        if($id < 1 || $role_id < 1) throw new InvalidArgumentException("id must be greater than 0");

        $hasRole = false;

        /** @var Group $group */
        $group = $this->getFromRepository($id);
        if(!$group) throw new Exception("No Group (id#$id) found");

        $role = $this->roleRepository->findOneBy(["id" => $role_id, "isActive" => true]);
        if(!$role) throw new Exception("No Role (id#$role_id) found");

        foreach($group->getRoles()->toArray() as $r) {
            /** @var Role $r */
            $hasRole = $r->getId() === $role->getId() ? true : $hasRole;
        }

        if(!$hasRole) throw new Exception("No Role (id#".$role_id.") in Group (id#".$id.")");

        $group->removeRole($role);

        $this->getManager()->flush();
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception group not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Group $group */
        $group = $this->getFromRepository($id);
        if(!$group) throw new Exception("No Group (id#$id) found");
        $group->setIsActive(false);
        $group->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}
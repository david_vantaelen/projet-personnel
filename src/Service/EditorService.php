<?php


namespace App\Service;


use App\Entity\Editor;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\EditorFormModel;
use App\Repository\EditorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class EditorService
 * @package App\Service
 */
class EditorService extends AbstractService
{
    /**
     * EditorService constructor.
     * @param EntityManagerInterface $manager
     * @param EditorRepository $repository
     */
    public function __construct(
        EntityManagerInterface $manager,
        EditorRepository $repository
    )
    {
        parent::__construct($manager, $repository);
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Editor[] e.length > 0 | one or more editors in array else e.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return  $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Editor|null e !== null | editor exists with id else e = null
     */
    public function getOneById(int $id): ?Editor
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param AbstractFormModel $data !== null && $data instanceof EditorFormModel
     * @throws InvalidArgumentException !($data instanceof EditorFormModel)
     * @return Editor
     */
    public function post(AbstractFormModel $data): Editor
    {
        if(!($data instanceof EditorFormModel)) throw new InvalidArgumentException('$data must an instance of '.EditorFormModel::class);

        ($editor = new Editor())->setEditor($data->getEditor());

        $this->getManager()->persist($editor);
        $this->getManager()->flush();

        return $editor;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof EditorFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException !($data instanceof EditorFormModel)
     * @throws Exception editor not found by $id
     * @return Editor
     */
    public function put(int $id, AbstractFormModel $data): Editor
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof EditorFormModel)) throw new InvalidArgumentException('$data must an instance of '.EditorFormModel::class);

        /** @var Editor $editor */
        $editor = $this->getFromRepository($id);
        if(!$editor) throw new Exception("No Editor (id#$id) found");

        $editor->setEditor($data->getEditor());

        $this->getManager()->flush();
        return $editor;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception editor not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Editor $editor */
        $editor = $this->getFromRepository($id);
        if(!$editor) throw new Exception("No Editor (id#$id) found");
        $editor->setIsActive(false);
        $editor->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}
<?php

namespace App\Service;

use App\DTO\AbstractDTO;
use App\Form\Model\AbstractFormModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Class AbstractService
 * @package App\Service
 */
abstract class AbstractService
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var ServiceEntityRepositoryInterface
     */
    private $repository;

    /**
     * AbstractService constructor.
     * @param EntityManagerInterface $manager
     * @param ServiceEntityRepositoryInterface $repository
     */
    public function __construct(
        EntityManagerInterface $manager,
        ServiceEntityRepositoryInterface $repository)
    {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    abstract public function getAll(?int $limit, ?int $offset): array;

    /**
     * @param int $id
     * @return AbstractDTO|null
     */
    abstract public function getOneById(int $id);

    /**
     * @param AbstractFormModel $data
     * @return AbstractDTO
     */
    abstract public function post(AbstractFormModel $data);

    /**
     * @param int $id
     * @param AbstractFormModel $data
     * @return AbstractDTO
     */
    abstract public function put(int $id, AbstractFormModel $data);

    /**
     * @param int $id
     * @return mixed
     */
    abstract public function delete(int $id);

    /**
     * @return EntityManagerInterface
     */
    protected function getManager(): EntityManagerInterface
    {
        return $this->manager;
    }

    /**
     * @return ServiceEntityRepositoryInterface
     */
    protected function getRepository(): ServiceEntityRepositoryInterface
    {
        return $this->repository;
    }

    /**
     * @param $id
     * @return mixed
     */
    protected function getFromRepository($id) {
        return $this->getRepository()->findOneBy(["id" => $id, "isActive" => true]);
    }
}

<?php

namespace App\Service;

use App\Entity\Group;
use App\Entity\Role;
use App\Entity\UserRole;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\RoleFormModel;
use App\Repository\RoleRepository;
use App\Repository\GroupRepository;
use App\Repository\UserRoleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class RoleService
 * @package App\Service
 */
class RoleService extends AbstractService
{
    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /**
     * @var UserRoleRepository
     */
    private $userRoleRepository;

    /**
     * RoleService constructor.
     * @param EntityManagerInterface $manager
     * @param RoleRepository $repository
     * @param GroupRepository $groupRepository
     * @param UserRoleRepository $userRoleRepository
     */
    public function __construct(
        EntityManagerInterface $manager,
        RoleRepository $repository,
        GroupRepository $groupRepository,
        UserRoleRepository $userRoleRepository)
    {
        parent::__construct($manager, $repository);
        $this->groupRepository = $groupRepository;
        $this->userRoleRepository = $userRoleRepository;
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Role[] r.length > 0 | one or more roles in array else r.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Role|null r !== null | role exists with id else r = null
     */
    public function getOneById(int $id): ?Role
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Group[] g.length > 0 | one or more groups in array else g.length = 0
     */
    public function getGroups(int $id): array
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return array_values(
            array_filter(
                $this->groupRepository->findByRoleId($id), function($group) {
                /** @var Group $group */
                return $group->getIsActive();
            })
        );
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return UserRole[] ur.length > 0 | one or more userRoles in array else ur.length = 0
     */
    public function getUserRoles(int $id): array
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return array_values(
            array_filter($this->userRoleRepository->findBy(["role" => $id]), function($userRole) {
                /** @var UserRole $userRole */
                return $userRole->getUser()->getIsActive() && $userRole->getRole()->getIsActive();
            })
        );
    }

    /**
     * @param AbstractFormModel $data instanceof RoleFormModel
     * @throws InvalidArgumentException !($data instanceof RoleFormModel)
     * @throws Exception role with label already exists
     * @return Role
     */
    public function post(AbstractFormModel $data): Role
    {
        if(!($data instanceof RoleFormModel)) throw new InvalidArgumentException('$data must an instance of '.RoleFormModel::class);

        if($this->getRepository()->findOneByLabel($data->getLabel())) throw new Exception("label exists");

        ($role = new Role())
            ->setLabel($data->getLabel());

        $this->getManager()->persist($role);
        $this->getManager()->flush();

        return $role;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof RoleFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException !($data instanceof RoleFormModel)
     * @throws Exception role not found by $id
     * @throws Exception role with label already exists
     * @return Role
     */
    public function put(int $id, AbstractFormModel $data): Role
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof RoleFormModel)) throw new InvalidArgumentException('$data must an instance of '.RoleFormModel::class);

        /** @var Role $role */
        $role = $this->getFromRepository($id);
        if(!$role) throw new Exception("No Role (id#$id) found");

        if($role->getLabel() !== $data->getLabel())
            if($this->getRepository()->findOneByLabel($data->getLabel())) throw new Exception("label exists");

        $role
            ->setLabel($data->getLabel());

        $this->getManager()->flush();
        return $role;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception role not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Role $role */
        $role = $this->getFromRepository($id);
        if(!$role) throw new Exception("No Role (id#$id) found");
        $role->setIsActive(false);
        $role->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}
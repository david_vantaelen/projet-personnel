<?php


namespace App\Service;


use App\Entity\Format;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\FormatFormModel;
use App\Repository\FormatRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class FormatService
 * @package App\Service
 */
class FormatService extends AbstractService
{
    /**
     * FormatService constructor.
     * @param EntityManagerInterface $manager
     * @param FormatRepository $repository
     */
    public function __construct(
        EntityManagerInterface $manager,
        FormatRepository $repository
    )
    {
        parent::__construct($manager, $repository);
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Format[] f.length > 0 | one or more formats in array else f.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return  $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Format|null f !== null | format exists with id else f = null
     */
    public function getOneById(int $id): ?Format
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param string $value
     * @return Format|null f !== null | format exists with id else f = null
     */
    public function getOneByFormat(string $value): ?Format
    {
        return $this->getRepository()->findOneBy(["isActive" => true, "format" => $value]);
    }

    /**
     * @param string $value
     * @return bool
     */
    public function hasFormat(string $value): bool
    {
        return $this->getRepository()->findOneByFormat($value) ? true : false;
    }

    /**
     * @param AbstractFormModel $data !== null && $data instanceof FormatFormModel
     * @throws InvalidArgumentException !($data instanceof FormatFormModel)
     * @throws Exception format already exists
     * @return Format
     */
    public function post(AbstractFormModel $data): Format
    {
        if(!($data instanceof FormatFormModel)) throw new InvalidArgumentException('$data must an instance of '.FormatFormModel::class);
        if($this->getRepository()->findOneByFormat($data->getFormat())) throw new Exception("Format exists");

        ($format = new Format())->setFormat($data->getFormat());

        $this->getManager()->persist($format);
        $this->getManager()->flush();

        return $format;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data instanceof FormatFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException !($data instanceof FormatFormModel)
     * @throws Exception format not found by $id
     * @throws Exception format already exists
     * @return Format
     */
    public function put(int $id, AbstractFormModel $data): Format
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof FormatFormModel)) throw new InvalidArgumentException('$data must an instance of '.FormatFormModel::class);

        /** @var Format $format */
        $format = $this->getFromRepository($id);
        if(!$format) throw new Exception("No Format (id#$id) found");

        if($format->getFormat() !== $data->getFormat())
            if($this->getRepository()->findOneByFormat($data->getFormat())) throw new Exception("Format exists");

        $format->setFormat($data->getFormat());

        $this->getManager()->flush();
        return $format;
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception format not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Format $format */
        $format = $this->getFromRepository($id);
        if(!$format) throw new Exception("No Format (id#$id) found");
        $format->setIsActive(false);
        $format->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}
<?php

namespace App\Service;

use App\Entity\Command;
use App\Entity\CommandEdition;
use App\Form\Model\AbstractFormModel;
use App\Form\Model\CommandEditionFormModel;
use App\Form\Model\CommandFormModel;
use App\Repository\CommandEditionRepository;
use App\Repository\CommandRepository;
use App\Repository\CourierRepository;
use App\Repository\EditionRepository;
use App\Repository\FinancialRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use InvalidArgumentException;

/**
 * Class CommandService
 * @package App\Service
 */
class CommandService extends AbstractService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var CourierRepository
     */
    private $courierRepository;

    /**
     * @var FinancialRepository
     */
    private $financialRepository;

    /**
     * @var EditionRepository
     */
    private $editionRepository;

    /**
     * @var CommandEditionRepository
     */
    private $commandEditionRepository;

    /**
     * CommandService constructor.
     * @param EntityManagerInterface $manager
     * @param CommandRepository $repository
     * @param UserRepository $userRepository
     * @param CourierRepository $courierRepository
     * @param FinancialRepository $financialRepository
     * @param EditionRepository $editionRepository
     * @param CommandEditionRepository $commandEditionRepository
     */
    public function __construct(
        EntityManagerInterface $manager,
        CommandRepository $repository,
        UserRepository $userRepository,
        CourierRepository $courierRepository,
        FinancialRepository $financialRepository,
        EditionRepository $editionRepository,
        CommandEditionRepository $commandEditionRepository
    )
    {
        parent::__construct($manager, $repository);
        $this->userRepository = $userRepository;
        $this->courierRepository = $courierRepository;
        $this->financialRepository = $financialRepository;
        $this->editionRepository = $editionRepository;
        $this->commandEditionRepository = $commandEditionRepository;
    }

    /**
     * @param int|null $limit > -1
     * @param int|null $offset > -1
     * @throws InvalidArgumentException | $limit < 0
     * @throws InvalidArgumentException | $offset < 0
     * @return Command[] c.length > 0 | one or more commands in array else c.length = 0
     */
    public function getAll(?int $limit, ?int $offset): array
    {
        if(!is_null($limit) && $limit < 0) throw new InvalidArgumentException("limit must be positive");
        if(!is_null($offset) && $offset < 0) throw new InvalidArgumentException("offset must be positive");

        return $this->getRepository()->findBy(["isActive" => true], null, $limit, $offset);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return Command|null c !== null | command exists with id else c = null
     */
    public function getOneById(int $id): ?Command
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return $this->getFromRepository($id);
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @return CommandEdition[] ce.length > 0 | one or more commandEdition in array else ce.length = 0
     */
    public function getCommandEditions(int $id): array
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        return array_values(
            array_filter(
                $this->commandEditionRepository->findBy(["command" => $id]), function($commandEdition){
                /** @var CommandEdition $commandEdition */
                return $commandEdition->getEdition()->getIsActive() && $commandEdition->getCommand()->getIsActive();
            })
        );
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $edition_id != null && $id > 0
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $edition_id < 1
     * @return CommandEdition|null ce !== null | commandEdition exists with ids else ce = null
     */
    public function getOneCommandEdition(int $id, int $edition_id): ?CommandEdition
    {
        if($id < 1 || $edition_id < 1) throw new InvalidArgumentException("id must be greater than 0");
        $commandEdition = $this->commandEditionRepository->findOneBy(["command" => $id, "edition" => $edition_id]);
        return $commandEdition && $commandEdition->getEdition()->getIsActive()
            && $commandEdition->getCommand()->getIsActive() ? $commandEdition : null;
    }

    /**
     * @param AbstractFormModel $data !== null && $data instanceof CommandFormModel
     * @throws InvalidArgumentException !($data instanceof CommandFormModel)
     * @throws Exception user not found by userId in $data
     * @throws Exception financial not found by financialId in $data
     * @throws Exception courier not found by courierId in $data
     * @return Command
     */
    public function post(AbstractFormModel $data): Command
    {
        if(!($data instanceof CommandFormModel)) throw new InvalidArgumentException('$data must be an instance of '.CommandFormModel::class);

        $user = $this->userRepository->findOneBy(["id" => $data->getUserId(), "isActive" => true]);
        if(!$user) throw new Exception("No User (id#".$data->getUserId().") found");

        $financial = $this->financialRepository->findOneBy(["id" => $data->getFinancialId()]);
        if(!$financial) throw new Exception("No Financial (id#".$data->getFinancialId().") found");

        if(!is_null($data->getCourierId())){
            $courier = $this->courierRepository->findOneBy(["id" => $data->getCourierId()]);
            if(!$courier) throw new Exception("No Courier (id#".$data->getCourierId().") found");
        }

        ($command = new Command())
            ->setStatus($data->getStatus())
            ->setUser($user)
            ->setFinancial($financial);

        if(isset($courier)) $command->setCourier($courier);

        $this->getManager()->persist($command);
        $this->getManager()->flush();

        return $command;
    }

    /**
     * @param int $id != null && $id > 0
     * @param AbstractFormModel $data !== null && $data instanceof CommandFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException !($data instanceof CommandFormModel)
     * @throws Exception command not found by $id
     * @throws Exception user not found by userId in $data
     * @throws Exception financial not found by financialId in $data
     * @throws Exception courier not found by courierId in $data
     * @return Command
     */
    public function put(int $id, AbstractFormModel $data): Command
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof CommandFormModel)) throw new InvalidArgumentException('$data must be an instance of '.CommandFormModel::class);

        /** @var Command $command */
        $command = $this->getFromRepository($id);
        if(!$command) throw new Exception("No Command (id#$id) found");

        $user = $this->userRepository->findOneBy(["id" => $data->getUserId(), "isActive" => true]);
        if(!$user) throw new Exception("No User (id#".$data->getUserId().") found");

        $financial = $this->financialRepository->findOneBy(["id" => $data->getFinancialId(), "isActive" => true]);
        if(!$financial) throw new Exception("No Financial (id#".$data->getFinancialId().") found");

        if(!is_null($data->getCourierId())){
            $courier = $this->courierRepository->findOneBy(["id" => $data->getCourierId()]);
            if(!$courier) throw new Exception("No Courier (id#".$data->getCourierId().") found");
        }

        $command
            ->setStatus($data->getStatus())
            ->setUser($user)
            ->setFinancial($financial);

        if(isset($courier)) $command->setCourier($courier);
        else if(is_null($data->getCourierId()))  $command->setCourier(null);

        $this->getManager()->flush();
        return $command;
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $edition_id != null && $edition_id > 0
     * @param AbstractFormModel $data instanceof CommandEditionFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $edition_id < 1
     * @throws InvalidArgumentException !($data instanceof CommandEditionFormModel)
     * @throws Exception command not found by $id
     * @throws Exception edition not found by $edition_id
     * @throws Exception command has already edition
     * @return CommandEdition
     */
    public function addEdition(int $id, int $edition_id, AbstractFormModel $data): CommandEdition
    {
        if($id < 1 || $edition_id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof CommandEditionFormModel)) throw new InvalidArgumentException('$data must be an instance of '.CommandEditionFormModel::class);

        /** @var Command $command */
        $command = $this->getFromRepository($id);
        if(!$command) throw new Exception("No Command (id#$id) found");

        $edition = $this->editionRepository->findOneBy(["id" => $edition_id, "isActive" => true]);
        if(!$edition) throw new Exception("No Edition (id#$edition_id) found");

        $commandEdition = $this->commandEditionRepository->findOneBy(["command" => $id, "edition" => $edition_id]);
        if($commandEdition) throw new Exception("Command (id#$id) has already Edition (id#$edition_id)");

        $commandEdition = (new CommandEdition())
            ->setCommand($command)
            ->setEdition($edition)
            ->setQuantity($data->getQuantity())
            ->setPrice($data->getPrice());

        $this->getManager()->persist($commandEdition);
        $this->getManager()->flush();
        return $commandEdition;
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $edition_id != null && $edition_id > 0
     * @param AbstractFormModel $data instanceof CommandEditionFormModel
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $edition_id < 1
     * @throws InvalidArgumentException !($data instanceof CommandEditionFormModel)
     * @throws Exception command not found by $id
     * @throws Exception edition not found by $edition_id
     * @throws Exception command hasn't the edition
     * @return CommandEdition
     */
    public function editEdition(int $id, int $edition_id, AbstractFormModel $data): CommandEdition
    {
        if($id < 1 || $edition_id < 1) throw new InvalidArgumentException("id must be greater than 0");
        if(!($data instanceof CommandEditionFormModel)) throw new InvalidArgumentException('$data must be an instance of '.CommandEditionFormModel::class);

        /** @var Command $command */
        $command = $this->getFromRepository($id);
        if(!$command) throw new Exception("No Command (id#$id) found");

        $edition = $this->editionRepository->findOneBy(["id" => $edition_id, "isActive" => true]);
        if(!$edition) throw new Exception("No Edition (id#$edition_id) found");

        $commandEdition = $this->commandEditionRepository->findOneBy(["command" => $id, "edition" => $edition_id]);
        if(!$commandEdition) throw new Exception("No Edition (id#$edition_id) in Command (id#$id) found");

        $commandEdition
            ->setQuantity($data->getQuantity())
            ->setPrice($data->getPrice());

        $this->getManager()->flush();
        return $commandEdition;
    }

    /**
     * @param int $id != null && $id > 0
     * @param int $edition_id != null && $edition_id > 0
     * @throws InvalidArgumentException | $id < 1
     * @throws InvalidArgumentException | $edition_id < 1
     * @throws Exception command not found by $id
     * @throws Exception edition not found by $edition_id
     * @throws Exception command hasn't the edition
     */
    public function removeEdition(int $id, int $edition_id)
    {
        if($id < 1 || $edition_id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Command $command */
        $command = $this->getFromRepository($id);
        if(!$command) throw new Exception("No Command (id#$id) found");

        $edition = $this->editionRepository->findOneBy(["id" => $edition_id, "isActive" => true]);
        if(!$edition) throw new Exception("No Edition (id#$edition_id) found");

        $commandEdition = $this->commandEditionRepository->findOneBy(["command" => $id, "edition" => $edition_id]);
        if(!$commandEdition) throw new Exception("No Edition (id#".$edition_id.") in Command (id#".$id.")");

        $this->getManager()->remove($commandEdition);
        $this->getManager()->flush();
    }

    /**
     * @param int $id != null && $id > 0
     * @throws InvalidArgumentException $id < 1
     * @throws Exception command not found by $id
     */
    public function delete(int $id)
    {
        if($id < 1) throw new InvalidArgumentException("id must be greater than 0");

        /** @var Command $command */
        $command = $this->getFromRepository($id);
        if(!$command) throw new Exception("No Command (id#$id) found");
        $command->setIsActive(false);
        $command->setDeletedAt(new \DateTime());
        $this->getManager()->flush();
    }
}

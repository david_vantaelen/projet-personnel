<?php

    use OpenApi\Annotations as OA;

    /**
     * @OA\Info(title="BookTik", description="A library", version="1.00")
     *
     * @OA\Tag(
     *     name="Author",
     *     description="Author Operations",
     * )
     *
     * @OA\Tag(
     *     name="Book",
     *     description="Book Operations",
     * )
     *
     * @OA\Tag(
     *     name="Editor",
     *     description="Editor Operations",
     * )
     *
     * @OA\Tag(
     *     name="Collection",
     *     description="Collection Operations",
     * )
     *
     * @OA\Tag(
     *     name="Format",
     *     description="Format Operations",
     * )
     *
     * @OA\Tag(
     *     name="Edition",
     *     description="Edition Operations",
     * )
     *
     * @OA\Tag(
     *     name="Courier",
     *     description="Courier Operations",
     * )
     *
     * @OA\Tag(
     *     name="Financial",
     *     description="Fiancial Operations",
     * )
     *
     * @OA\Tag(
     *     name="Command",
     *     description="Command Operations",
     * )
     *
     * @OA\Tag(
     *     name="Group",
     *     description="Group Operations",
     * )
     *
     * @OA\Tag(
     *     name="Role",
     *     description="Role Operations",
     * )
     *
     * @OA\Tag(
     *     name="User",
     *     description="User Operations",
     * )
     *
     */


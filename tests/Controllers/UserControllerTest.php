<?php

namespace App\Tests\Controller;


use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserController extends WebTestCase
{
    private static $client;
    private static $logger;
    private static $encoder;
    private static $jwtManager;
    private static $application;

    public static function setUpBeforeClass() {
        self::$client = static::createClient();
        self::$kernel = static::createKernel();
        self::$logger = self::$container->get(LoggerInterface::class);
        self::$encoder = self::$container->get('security.user_password_encoder.generic');
        self::$jwtManager = self::$container->get('lexik_jwt_authentication.jwt_manager');
        self::$application = new Application(self::$kernel);
    }

    public function createAuthClient() {
        $client = clone self::$client;
        $token = self::$jwtManager->create(($user = new User())->setUsername("user")->setPassword(self::$encoder->encodePassword($user, "test")));
        $client->setServerParameters(["HTTP_Authorization" => "Bearer ".$token]);
        return $client;
    }

    public function testGetAllActionWithAuth() {
        $client = $this->createAuthClient();
        $client->request("GET", "/api/user");
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testGetAllActionContentType() {
        $client = $this->createAuthClient();
        $client->request("GET", "/api/book");
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                "Content-Type",
                "application/json"
            )
        );
    }

    public function testGetAllActionWithoutAuth() {
        $client = clone self::$client;
        $client->request("GET", "/api/user");
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    public static function tearDownAfterClass(): void {
        self::$client = null;
        self::$logger = null;
        self::$encoder = null;
        self::$encoder = null;
    }
}
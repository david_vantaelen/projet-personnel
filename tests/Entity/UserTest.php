<?php

namespace App\Tests\Entity;
use App\Entity\Group;
use App\Entity\Role;
use App\Entity\User;
use App\Entity\UserRole;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    public function testGetRolesFromGroupWithEmptyValues()
    {
        $user = (new User());
        $this->assertCount(0, $user->getRolesFromGroup());
    }

    public function testGetRolesFromGroup()
    {
        $group = (new Group())->setLabel("GRP_ADMIN")
            ->addRole((new Role)->setLabel("ROLE_ADMIN"));
        $user = (new User())->setGroup($group);
        $this->assertCount(1, $user->getRolesFromGroup());
    }

    public function testGetRolesFromUserRolesWithEmptyValues()
    {
        $user = (new User());
        $this->assertCount(0, $user->getRolesFromUserRoles());
    }

    public function testGetRolesFromUserRoles()
    {
        $role = (new Role)->setLabel("ROLE_ADMIN");
        $user = (new User());

        $userRole = (new UserRole())
            ->setRole($role);

        $user->addUserRole($userRole);
        $this->assertCount(1, $user->getRolesFromUserRoles());
    }

    public function testGetRolesFromUserRolesWithExpiredEndAt()
    {
        $role = (new Role)->setLabel("ROLE_ADMIN");
        $user = (new User());

        $userRole = (new UserRole())
            ->setRole($role)
            ->setEndAt((new \DateTime())->sub(new \DateInterval("P1DT5H")));

        $user->addUserRole($userRole);
        $this->assertCount(0, $user->getRolesFromUserRoles());
    }

    public function testGetRolesWithEmptyValues()
    {
        $user = (new User());
        $this->assertContains('ROLE_USER', $user->getRoles());
    }

    public function testGetRoles()
    {
        $role = (new Role)->setLabel("ROLE_ADMIN");
        $role2 = (new Role)->setLabel("ROLE_USER");
        $user = (new User());
        $group = (new Group())->setLabel("GRP_ADMIN")
            ->addRole($role);
        $userRole = (new UserRole())->setRole($role2);
        $userRole2 = (new UserRole())->setRole((new Role())->setLabel("ROLE_DESIGNER"));

        $user->setGroup($group);
        $user->addUserRole($userRole);
        $user->addUserRole($userRole2);

        $this->assertCount(3, $user->getRoles());
        $this->assertContains('ROLE_USER', $user->getRoles());
    }
}
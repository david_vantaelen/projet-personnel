<?php

namespace App\Tests\Entity;
use App\Entity\Edition;
use PHPUnit\Framework\TestCase;

class EditionTest extends TestCase
{
    public function testSetDescriptionWithMultilines()
    {
        $edition = (new Edition())
            ->setDescription("
                [[l]]
                    
                        [[li]]Test[[/li]]
                        
             [[/l]]
            ");
        $this->assertEquals("[[l]][[li]]Test[[/li]][[/l]]", $edition->getDescription());
    }

}
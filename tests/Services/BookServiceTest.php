<?php

namespace App\Tests\Services;


use App\Entity\Book;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use App\Services\BookService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookServiceTest extends KernelTestCase
{
    private $manager;
    private $repository;
    private $authorRepository;
    private $service;

    public function setUp(){
        self::bootKernel();

        $this->manager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->setMethods(["persist", "flush"])
            ->getMock();

        $this->repository = $this->getMockBuilder(BookRepository::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->setMethods(["findBy", "findOneBy"])
            ->getMock();

        $this->authorRepository = $this->getMockBuilder(AuthorRepository::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->setMethods(["findBy", "findOneBy"])
            ->getMock();

        $this->service = new BookService(
            $this->manager,
            $this->repository,
            $this->authorRepository
        );
    }

    public function testGetAll() {
        $this->repository->expects($this->once())
            ->method("findBy")
            ->willReturn([new Book()]);

        $this->assertCount(1,  $this->service->getAll());
    }

    public function tearDown(): void {
        $this->manager = null;
        $this->repository = null;
        $this->authorRepository = null;
        $this->service = null;
    }
}

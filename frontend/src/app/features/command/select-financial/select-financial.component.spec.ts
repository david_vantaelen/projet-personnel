import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectFinancialComponent } from './select-financial.component';

describe('SelectFinancialComponent', () => {
  let component: SelectFinancialComponent;
  let fixture: ComponentFixture<SelectFinancialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectFinancialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectFinancialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

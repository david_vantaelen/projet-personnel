import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Financial } from 'src/app/core/models/financial';
import { environment } from 'src/environments/environment';
import { HtmlFormatService } from 'src/app/core/services/html-format.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommandService } from 'src/app/core/services/command.service';
import { FinancialService } from 'src/app/core/services/financial.service';

@Component({
  selector: 'app-select-financial',
  templateUrl: './select-financial.component.html',
  styleUrls: ['./select-financial.component.scss']
})
export class SelectFinancialComponent implements OnInit {
  
  @Input() financials: Financial[];
  @Output() onSubmit: EventEmitter<Financial>;

  form: FormGroup;

  readonly url: string = environment.domain;

  constructor(
    private htmlFormatService: HtmlFormatService,
    private financialService: FinancialService,
    private commandService: CommandService,
    private formBuilder: FormBuilder
  ) {
    this.onSubmit = new EventEmitter<Financial>();
   }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      financial: new FormControl(null, Validators.compose([Validators.required]))
    });
  }
  
  format(text: string) {
    return text && this.htmlFormatService.format(text);
  }
}

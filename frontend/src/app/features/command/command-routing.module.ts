import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommandComponent } from './command.component';
import { GetEditionsResolver } from 'src/app/core/resolvers/edition.resolver';
import { CommandListComponent } from './command-list/command-list.component';
import { SelectFinancialComponent } from './select-financial/select-financial.component';
import { GetFinancialsResolver } from 'src/app/core/resolvers/financial.resolver';
import { SelectCourierComponent } from './select-courier/select-courier.component';
import { GetCouriersResolver } from 'src/app/core/resolvers/courier.resolver';
import { CommandStepComponent } from './command-step/command-step.component';
import { IsLoggedGuard } from 'src/app/core/guards/is-logged.guard';
import { HasShopItemsGuard } from 'src/app/core/guards/has-shop-items.guard';

const routes: Routes = [{ path: '', component: CommandComponent, children: [
    {path: '', component: CommandListComponent, resolve: { editions: GetEditionsResolver }},
    {path: 'step', component: CommandStepComponent, canActivate: [IsLoggedGuard, HasShopItemsGuard], resolve: { 
      financials: GetFinancialsResolver,
      couriers: GetCouriersResolver
    }},
    {path: '**', redirectTo: '', pathMatch: 'full'}
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommandRoutingModule { }

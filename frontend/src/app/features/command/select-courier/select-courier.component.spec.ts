import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectCourierComponent } from './select-courier.component';

describe('SelectCourierComponent', () => {
  let component: SelectCourierComponent;
  let fixture: ComponentFixture<SelectCourierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectCourierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectCourierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

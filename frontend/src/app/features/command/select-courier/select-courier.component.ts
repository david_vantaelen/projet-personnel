import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Courier } from 'src/app/core/models/courier';
import { environment } from 'src/environments/environment';
import { HtmlFormatService } from 'src/app/core/services/html-format.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommandService } from 'src/app/core/services/command.service';
import { CourierService } from 'src/app/core/services/courier.service';

@Component({
  selector: 'app-select-courier',
  templateUrl: './select-courier.component.html',
  styleUrls: ['./select-courier.component.scss']
})
export class SelectCourierComponent implements OnInit {

  @Input() couriers: Courier[];
  @Output() onSubmit: EventEmitter<Courier>;

  form: FormGroup;

  readonly url: string = environment.domain;

  constructor(
    private htmlFormatService: HtmlFormatService,
    private courierService: CourierService,
    private commandService: CommandService,
    private formBuilder: FormBuilder
  ) {
    this.onSubmit = new EventEmitter<Courier>();
   }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      courier: new FormControl(null, Validators.compose([Validators.required]))
    });

  }

  format(text: string) {
    return text && this.htmlFormatService.format(text);
  }
}

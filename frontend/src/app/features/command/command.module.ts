import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommandRoutingModule } from './command-routing.module';
import { CommandComponent } from './command.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommandListComponent } from './command-list/command-list.component';
import { SelectFinancialComponent } from './select-financial/select-financial.component';
import { SelectCourierComponent } from './select-courier/select-courier.component';
import { CommandStepComponent } from './command-step/command-step.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommandValidationComponent } from './command-validation/command-validation.component';


@NgModule({
  declarations: [CommandComponent, CommandListComponent, SelectFinancialComponent, SelectCourierComponent, CommandStepComponent, CommandValidationComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    CommandRoutingModule
  ]
})
export class CommandModule { }

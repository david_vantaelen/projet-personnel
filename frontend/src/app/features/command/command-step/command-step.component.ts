import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Address } from 'src/app/core/models/address';
import { Courier } from 'src/app/core/models/courier';
import { ActivatedRoute } from '@angular/router';
import { Financial } from 'src/app/core/models/financial';
import { CommandService } from 'src/app/core/services/command.service';
import { concatMap } from 'rxjs/operators';
import { SelectFinancialComponent } from '../select-financial/select-financial.component';
import { SelectCourierComponent } from '../select-courier/select-courier.component';
import { CourierService } from 'src/app/core/services/courier.service';
import { FinancialService } from 'src/app/core/services/financial.service';
import { forkJoin, of } from 'rxjs';
import { CatalogService } from 'src/app/core/services/catalog.service';
import { ShopItem } from 'src/app/core/models/shop-item';
import { CommandValidationComponent } from '../command-validation/command-validation.component';
import { NbStepperComponent } from '@nebular/theme';

@Component({
  selector: 'app-command-step',
  templateUrl: './command-step.component.html',
  styleUrls: ['./command-step.component.scss']
})
export class CommandStepComponent implements OnInit, AfterViewInit {

  couriers: Courier[];
  financials: Financial[];
  address: Address;
  listItems: ShopItem[];
  total: number;

  @ViewChild(NbStepperComponent) stepper: NbStepperComponent;
  @ViewChild(SelectCourierComponent) courier: SelectCourierComponent;
  @ViewChild(SelectFinancialComponent) financial: SelectFinancialComponent; 
  @ViewChild(CommandValidationComponent) validation: CommandValidationComponent; 

  constructor(
    private cdr: ChangeDetectorRef,
    private authService: AuthService,
    private courierService: CourierService,
    private financialService: FinancialService,
    private catalogService: CatalogService,
    private commandService: CommandService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngAfterViewInit(): void {
    this.stepper.reset();
    this.cdr.detectChanges();
  }

  ngOnInit(): void {
    try{
      this.catalogService.fromStorage();
      this.authService.getAuthenticatedUser().pipe(
        concatMap(user => {
          this.address = user.main_address
          return this.activatedRoute.data
        }),
        concatMap((data:any) => { 
          this.couriers = data.couriers;
          this.financials = data.financials;
          return this.catalogService.shopcart
        })
      ).subscribe(shopCart => {
        this.listItems = shopCart;
        this.total = 0;
    
        for(let item of this.listItems) {
          this.total += item.price * item.quantity;
        }
      });

    } catch(e) { console.log(e); }
      
  }

  prepareData() {
    const courierId = this.courier.form.controls.courier.value;
    const financialId = this.financial.form.controls.financial.value;
    forkJoin([
      courierId ? this.courierService.getCourier(courierId) : of(null),
      financialId ? this.financialService.getFinancial(financialId) : of(null)
    ]).subscribe(data => {
      if(data[0])this.commandService.setCourier(data[0]);
      if(data[1])this.commandService.setFinancial(data[1]);
    }) 
  }  

  createCommand() {
    this.courier.form.reset();
    this.financial.form.reset();
    this.validation.createCommand();
  }
}

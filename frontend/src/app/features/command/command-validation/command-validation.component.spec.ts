import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandValidationComponent } from './command-validation.component';

describe('CommandValidationComponent', () => {
  let component: CommandValidationComponent;
  let fixture: ComponentFixture<CommandValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

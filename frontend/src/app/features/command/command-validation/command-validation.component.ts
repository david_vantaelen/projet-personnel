import { Component, OnInit, Input } from '@angular/core';
import { Financial } from 'src/app/core/models/financial';
import { Courier } from 'src/app/core/models/courier';
import { Address } from 'src/app/core/models/address';
import { CommandService } from 'src/app/core/services/command.service';
import { HtmlFormatService } from 'src/app/core/services/html-format.service';
import { environment } from 'src/environments/environment';
import { ShopItem } from 'src/app/core/models/shop-item';
import { Router } from '@angular/router';
import { CommandForm, STATUS } from 'src/app/core/models/command';
import { AuthService } from 'src/app/core/services/auth.service';
import { concatMap, finalize, map } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { CatalogService } from 'src/app/core/services/catalog.service';
import { NbToastrService } from '@nebular/theme';
import { EditionService } from 'src/app/core/services/edition.service';
import { EditionForm } from 'src/app/core/models/edition';
import { CommandEdition } from 'src/app/core/models/command-edition';

@Component({
  selector: 'app-command-validation',
  templateUrl: './command-validation.component.html',
  styleUrls: ['./command-validation.component.scss']
})
export class CommandValidationComponent implements OnInit {

  @Input() address: Address;

  courier: Courier;
  financial: Financial;
  @Input() listItems: ShopItem[];
  @Input() total: number;

  readonly url: string = environment.domain;

  constructor(
    private router: Router,
    private authService: AuthService,
    private catalogService: CatalogService,
    private editionService: EditionService,
    private htmlFormatService: HtmlFormatService,
    private nbToastrService: NbToastrService,
    private commandService: CommandService
  ) { }

  ngOnInit(): void {
    try {
      this.commandService.financial.subscribe(f =>  this.financial = f)
      this.commandService.courier.subscribe(c => this.courier = c);
      
    } catch(e) {
      console.log(e);
    }
  }

  format(text: string) {
    return text && this.htmlFormatService.format(text);
  }

  createCommand() {
    try {
      this.authService.getAuthenticatedUser().toPromise()
      .then(user => {
        const command: CommandForm = {
          userId: user.id,
          financialId: this.financial.id,
          courierId: this.courier.id,
          status: STATUS.PENDING
        } 
        this.commandService.createCommand(command)
        .pipe(
          concatMap(command => { 
            const items = this.listItems.map(item => this.commandService.addCommandEdition(command.id, item.id, {price: item.price * item.quantity, quantity: item.quantity}));
            return forkJoin(items)
          }),
          concatMap((ces: CommandEdition[]) => 
            forkJoin(ces.map(ce => this.editionService.getEdition(ce.edition.id)
            .pipe(map(e => ({...e, stockToRemove: ce.quantity}))))
          )),
          concatMap((editions: any[]) => 
            forkJoin(editions.map(edition => this.editionService.editEdition(edition.id, {
              stock: edition.stock - edition.stockToRemove < 0 ? 0 : edition.stock - edition.stockToRemove
            } as EditionForm))
          )),
          finalize(() => { 
            this.nbToastrService.success('Your command has been send', 'Thank You')
            this.router.navigate(['/', 'catalog']);
            this.catalogService.setShopCart([]);
            this.commandService.setFinancial(null);
            this.commandService.setCourier(null);
          })).subscribe()
      })
    } catch(e) {
      console.log(e);
    }
  }

}

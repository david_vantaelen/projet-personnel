import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Edition } from 'src/app/core/models/edition';
import { ShopItem } from 'src/app/core/models/shop-item';
import { ActivatedRoute, Router } from '@angular/router';
import { CatalogService } from 'src/app/core/services/catalog.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { LoginComponent } from '../../auth/login/login.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-command-list',
  templateUrl: './command-list.component.html',
  styleUrls: ['./command-list.component.scss']
})
export class CommandListComponent implements OnInit {

  readonly url: string = environment.domain + "/uploads/editions/";

  editions: Array<Edition>;
  listItems: Array<ShopItem>;
  total: number;
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private catalogService: CatalogService,
    private NbDialogService: NbDialogService,
    private nbToastrService: NbToastrService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    try {
      this.catalogService.fromStorage();

      this.activatedRoute.data.subscribe((data: {editions: Array<Edition>}) => { 
        if(!data.editions) return;
        this.editions = data.editions;
        this.catalogService.shopcart.subscribe(shopCart => {
          this.listItems = shopCart.map(item => {
            const edition = this.editions.find(e => e.id === item.id);
            item.stock = edition ? edition.stock : item.stock;
            item.quantity = item.quantity > item.stock ? item.stock : item.quantity;
            return item;
          });

          this.total = 0;
      
          for(let item of this.listItems) {
            this.total += item.price * item.quantity;
          }
        });
      });

    } catch(e) {
      console.log(e);
    }
  }

  removeFromShopCart(item: ShopItem) {
    try {
      this.catalogService.removeAllFromShopCart(item);
    } catch(e) {
      console.log(e);
    }
  }

  addItem(item: ShopItem) {
    if(item.quantity + 1 <= item.stock) {
      item.quantity++;
      this.total += item.price;
    }
  }

  removeItem(item: ShopItem) {
    if(item.quantity - 1 >= 0) {
      item.quantity--
      this.total -= item.price;
    };
  }

  nextStep() {
    try {
      if(!this.authService.isLogged()) {
        const ref = this.NbDialogService.open(LoginComponent, { closeOnBackdropClick: true, context: {action: this.location.path()}});
      } else {
        const items: ShopItem[] = this.listItems.filter(item => item.quantity > 0)
        this.catalogService.setShopCart(items);
        if(items.length) this.router.navigate(['/', 'command', 'step']);
        else {
          this.nbToastrService.danger('No products to command', 'error');
          this.router.navigate(['/', 'catalog']);
        }
       }
    } catch(e) {
      console.log(e);
    }
  }
}
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { registerUser, UserForm } from 'src/app/core/models/user';
import { GroupService } from 'src/app/core/services/group.service';
import { Auth } from 'src/app/core/models/auth';
import { AuthService } from 'src/app/core/services/auth.service';
import { TokenService } from 'src/app/core/services/token.service';
import { forkJoin } from 'rxjs';
import { ERROR_MESSAGES } from 'src/app/core/models/error-message';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {user: false};
  success: {[key: string]: boolean} = {user: false};

  constructor(
    private userService: UserService,
    private groupService: GroupService,
    private authService: AuthService,
    private tokenService: TokenService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { super(); }

  ngOnInit(): void {
    this.forms.user = this.formBuilder.group(registerUser, {validator: this.matchPassword});
    this.forms.user.reset();
  }

  private matchPassword(group: FormGroup) {
    const valid =  group.get('password').value === group.get('confirmPassword').value;
    if(valid) return null;
    group.get('confirmPassword').setErrors({matchPassword: true})
  }

  send() {
    try {
      this.error.user = false;
      this.success.user = false;
      this.submitted = true;
      
      if(this.forms.user.valid) {
        let values = this.forms.user.value;
        delete values.confirmPassword;
        let user = <UserForm>values;
        user.address.isMain = true;

        forkJoin([
          this.userService.hasEmail(user.email),
          this.userService.hasUsername(user.username),
          this.groupService.getDefaultGroup()

        ]).subscribe(r => {
          if(r[0]) this.forms.user.get('email').setErrors({emailExists: true})
          if(r[1]) this.forms.user.get('username').setErrors({usernameExists: true})
          if(r[2]) user.groupId = r[2].id;

          if(!r[0] && !r[1] && r[2]) {
            this.userService.createUser(user).subscribe(data => {
              let auth: Auth = {username: user.username, password: user.password}; 
              this.authService.login(auth).subscribe(data => {
                if(data.token) { 
                  this.tokenService.token = data.token;
                }
              });
              this.router.navigateByUrl('/catalog');
    
            }, error => { this.error.user = true; console.log(error); });
          } else this.error.user = true;
        });
      }
    } catch(e) {
      console.log(e);
    }
  }
}

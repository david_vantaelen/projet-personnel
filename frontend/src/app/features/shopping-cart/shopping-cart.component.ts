import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { ShopItem } from 'src/app/core/models/shop-item';
import { CatalogService } from 'src/app/core/services/catalog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit {
  
  listItems: Array<ShopItem>;
  total: number;

  constructor(
    private ref: NbDialogRef<ShoppingCartComponent>, 
    private catalogService: CatalogService,
    private router: Router) { }

  ngOnInit(): void {
    try {
      this.catalogService.shopcart.subscribe(shopCart => {
        this.listItems = shopCart.reduce((acc, curr) => {
          if(curr.quantity > 1) {
            for(let i = 0; i < curr.quantity; i++){
              acc = [...acc, {...curr, quantity: 1}]
            }
          } else {
            acc = [...acc, curr]
          }
          return acc;
        }, []);
    
        this.total = 0;
    
        for(let item of this.listItems) {
          this.total += item.price;
        }

        if(this.listItems.length < 1) this.ref.close();
      });
    } catch(e) {
      console.log(e);
    }
  }

  removeFromShopCart(item: ShopItem) {
    try {
      this.catalogService.removeFromShopCart(item);
    } catch(e) {
      console.log(e);
    }
  }

  gotoCommandPage() {
    this.router.navigate(['/', 'command']);
    this.close();
  }

  close() {
    this.ref.close();
  }
}

import { Component, OnInit } from '@angular/core';
import { Edition } from 'src/app/core/models/edition';
import { CatalogService } from 'src/app/core/services/catalog.service';
import { ShopItem } from 'src/app/core/models/shop-item';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Page } from 'src/app/core/models/page';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {

  search: boolean;
  searchValue: string;
  page: Page<Edition>;
  errorLoading: boolean = false;

  readonly url: string = environment.domain + "/uploads/editions/";

  constructor(
    private catalogService: CatalogService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.search = false;
    this.searchValue = null;

    this.activatedRoute.data.subscribe((data: { page: Page<Edition> }) => {
      if(!data.page) {
         this.errorLoading = true;
         this.page = {page: 1, data: [], per_page: 0, nb_pages: 1, count: 0, total: 0}
      }
      else {
        this.page = data.page;
        this.errorLoading = false;
      }
    });

    this.activatedRoute.params.subscribe(params => {
      if(params && params.search) {
          this.search = true;
          this.searchValue = params.search;
      }
    });
  }

  addToShopCart(book: Edition) {
    try {
      this.catalogService.addToShopCart(<ShopItem>{...book, quantity: 1});
    } catch(e) {
      console.log(e);
    }
  }

  changePage(page: number){
    if(this.search) this.router.navigate(["/", "catalog", "search", this.searchValue, "page", page]);
    else this.router.navigate(["/", "catalog", "page", page]);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BookComponent } from './book/book.component';
import { BookListComponent } from './book-list/book-list.component';


@NgModule({
  declarations: [
    CatalogComponent,
    BookComponent,
    BookListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CatalogRoutingModule
  ]
})
export class CatalogModule { }

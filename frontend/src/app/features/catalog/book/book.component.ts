import { Component, OnInit } from '@angular/core';
import { Edition } from 'src/app/core/models/edition';
import { ShopItem } from 'src/app/core/models/shop-item';
import { CatalogService } from 'src/app/core/services/catalog.service';
import { HtmlFormatService } from 'src/app/core/services/html-format.service';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {

  book: Edition;
  
  readonly url: string = environment.domain + "/uploads/editions/";

  constructor(
    private htmlFormatService: HtmlFormatService,
    private catalogService: CatalogService,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: { edition: Edition }) => this.book = data.edition);
  }

  format(text: string) {
      return text && this.htmlFormatService.format(text);
  }

  addToShopCart(book: Edition) {
    try {
      this.catalogService.addToShopCart(<ShopItem>{...book, quantity: 1});
    } catch(e) {
      console.log(e);
    }
  }

}

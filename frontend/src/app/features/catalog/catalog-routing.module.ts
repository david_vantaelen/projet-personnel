import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogComponent } from './catalog.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookComponent } from './book/book.component';
import { GetOneEditionResolver, GetPageOfEditionsBySearchResolver, GetPageOfEditionsResolver } from 'src/app/core/resolvers/edition.resolver';
import { HasEditionGuard } from 'src/app/core/guards/has-edition.guard';

const routes: Routes = [{ path: '', component: CatalogComponent, children: [
  {path: '', redirectTo: 'page/1', pathMatch: 'full'  },
  {path: 'page/:page', component: BookListComponent, resolve: { page: GetPageOfEditionsResolver } },
  {path: 'search/:search', component: BookListComponent, resolve: { page: GetPageOfEditionsBySearchResolver } },
  {path: 'search/:search/page/:page', component: BookListComponent, resolve: { page: GetPageOfEditionsBySearchResolver } },
  {path: ':id', component: BookComponent, canActivate: [HasEditionGuard], resolve: { edition: GetOneEditionResolver } },
  {path: '**', redirectTo: 'page/1', pathMatch: 'full' }
]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }

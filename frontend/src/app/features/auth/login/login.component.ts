import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { TokenService } from 'src/app/core/services/token.service';
import { Router } from '@angular/router';
import { NbToastrService, NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  authForm: FormGroup;
  @Input() action: string;

  constructor(
    private ref: NbDialogRef<LoginComponent>, 
    private authService: AuthService,
    private tokenService: TokenService,
    private router: Router,
    private nbToastrService: NbToastrService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    const form: {[key: string]: AbstractControl} = {
      username: new FormControl(null, Validators.compose([Validators.required])),
      password: new FormControl(null, Validators.compose([Validators.required]))
    };

    this.authForm = this.formBuilder.group(form);
  }

  login() {
    try {
      const position: any = 'bottom-right';
      this.authService.login(this.authForm.value).subscribe(data => {
        if(data.token) { 
          this.tokenService.token = data.token;
          if(this.action) { 
            this.router.navigateByUrl(this.action);
            this.close();
          }
          else this.router.navigate(['/', 'catalog']);
          this.nbToastrService.success(`You are now logged`, `Login`, {position});
        }
      }, error =>  this.nbToastrService.danger(`Invalid Credentials`, `Login`, {position}))
    } catch(e) {
      console.log(e);
    }
  }

  close() {
    this.ref.close();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NbDialogModule, NbDialogService, NbToastrModule, NbToastrService } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  const spyNbDialogService = jasmine.createSpyObj('spyNbDialogService', ['open', 'close']);
  spyNbDialogService.open.and.returnValue(null);
  spyNbDialogService.close.and.returnValue(null);
  const spyNbToastrModule = jasmine.createSpyObj('spyNbToastrModule', ['danger', 'success']);
  spyNbToastrModule.danger.and.returnValue(null);
  spyNbToastrModule.success.and.returnValue(null);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NbDialogModule,
        NbToastrModule
      ],
      providers: [
        {provide: NbDialogService, useValue: spyNbDialogService},
        {provide: NbToastrService, useValue: spyNbToastrModule},
        FormBuilder

      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

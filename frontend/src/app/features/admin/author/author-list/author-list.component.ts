import { Component, OnInit } from '@angular/core';
import { Author } from 'src/app/core/models/author';
import { NbMenuItem, NbDialogService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorService } from 'src/app/core/services/author.service';
import { DialogBoxComponent } from 'src/app/shared/components/dialog-box/dialog-box.component';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.scss']
})
export class AuthorListComponent implements OnInit {

  authors: Array<Author>;
  columns: Array<string>;
  menu: Array<NbMenuItem>;

  constructor(
    private authorService: AuthorService,
    private nbDialogoService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {} 

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: {authors: Array<Author>}) => this.authors = data.authors);

    this.menu = [{title: 'Add', link: '/admin/author/add'}]

    this.columns = [
      "id",
      "first_name",
      "last_name",
      "created_at",
      "updated_at"
    ];
  }

  delete(author: Author) {
    try {
      this.nbDialogoService.open(DialogBoxComponent, {
        closeOnBackdropClick: false,
        context: {
          title: `Delete Author`,
          message: `Are you sure you want to delete ${author.first_name} ${author.last_name} ?`,
          action: () => this.authorService.deleteAuthor(author.id).subscribe(() => { 
            this.router.navigate(['/', 'admin', 'author', 'list']);
            this.authors = this.authors.filter(a => a.id !== author.id)
          } )  
      }});
    } catch(e) {
      console.log(e);
    }
  }
}

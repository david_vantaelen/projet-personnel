import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthorService } from 'src/app/core/services/author.service';
import { putAthor, AuthorForm, Author } from 'src/app/core/models/author';
import { ActivatedRoute } from '@angular/router';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-author-edit',
  templateUrl: './author-edit.component.html',
  styleUrls: ['./author-edit.component.scss']
})
export class AuthorEditComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {author: false};
  success: {[key: string]: boolean} = {author: false};

  author: Author;

  constructor(
    private authorService: AuthorService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) { super(); }

  ngOnInit(): void {
    try {
      this.forms.author = this.formBuilder.group(putAthor);
      this.forms.author.reset();

      this.activatedRoute.data.subscribe((data: {author: Author}) => {
        this.author = data.author; 
        this.forms.author.patchValue(
          {
            firstName: this.author.first_name,
            lastName: this.author.last_name
          });
      });
    } catch(e) {
      console.log(e);
    }
  }

  send() {
    try {
      this.error.author = false;
      this.success.author = false;
      this.submitted = true;

      if(this.forms.author.valid) {      
        this.authorService.editAuthor(this.author.id, <AuthorForm>this.forms.author.value).subscribe(data => {
          this.success.author = true;
          this.submitted = false;
          this.author = data;
        }, error => this.error.author = true);
      }
    } catch(e) {
      console.log(e);
    }
  }

}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthorService } from 'src/app/core/services/author.service';
import { postAuthor, AuthorForm, Author } from 'src/app/core/models/author';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-author-add',
  templateUrl: './author-add.component.html',
  styleUrls: ['./author-add.component.scss']
})
export class AuthorAddComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {author: false};
  success: {[key: string]: boolean} = {author: false};
  forms: {[key: string]: FormGroup} = {};

  @Output('add') onAddAuthor: EventEmitter<Author>;

  constructor(
    private authorService: AuthorService,
    private formBuilder: FormBuilder
  ) {
    super();
    this.onAddAuthor = new EventEmitter<Author>();
   }

  ngOnInit(): void {
    this.forms.author = this.formBuilder.group(postAuthor);
    this.forms.author.reset();
  }

  send() {
    try {
      this.error.author = false;
      this.success.author = false;
      this.submitted = true;

      if(this.forms.author.valid) {      
        this.authorService.createAuthor(<AuthorForm>this.forms.author.value).subscribe(data => {
          this.success.author = true;
          this.forms.author.reset();
          this.submitted = false;
          this.onAddAuthor.emit(data as Author);
        }, error => this.error.author = true);
      }
    } catch(e) {
      console.log(e);
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Edition } from 'src/app/core/models/edition';
import { NbMenuItem, NbDialogService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { EditionService } from 'src/app/core/services/edition.service';
import { DialogBoxComponent } from 'src/app/shared/components/dialog-box/dialog-box.component';


@Component({
  selector: 'app-edition-list',
  templateUrl: './edition-list.component.html',
  styleUrls: ['./edition-list.component.scss']
})
export class EditionListComponent implements OnInit {

  editions: Array<Edition>;
  columns: Array<string>;
  menu: Array<NbMenuItem>;

  constructor(
    private editionService: EditionService,
    private nbDialogoService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {} 

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: {editions: Array<Edition>}) => this.editions = data.editions);

    this.menu = [{title: 'Add', link: '/admin/edition/add'}]
    this.columns = [
      "id",
      "title",
      "author",
      "price",
      "stock",
      "created_at",
      "updated_at"
    ];
  }

  delete(edition: Edition) {
    try {
      this.nbDialogoService.open(DialogBoxComponent, {
        closeOnBackdropClick: false,
        context: {
          title: `Delete Edition`,
          message: `Are you sure you want to delete ${edition.title} ?`,
          action: () => this.editionService.deleteEdition(edition.id).subscribe(() => { 
            this.router.navigate(['/', 'admin', 'edition', 'list']);
            this.editions = this.editions.filter(e => e.id !== edition.id)
          }  
      )}});
    } catch(e) {
      console.log(e);
    }
  }
}

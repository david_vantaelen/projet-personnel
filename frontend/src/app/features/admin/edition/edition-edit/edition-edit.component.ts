import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { Book } from 'src/app/core/models/book';
import { Editor } from 'src/app/core/models/editor';
import { Collection } from 'src/app/core/models/collection';
import { Format } from 'src/app/core/models/format';
import { BookAddComponent } from '../../book/book-add/book-add.component';
import { CollectionAddComponent } from '../../collection/collection-add/collection-add.component';
import { EditorAddComponent } from '../../editor/editor-add/editor-add.component';
import { EditionService } from 'src/app/core/services/edition.service';
import { ActivatedRoute } from '@angular/router';
import { Edition, putEdition, EditionForm } from 'src/app/core/models/edition';
import { startWith, map } from 'rxjs/operators';
import { parseISO } from 'date-fns';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edition-edit',
  templateUrl: './edition-edit.component.html',
  styleUrls: ['./edition-edit.component.scss']
})
export class EditionEditComponent extends AbstractForm implements OnInit, AfterViewInit {

  readonly url: string = environment.domain + "/uploads/editions/";
  
  error: {[key: string]: boolean} = {edition: false};
  success: {[key: string]: boolean} = {edition: false};
  show: {[key: string]: boolean} = {book: false, editor: false, collection: false};
  options$: {[key: string]: Observable<any[]>} = {}

  books: Array<Book>;
  editors: Array<Editor>;
  collections: Array<Collection>;
  formats: Array<Format>;

  edition: Edition;

  image: File;

  @ViewChild(BookAddComponent) bookComponent: BookAddComponent;
  @ViewChild(CollectionAddComponent) collectionComponent: CollectionAddComponent;
  @ViewChild(EditorAddComponent) editorComponent: EditorAddComponent;
  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('fileText') fileField: ElementRef;
  @ViewChild('releaseAtField') releaseAtField: ElementRef;
  @ViewChild('collectionIdField') collectionIdField: ElementRef;


  constructor(
    private editionService: EditionService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef
    ) { super(); }

  ngOnInit(): void {
    try {
      this.books = [];
      this.collections = [];
      this.editors = [];
      this.formats = [];

      this.forms.edition = this.formBuilder.group(putEdition, {validators: [
        this.validImage.bind(this),
        this.validBookAutocomplete.bind(this),
        this.validEditorAutocomplete.bind(this),
        this.validCollectionAutocomplete.bind(this)
      ]});

      this.forms.edition.reset();
      
      this.activatedRoute.data.subscribe((data: {edition: Edition,  books: Array<Book>, collections: Array<Collection>, editors: Array<Editor>, formats: Array<Format> }) => {
        this.edition = data.edition;
        this.books = data.books;
        this.collections = data.collections;
        this.editors = data.editors;
        this.formats = data.formats;

        this.options$.book = of(this.books);
        this.options$.editor = of(this.editors);
        this.options$.collection = of(this.collections);

        this.forms.edition.patchValue(
        {
          description: this.edition.description !== null ? this.edition.description : '',
          price: this.edition.price,
          stock: this.edition.stock,
          releaseAt: parseISO(this.edition.release_at.toString()),
          bookId: this.formatBook(this.edition.book),
          formatId: this.edition.format.id.toString(),
          editorId: this.formatEditor(this.edition.editor),
          collectionId: this.edition.collection && this.formatCollection(this.edition.collection)
        });

        this.options$.book = this.forms.edition.get('bookId').valueChanges
        .pipe(
          startWith(''),
          map(str => this.filterBook(str))
        );

        this.options$.collection = this.forms.edition.get('collectionId').valueChanges
          .pipe(
            startWith(''),
            map(str => this.filterCollection(str))
          );

        this.options$.editor = this.forms.edition.get('editorId').valueChanges
          .pipe(
            startWith(''),
            map(str => this.filterEditor(str))
          );
        
      });
    } catch(e) {
      console.log(e);
    }
  }
  
  ngAfterViewInit(): void {
    this.collectionIdField.nativeElement.blur();
    this.cdr.detectChanges();
  }

  private validImage(group: FormGroup) {
    if(!group.get('image').value) return null;
    const images = this.fileInput.nativeElement.files;
    if(images && images.length) {
      if(images[0].size > 500000) group.get('image').setErrors({validImage: true});
    }
  }

  private validBookAutocomplete(group: FormGroup) {
    if(!group.get('bookId').value) return null;
    if(!this.books.find((o: Book) => { 
      const opt = this.formatBook(o).toLowerCase();
      return group.get('bookId').value.toString().toLowerCase().trim() === opt;
    })) group.get('bookId').setErrors({validBookAutocomplete: true});
  }

  private filterBook(value: string): Array<Book> {
    return this.books.filter((o: Book) => { 
      const opt = this.formatBook(o).toLowerCase();
      return value && opt.includes(value.toString().toLowerCase());
    });
  }

  formatBook(book: Book): string {
    return `${book.title} (id#${book.id})`.replace(/\s{2,}/gms, ' ');
  }

  private validCollectionAutocomplete(group: FormGroup) {
    if(!group.get('collectionId').value) return null;
    if(!this.collections.find((o: Collection) => { 
      const opt = this.formatCollection(o).toLowerCase();
      return group.get('collectionId').value.toString().toLowerCase().trim() === opt;
    })) group.get('collectionId').setErrors({validCollectionAutocomplete: true});
  }

  private filterCollection(value: string): Array<Collection> {
    return this.collections.filter((o: Collection) => { 
      const opt = this.formatCollection(o).toLowerCase();
      return value && opt.includes(value.toString().toLowerCase());
    });
  }

  formatCollection(collection: Collection): string {
    return `${collection.collection} (id#${collection.id})`.replace(/\s{2,}/gms, ' ');
  }


  private validEditorAutocomplete(group: FormGroup) {
    if(!group.get('editorId').value) return null;
    if(!this.editors.find((o: Editor) => { 
      const opt = this.formatEditor(o).toLowerCase();
      return group.get('editorId').value.toString().toLowerCase().trim() === opt;
    })) group.get('editorId').setErrors({validEditorAutocomplete: true});
  }

  private filterEditor(value: string): Array<Editor> {
    return this.editors.filter((o: Editor) => { 
      const opt = this.formatEditor(o).toLowerCase();
      return value && opt.includes(value.toString().toLowerCase());
    });
  }

  formatEditor(editor: Editor): string {
    return `${editor.editor} (id#${editor.id})`.replace(/\s{2,}/gms, ' ');
  }

  onFileChanged(event) {
    if(event.target.files && event.target.files.length) {
      this.image = event.target.files[0];
      this.fileField.nativeElement.value = event.target.files[0].name;
    } else {
      this.image = null;
      this.fileField.nativeElement.value = '';
    }
  }

  showAddBook(value: boolean) {
    this.show.book = value;
  }

  showAddCollection(value: boolean) {
    this.show.collection = value;
  }

  showAddEditor(value: boolean) {
    this.show.editor = value;
  }

  sendBook(book: Book) {
    try {
      this.books.push(book);
      this.bookComponent.success.book = false;
      this.forms.edition.controls.bookId.patchValue(this.formatBook(book));
      this.show.book = false;
    } catch(e) {
      console.log(e);
    }
  }

  sendCollection(collection: Collection) {
    try {
      this.collections.push(collection);
      this.collectionComponent.success.collection = false;
      this.forms.edition.controls.collectionId.patchValue(this.formatCollection(collection));
      this.show.collection = false;
    } catch(e) {
      console.log(e);
    }
  }

  sendEditor(editor: Editor) {
    try {
      this.editors.push(editor);
      this.editorComponent.success.editor = false;
      this.forms.edition.controls.editorId.patchValue(this.formatEditor(editor));
      this.show.editor = false;
    } catch(e) {
      console.log(e);
    }
  }

  send() {
    try {
      this.error.edition = false;
      this.success.edition = false;
      this.submitted = true;
      
      if(this.forms.edition.valid) {
        let edition = <EditionForm>this.forms.edition.value;

        ['bookId', 'collectionId', 'editorId'].forEach( v => {
          if(this.forms.edition.get(v).value == null) return;
          let matches: any[] = this.forms.edition.get(v).value.match(/\(id\#([0-9]+)\)$/);

          if(!(matches && matches[1]))  { 
            this.error.book = true; 
          } else edition[v] = parseInt(matches[1]);
        });

        if(this.error.edition) return;

        edition.releaseAt = this.releaseAtField.nativeElement.value

        this.editionService.editEdition(this.edition.id, edition).subscribe(data => {

          if(this.image) {
            const formData = new FormData();
            formData.append('image', this.image, this.image.name);
            this.editionService.editImage(this.edition.id, formData).subscribe(data => {
              this.success.edition = true;
              this.submitted = false;
              this.edition = data;
            }, error => this.error.edition = true)
          } else {
            this.success.edition = true;
            this.submitted = false;
          }
        }, error => this.error.edition = true);
      }
    } catch(e) {
      console.log(e);
    }
  }
}

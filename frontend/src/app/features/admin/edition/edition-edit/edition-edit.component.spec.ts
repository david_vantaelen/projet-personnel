import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditionEditComponent } from './edition-edit.component';

describe('EditionEditComponent', () => {
  let component: EditionEditComponent;
  let fixture: ComponentFixture<EditionEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

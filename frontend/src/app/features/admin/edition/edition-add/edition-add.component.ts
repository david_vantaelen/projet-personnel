import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { of, Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { postEdition } from 'src/app/core/models/edition';
import { EditionService } from 'src/app/core/services/edition.service';
import { ActivatedRoute } from '@angular/router';
import { Book } from 'src/app/core/models/book';
import { Collection } from 'src/app/core/models/collection';
import { Format } from 'src/app/core/models/format';
import { BookAddComponent } from '../../book/book-add/book-add.component';
import { CollectionAddComponent } from '../../collection/collection-add/collection-add.component';
import { Editor } from 'src/app/core/models/editor';
import { EditorAddComponent } from '../../editor/editor-add/editor-add.component';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-edition-add',
  templateUrl: './edition-add.component.html',
  styleUrls: ['./edition-add.component.scss']
})
export class EditionAddComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {edition: false};
  success: {[key: string]: boolean} = {edition: false};
  show: {[key: string]: boolean} = {book: false, editor: false, collection: false};
  options$: {[key: string]: Observable<any[]>} = {}

  books: Array<Book>;
  editors: Array<Editor>;
  collections: Array<Collection>;
  formats: Array<Format>;

  image: File;

  @ViewChild(BookAddComponent) bookComponent: BookAddComponent;
  @ViewChild(CollectionAddComponent) collectionComponent: CollectionAddComponent;
  @ViewChild(EditorAddComponent) editorComponent: EditorAddComponent;
  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('fileText') fileField: ElementRef;
  @ViewChild('releaseAtField') releaseAtField: ElementRef;

  constructor(
    private editionService: EditionService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder
    ) { super(); }

  ngOnInit(): void {
    try {
      this.activatedRoute.data.subscribe((data: { books: Array<Book>, collections: Array<Collection>, editors: Array<Editor>, formats: Array<Format> }) => {
        this.books = data.books;
        this.collections = data.collections;
        this.editors = data.editors;
        this.formats = data.formats;

        this.options$.book = of(this.books);
        this.options$.editor = of(this.editors);
        this.options$.collection = of(this.collections);
      });

      this.forms.edition = this.formBuilder.group(postEdition, {validators: [
        this.validImage.bind(this),
        this.validBookAutocomplete.bind(this),
        this.validEditorAutocomplete.bind(this),
        this.validCollectionAutocomplete.bind(this)
      ]});

      this.options$.book = this.forms.edition.get('bookId').valueChanges
        .pipe(
          startWith(''),
          map(str => this.filterBook(str))
        );

      this.options$.collection = this.forms.edition.get('collectionId').valueChanges
        .pipe(
          startWith(''),
          map(str => this.filterCollection(str))
        );

      this.options$.editor = this.forms.edition.get('editorId').valueChanges
        .pipe(
          startWith(''),
          map(str => this.filterEditor(str))
        );

    } catch(e) {
      console.log(e);
    }
  }

  private validImage(group: FormGroup) {
    if(!group.get('image').value) return null;
    const images = this.fileInput.nativeElement.files;
    if(images && images.length) {
      if(images[0].size > 500000) group.get('image').setErrors({validImage: true});
    }
  }

  private validBookAutocomplete(group: FormGroup) {
    if(!group.get('bookId').value) return null;
    if(!this.books.find((o: Book) => { 
      const opt = this.formatBook(o).toLowerCase();
      return group.get('bookId').value.toString().toLowerCase().trim() === opt;
    })) group.get('bookId').setErrors({validBookAutocomplete: true});
  }

  private filterBook(value: string): Array<Book> {
    return this.books.filter((o: Book) => { 
      const opt = this.formatBook(o).toLowerCase();
      return value && opt.includes(value.toString().toLowerCase());
    });
  }

  formatBook(book: Book): string {
    return `${book.title} (id#${book.id})`.replace(/\s{2,}/gms, ' ');
  }

  private validCollectionAutocomplete(group: FormGroup) {
    if(!group.get('collectionId').value) return null;
    if(!this.collections.find((o: Collection) => { 
      const opt = this.formatCollection(o).toLowerCase();
      return group.get('collectionId').value.toString().toLowerCase().trim() === opt;
    })) group.get('collectionId').setErrors({validCollectionAutocomplete: true});
  }

  private filterCollection(value: string): Array<Collection> {
    return this.collections.filter((o: Collection) => { 
      const opt = this.formatCollection(o).toLowerCase();
      return value && opt.includes(value.toString().toLowerCase());
    });
  }

  formatCollection(collection: Collection): string {
    return `${collection.collection} (id#${collection.id})`.replace(/\s{2,}/gms, ' ');
  }


  private validEditorAutocomplete(group: FormGroup) {
    if(!group.get('editorId').value) return null;
    if(!this.editors.find((o: Editor) => { 
      const opt = this.formatEditor(o).toLowerCase();
      return group.get('editorId').value.toString().toLowerCase().trim() === opt;
    })) group.get('editorId').setErrors({validEditorAutocomplete: true});
  }

  private filterEditor(value: string): Array<Editor> {
    return this.editors.filter((o: Editor) => { 
      const opt = this.formatEditor(o).toLowerCase();
      return value && opt.includes(value.toString().toLowerCase());
    });
  }

  formatEditor(editor: Editor): string {
    return `${editor.editor} (id#${editor.id})`.replace(/\s{2,}/gms, ' ');
  }

  onFileChanged(event) {
    if(event.target.files && event.target.files.length) {
      this.image = event.target.files[0];
      this.fileField.nativeElement.value = event.target.files[0].name;
    } else {
      this.image = null;
      this.fileField.nativeElement.value = '';
    }
  }

  showAddBook(value: boolean) {
    this.show.book = value;
  }

  showAddCollection(value: boolean) {
    this.show.collection = value;
  }

  showAddEditor(value: boolean) {
    this.show.editor = value;
  }

  sendBook(book: Book) {
    try {
      this.books.push(book);
      this.bookComponent.success.book = false;
      this.forms.edition.controls.bookId.patchValue(this.formatBook(book));
      this.show.book = false;
    } catch(e) {
      console.log(e);
    }
  }

  sendCollection(collection: Collection) {
    try {
      this.collections.push(collection);
      this.collectionComponent.success.collection = false;
      this.forms.edition.controls.collectionId.patchValue(this.formatCollection(collection));
      this.show.collection = false;
    } catch(e) {
      console.log(e);
    }
  }

  sendEditor(editor: Editor) {
    try {
      this.editors.push(editor);
      this.editorComponent.success.editor = false;
      this.forms.edition.controls.editorId.patchValue(this.formatEditor(editor));
      this.show.editor = false;
    } catch(e) {
      console.log(e);
    }
  }

  send() {
    try {
      this.error.edition = false;
      this.success.edition = false;
      this.submitted = true;
      
      if(this.forms.edition.valid) {
        const formData = new FormData();

        ['bookId', 'collectionId', 'editorId'].forEach( v => {
          if(this.forms.edition.get(v).value == null) return;
          let matches: any[] = this.forms.edition.get(v).value.match(/\(id\#([0-9]+)\)$/);

          if(!(matches && matches[1]))  { 
            this.error.book = true; 
          } else formData.append(v, matches[1]);

        });

        if(this.error.book) return;
        
        formData.append('formatId', this.forms.edition.controls.formatId.value);
        if(this.forms.edition.controls.description) formData.append('description', this.forms.edition.controls.description.value);
        formData.append('price', this.forms.edition.controls.price.value);
        formData.append('releaseAt', this.releaseAtField.nativeElement.value);
        formData.append('stock', this.forms.edition.controls.stock.value);
        if(this.image) formData.append('image', this.image, this.image.name);

        this.editionService.createEdition(formData).subscribe(data => {
          this.success.edition = true;
          this.forms.edition.reset();

          ['bookId', 'collectionId', 'editorId'].forEach(e => {
            this.forms.edition.get(e).patchValue('');
            this.forms.edition.get(e).markAsUntouched();
          })
          
          this.submitted = false;
        }, error => this.error.edition = true);
      }
    } catch(e) {
      console.log(e);
    }
  }
}

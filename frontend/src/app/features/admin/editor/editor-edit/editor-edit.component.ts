import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Editor, postEditor, EditorForm } from 'src/app/core/models/editor';
import { EditorService } from 'src/app/core/services/editor.service';
import { ActivatedRoute } from '@angular/router';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-editor-edit',
  templateUrl: './editor-edit.component.html',
  styleUrls: ['./editor-edit.component.scss']
})
export class EditorEditComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {editor: false};
  success: {[key: string]: boolean} = {editor: false};

  editor: Editor;

  constructor(
    private editorService: EditorService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) { super(); }

  ngOnInit(): void {
    try {
      this.forms.editor = this.formBuilder.group(postEditor);
      this.forms.editor.reset();
      
      this.activatedRoute.data.subscribe((data: {editor: Editor}) => {
        this.editor = data.editor; 
        this.forms.editor.patchValue(
          {
            editor: this.editor.editor
          });
      });
    } catch(e) {
      console.log(e);
    }
  }

  send() {
    try {
      this.error.editor = false;
      this.success.editor = false;
      this.submitted = true;

      if(this.forms.editor.valid) {      
        this.editorService.editEditor(this.editor.id, <EditorForm>this.forms.editor.value).subscribe(data => {
          this.success.editor = true;
          this.submitted = false;
          this.editor = data;
        }, error => this.error.editor = true);
      }
    } catch(e) {
      console.log(e);
    }
  }
}

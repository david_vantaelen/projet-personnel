import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EditorService } from 'src/app/core/services/editor.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { postEditor, EditorForm, Editor } from 'src/app/core/models/editor';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-editor-add',
  templateUrl: './editor-add.component.html',
  styleUrls: ['./editor-add.component.scss']
})
export class EditorAddComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {editor: false};
  success: {[key: string]: boolean} = {editor: false};
  forms: {[key: string]: FormGroup} = {};

  @Output('add') onAddEditor: EventEmitter<Editor>;

  constructor(
    private editorService: EditorService,
    private formBuilder: FormBuilder
  ) { 
    super();
    this.onAddEditor = new EventEmitter<Editor>()
  }

  ngOnInit(): void {
    this.forms.editor = this.formBuilder.group(postEditor);
    this.forms.editor.reset();
  }

  send() {
    try {
      this.error.editor = false;
      this.success.editor = false;
      this.submitted = true;

      if(this.forms.editor.valid) {      
        this.editorService.createEditor(<EditorForm>this.forms.editor.value).subscribe(data => {
          this.success.editor = true;
          this.forms.editor.reset();
          this.submitted = false;
          this.onAddEditor.emit(data as Editor)
        }, error => this.error.editor = true);
      }
    } catch(e) {
      console.log(e);
    }
  }
}

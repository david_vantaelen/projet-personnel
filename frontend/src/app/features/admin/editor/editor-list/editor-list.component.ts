import { Component, OnInit } from '@angular/core';
import { Editor } from 'src/app/core/models/editor';
import { EditorService } from 'src/app/core/services/editor.service';
import { NbMenuItem, NbDialogService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogBoxComponent } from 'src/app/shared/components/dialog-box/dialog-box.component';

@Component({
  selector: 'app-editor-list',
  templateUrl: './editor-list.component.html',
  styleUrls: ['./editor-list.component.scss']
})
export class EditorListComponent implements OnInit {

  editors: Array<Editor>;
  columns: Array<string>;
  menu: Array<NbMenuItem>;

  constructor(
    private editorService: EditorService,
    private nbDialogoService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {} 

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: {editors: Array<Editor>}) => this.editors = data.editors);

    this.menu = [{title: 'Add', link: '/admin/editor/add'}]

    this.columns = [
      "id",
      "editor",
      "created_at",
      "updated_at"
    ];
  }

  delete(editor: Editor) {
    try {
      this.nbDialogoService.open(DialogBoxComponent, {
        closeOnBackdropClick: false,
        context: {
          title: `Delete Editor`,
          message: `Are you sure you want to delete ${editor.editor} ?`,
          action: () => this.editorService.deleteEditor(editor.id).subscribe(() => { 
            this.router.navigate(['/', 'admin', 'editor', 'list']);
            this.editors = this.editors.filter(e => e.id !== editor.id)
          } )  
      }});
    } catch(e) {
      console.log(e);
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { FormatService } from 'src/app/core/services/format.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { postFormat, FormatForm } from 'src/app/core/models/format';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-format-add',
  templateUrl: './format-add.component.html',
  styleUrls: ['./format-add.component.scss']
})
export class FormatAddComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {format: false};
  success: {[key: string]: boolean} = {format: false};
  forms: {[key: string]: FormGroup} = {};

  constructor(
    private formatService: FormatService,
    private formBuilder: FormBuilder
  ) { super(); }

  ngOnInit(): void {
    this.forms.format = this.formBuilder.group(postFormat);
    this.forms.format.reset();
  }

  send() {
    try {
      this.error.format = false;
      this.success.format = false;
      this.submitted = true;

      if(this.forms.format.valid) {   
        
        forkJoin([
          this.formatService.hasFormat(this.forms.format.value.format)
          
        ]).subscribe(r => {
          if(r[0]) this.forms.format.get('format').setErrors({formatExists: true})

          if(!r[0]) {
            this.formatService.createFormat(<FormatForm>this.forms.format.value).subscribe(data => {
              this.success.format = true;
              this.forms.format.reset();
              this.submitted = false;
            }, error => this.error.format = true);

          } else this.error.format = true;
        });
      }
    } catch(e) {
      console.log(e);
    }
  }
}

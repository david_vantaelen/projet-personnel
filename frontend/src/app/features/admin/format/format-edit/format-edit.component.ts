import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Format, postFormat, FormatForm } from 'src/app/core/models/format';
import { FormatService } from 'src/app/core/services/format.service';
import { ActivatedRoute } from '@angular/router';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-format-edit',
  templateUrl: './format-edit.component.html',
  styleUrls: ['./format-edit.component.scss']
})
export class FormatEditComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {format: false};
  success: {[key: string]: boolean} = {format: false};

  format: Format;

  constructor(
    private formatService: FormatService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) { super(); }

  ngOnInit(): void {
    try {
      this.forms.format = this.formBuilder.group(postFormat);
      this.forms.format.reset();
      
      this.activatedRoute.data.subscribe((data: {format: Format}) => {
        this.format = data.format; 
        this.forms.format.patchValue(
          {
            format: this.format.format
          });
      });
    } catch(e) {
      console.log(e);
    }
  }

  send() {
    try {
      this.error.format = false;
      this.success.format = false;
      this.submitted = true;

      if(this.forms.format.valid) {     
        
        forkJoin([
          this.formatService.hasFormat(this.forms.format.value.format)
          
        ]).subscribe(r => {
          let hasError = false;

          if(r[0] && this.forms.format.value.format !== this.format.format){
            this.forms.format.get('format').setErrors({formatExists: true})
            hasError = true;
          }

          if(!hasError) {
            this.formatService.editFormat(this.format.id, <FormatForm>this.forms.format.value).subscribe(data => {
              this.success.format = true;
              this.submitted = false;
              this.format = data;
            }, error => this.error.format = true);

          } else this.error.format = true;
        });
      }
    } catch(e) {
      console.log(e);
    }
  }
}

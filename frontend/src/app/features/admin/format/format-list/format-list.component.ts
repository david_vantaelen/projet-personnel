import { Component, OnInit } from '@angular/core';
import { Format } from 'src/app/core/models/format';
import { NbMenuItem, NbDialogService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { FormatService } from 'src/app/core/services/format.service';
import { DialogBoxComponent } from 'src/app/shared/components/dialog-box/dialog-box.component';

@Component({
  selector: 'app-format-list',
  templateUrl: './format-list.component.html',
  styleUrls: ['./format-list.component.scss']
})
export class FormatListComponent implements OnInit {

  formats: Array<Format>;
  columns: Array<string>;
  menu: Array<NbMenuItem>;

  constructor(
    private formatService: FormatService,
    private nbDialogoService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {} 

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: {formats: Array<Format>}) => this.formats = data.formats);

    this.menu = [{title: 'Add', link: '/admin/format/add'}]

    this.columns = [
      "id",
      "format",
      "created_at",
      "updated_at"
    ];
  }

  delete(format: Format) {
    try {
      this.nbDialogoService.open(DialogBoxComponent, {
        closeOnBackdropClick: false,
        context: {
          title: `Delete Format`,
          message: `Are you sure you want to delete ${format.format} ?`,
          action: () => this.formatService.deleteFormat(format.id).subscribe(() => { 
            this.router.navigate(['/', 'admin', 'format', 'list']);
            this.formats = this.formats.filter(f => f.id !== format.id)
          } )  
      }});
    } catch(e) {
      console.log(e);
    }
  }

}

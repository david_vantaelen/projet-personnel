import { Component, OnInit } from '@angular/core';
import { Collection } from 'src/app/core/models/collection';
import { NbMenuItem, NbDialogService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { CollectionService } from 'src/app/core/services/collection.service';
import { DialogBoxComponent } from 'src/app/shared/components/dialog-box/dialog-box.component';

@Component({
  selector: 'app-collection-list',
  templateUrl: './collection-list.component.html',
  styleUrls: ['./collection-list.component.scss']
})
export class CollectionListComponent implements OnInit {

  collections: Array<Collection>;
  columns: Array<string>;
  menu: Array<NbMenuItem>;

  constructor(
    private collectionService: CollectionService,
    private nbDialogoService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {} 

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: {collections: Array<Collection>}) => this.collections = data.collections);

    this.menu = [{title: 'Add', link: '/admin/collection/add'}]

    this.columns = [
      "id",
      "collection",
      "created_at",
      "updated_at"
    ];
  }

  delete(collection: Collection) {
    try {
      this.nbDialogoService.open(DialogBoxComponent, {
        closeOnBackdropClick: false,
        context: {
          title: `Delete Collection`,
          message: `Are you sure you want to delete ${collection.collection} ?`,
          action: () => this.collectionService.deleteCollection(collection.id).subscribe(() => { 
            this.router.navigate(['/', 'admin', 'collection', 'list']);
            this.collections = this.collections.filter(c => c.id !== collection.id)
          })  
      }});
    } catch(e) {
      console.log(e);
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Collection, CollectionForm, putCollection } from 'src/app/core/models/collection';
import { CollectionService } from 'src/app/core/services/collection.service';
import { ActivatedRoute } from '@angular/router';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-collection-edit',
  templateUrl: './collection-edit.component.html',
  styleUrls: ['./collection-edit.component.scss']
})
export class CollectionEditComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {collection: false};
  success: {[key: string]: boolean} = {collection: false};

  collection: Collection;

  constructor(
    private collectionService: CollectionService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) { super(); }

  ngOnInit(): void {
    try {
      this.forms.collection = this.formBuilder.group(putCollection);
      this.forms.collection.reset();
      
      this.activatedRoute.data.subscribe((data: {collection: Collection}) => {
        this.collection = data.collection; 
        this.forms.collection.patchValue(
          {
            collection: this.collection.collection
          });
      });
    } catch(e) {
      console.log(e);
    }
  }

  send() {
    try {
      this.error.collection = false;
      this.success.collection = false;
      this.submitted = true;

      if(this.forms.collection.valid) {      
        this.collectionService.editCollection(this.collection.id, <CollectionForm>this.forms.collection.value).subscribe(data => {
          this.success.collection = true;
          this.submitted = false;
          this.collection = data;
        }, error => this.error.collection = true);
      }
    } catch(e) {
      console.log(e);
    }
  }

}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CollectionService } from 'src/app/core/services/collection.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { postCollection, CollectionForm, Collection } from 'src/app/core/models/collection';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-collection-add',
  templateUrl: './collection-add.component.html',
  styleUrls: ['./collection-add.component.scss']
})
export class CollectionAddComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {collection: false};
  success: {[key: string]: boolean} = {collection: false};

  @Output('add') onAddCollection: EventEmitter<Collection>;

  constructor(
    private collectionService: CollectionService,
    private formBuilder: FormBuilder
  ) {
    super();
    this.onAddCollection = new EventEmitter<Collection>();
   }

  ngOnInit(): void {
    this.forms.collection = this.formBuilder.group(postCollection);
    this.forms.collection.reset();
  }

  send() {
    try {
      this.error.collection = false;
      this.success.collection = false;
      this.submitted = true;

      if(this.forms.collection.valid) {      
        this.collectionService.createCollection(<CollectionForm>this.forms.collection.value).subscribe(data => {
          this.success.collection = true;
          this.forms.collection.reset();
          this.submitted = false;
          this.onAddCollection.emit(data as Collection);
        }, error => this.error.collection = true);
      }
    } catch(e) {
      console.log(e);
    }
  }
}

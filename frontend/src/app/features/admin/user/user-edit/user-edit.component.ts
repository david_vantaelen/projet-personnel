import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Group } from 'src/app/core/models/group';
import { UserService } from 'src/app/core/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserForm, User, putUser } from 'src/app/core/models/user';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';
import { forkJoin } from 'rxjs';
import { TokenService } from 'src/app/core/services/token.service';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})

export class UserEditComponent extends AbstractForm implements OnInit {
  
  error: {[key: string]: boolean} = {user: false};
  success: {[key: string]: boolean} = {user: false};

  groups: Array<Group>;
  user: User;

  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private tokenService: TokenService,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { super(); }
  
  ngOnInit(): void {
    try {
      this.groups = [];
      this.forms.user = this.formBuilder.group(putUser, {validator: this.matchPassword});
      this.forms.user.reset();
    
      this.activatedRoute.data.subscribe((data: { user: User, groups: Array<Group> }) => {
        this.user = data.user; 
        this.groups = data.groups;
        this.forms.user.patchValue(
          {
            firstName: this.user.first_name,
            lastName: this.user.last_name,
            email: this.user.email,
            groupId: this.user.group.id.toString(),
          });

          if(this.user.main_address){
            this.forms.user.controls.address.patchValue(
            {
              street: this.user.main_address.street,
              city: this.user.main_address.city,
              postCode: this.user.main_address.post_code,
              country: this.user.main_address.country,
            });
          }
      });
    } catch(e) {
      console.log(e);
    }
  }

  private matchPassword(group: FormGroup) {
    if(group.get('password').value === '') return;
    const valid =  group.get('password').value === group.get('confirmPassword').value;
    if(!valid) group.get('confirmPassword').setErrors({matchPassword: true})
  }

  IsAuthProfile(): boolean {
    let decoded = this.tokenService.decode();
    if(!decoded?.username) return false;
    return decoded?.username === this.user.username;
  }

  send() {
    try {
      this.error.user = false;
      this.success.user = false;
      this.submitted = true;

      if(this.forms.user.valid) {
        let values = this.forms.user.value;
        delete values.confirmPassword;
        let user = <UserForm>values;
        user.address.isMain = true;

        forkJoin([
          this.userService.hasEmail(user.email),
          this.userService.hasUsername(user.username)

        ]).subscribe(r => {
          let hasError = false;

          if(r[0] && user.email !== this.user.email){
            this.forms.user.get('email').setErrors({emailExists: true})
            hasError = true;
          }
          if(r[1] && user.username !== this.user.username){
            this.forms.user.get('username').setErrors({usernameExists: true})
            hasError = true;
          }

          if(!hasError) {
            this.userService.editUser(this.user.id, user).subscribe(data => {
              this.userService.getUser(this.user.id).subscribe(data => {
                this.success.user = true;
                this.submitted = false;
                if(this.IsAuthProfile()) {
                  if(user.password || this.user.group.id != user.groupId) {
                    this.tokenService.token = null;
                    this.router.navigate(['/', 'auth', 'login']);
                  }
                }
                this.user = data; 
              }, error => { this.error.user = true });
            }, error => { this.error.user = true });
          } else this.error.user = true;
        });
      }
    } catch(e) {
      console.log(e);
    }
  }
}

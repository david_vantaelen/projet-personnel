import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { postUser, UserForm } from 'src/app/core/models/user';
import { Group } from 'src/app/core/models/group';
import { UserService } from 'src/app/core/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {user: false};
  success: {[key: string]: boolean} = {user: false};

  groups: Array<Group>;

  constructor(
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { super(); }

  ngOnInit(): void {
    this.groups = [];
    this.activatedRoute.data.subscribe((data: { groups: Array<Group> }) => this.groups = data.groups);
    this.forms.user = this.formBuilder.group(postUser, {validator: this.matchPassword});
    this.forms.user.reset();
  }

  private matchPassword(group: FormGroup) {
    const valid =  group.get('password').value === group.get('confirmPassword').value;
    if(valid) return null;
    group.get('confirmPassword').setErrors({matchPassword: true})
  }

  send() {
    try {
      this.error.user = false;
      this.success.user = false;
      this.submitted = true;
      
      if(this.forms.user.valid) {
        let values = this.forms.user.value;
        delete values.confirmPassword;

        let user = <UserForm>values;
        user.address.isMain = true;

        forkJoin([
          this.userService.hasEmail(user.email),
          this.userService.hasUsername(user.username)
          
        ]).subscribe(r => {
          if(r[0]) this.forms.user.get('email').setErrors({emailExists: true})
          if(r[1]) this.forms.user.get('username').setErrors({usernameExists: true})

          if(!r[0] && !r[1]) {
            this.userService.createUser(user).subscribe(data => {
              this.success.user = true;
              this.forms.user.reset();
              this.submitted = false;
    
            }, error => this.error.user = true);
          } else this.error.user = true;
        });
      }
    } catch(e) {
      console.log(e);
    }
  }
}

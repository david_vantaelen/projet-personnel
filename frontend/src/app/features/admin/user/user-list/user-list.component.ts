import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/models/user';
import { NbMenuItem, NbDialogService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogBoxComponent } from 'src/app/shared/components/dialog-box/dialog-box.component';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  users: Array<User>;
  columns: Array<string>;
  menu: Array<NbMenuItem>;

  constructor(
    private userService: UserService,
    private nbDialogoService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {} 

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: {users: Array<User>}) => this.users = data.users);

    this.menu = [{title: 'Add', link: '/admin/user/add'}]

    this.columns = [
      "id",
      "first_name",
      "last_name",
      "username",
      "email",
      "group_name",
      "roles",
      "created_at",
      "updated_at"
      
    ];
  }

  delete(user: User) {
    try {
      this.nbDialogoService.open(DialogBoxComponent, {
        closeOnBackdropClick: false,
        context: {
          title: `Delete User`,
          message: `Are you sure you want to delete ${user.username} ?`,
          action: () => this.userService.deleteUser(user.id).subscribe(() => { 
            this.router.navigate(['/', 'admin', 'user', 'list']);
            this.users = this.users.filter(u => u.id !== user.id)
          } )  
      }});
    } catch(e) {
      console.log(e);
    }
  }
}

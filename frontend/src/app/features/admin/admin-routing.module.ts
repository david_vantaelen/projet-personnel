import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { EditionListComponent } from './edition/edition-list/edition-list.component';
import { EditionAddComponent } from './edition/edition-add/edition-add.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserAddComponent } from './user/user-add/user-add.component';
import { GetUsersResolver, GetOneUserResolver } from 'src/app/core/resolvers/user.resolver';
import { GetEditionsResolver, GetOneEditionResolver } from 'src/app/core/resolvers/edition.resolver';
import { GetGroupsResolver } from 'src/app/core/resolvers/group.resolver';
import { BookAddComponent } from './book/book-add/book-add.component';
import { BookListComponent } from './book/book-list/book-list.component';
import { GetBooksResolver, GetOneBookResolver } from 'src/app/core/resolvers/book.resolver';
import { GetAuthorsResolver, GetOneAuthorResolver } from 'src/app/core/resolvers/author.resolver';
import { AuthorListComponent } from './author/author-list/author-list.component';
import { AuthorAddComponent } from './author/author-add/author-add.component';
import { AuthorEditComponent } from './author/author-edit/author-edit.component';
import { HasAuthorGuard } from 'src/app/core/guards/has-author.guard';
import { BookEditComponent } from './book/book-edit/book-edit.component';
import { HasBookGuard } from 'src/app/core/guards/has-book.guard';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { HasUserGuard } from 'src/app/core/guards/has-user.guard';
import { CollectionListComponent } from './collection/collection-list/collection-list.component';
import { GetCollectionsResolver, GetOneCollectionResolver } from 'src/app/core/resolvers/collection.resolver';
import { CollectionAddComponent } from './collection/collection-add/collection-add.component';
import { CollectionEditComponent } from './collection/collection-edit/collection-edit.component';
import { HasCollectionGuard } from 'src/app/core/guards/has-collection.guard';
import { FormatListComponent } from './format/format-list/format-list.component';
import { FormatAddComponent } from './format/format-add/format-add.component';
import { FormatEditComponent } from './format/format-edit/format-edit.component';
import { GetFormatsResolver, GetOneFormatResolver } from 'src/app/core/resolvers/format.resolver';
import { HasFormatGuard } from 'src/app/core/guards/has-format.guard';
import { EditorListComponent } from './editor/editor-list/editor-list.component';
import { GetEditorsResolver, GetOneEditorResolver } from 'src/app/core/resolvers/editor.resolver';
import { EditorAddComponent } from './editor/editor-add/editor-add.component';
import { EditorEditComponent } from './editor/editor-edit/editor-edit.component';
import { HasEditorGuard } from 'src/app/core/guards/has-editor.guard';
import { EditionEditComponent } from './edition/edition-edit/edition-edit.component';
import { HasEditionGuard } from 'src/app/core/guards/has-edition.guard';

const routes: Routes = [{ 
  path: '', 
  component: AdminComponent, 
  children: [
      {
        path: '',
        redirectTo: 'user/list',
        pathMatch: 'full'
      },
      {
        path: 'user/list',
        component: UserListComponent,
        resolve: { 
          users: GetUsersResolver 
        }
      },
      {
        path: 'user/add',
        component: UserAddComponent,
        resolve: { 
          groups: GetGroupsResolver 
        }
      },
      {
        path: 'user/edit/:id',
        component: UserEditComponent,
        canActivate: [HasUserGuard],
        resolve: { 
          user: GetOneUserResolver,
          groups: GetGroupsResolver 
        }
      },
      {
        path: 'author/list',
        component: AuthorListComponent,
        resolve: { 
          authors: GetAuthorsResolver 
        }
      },
      {
        path: 'author/add', 
        component: AuthorAddComponent
      },
      {
        path: 'author/edit/:id', 
        component: AuthorEditComponent,
        canActivate: [HasAuthorGuard],
        resolve: { 
          author: GetOneAuthorResolver 
        }
      },
      {
        path: 'book/list',
        component: BookListComponent,
        resolve: { 
          books: GetBooksResolver 
        }
      },
      {
        path: 'book/add',
        component: BookAddComponent,
        resolve: { 
          authors: GetAuthorsResolver 
        }
      },
      {
        path: 'book/edit/:id',
        component: BookEditComponent,
        canActivate: [HasBookGuard],
        resolve: { 
          book: GetOneBookResolver, 
          authors: GetAuthorsResolver 
        }
      },
      {
        path: 'collection/list',
        component: CollectionListComponent,
        resolve: { 
          collections: GetCollectionsResolver 
        }
      },
      {
        path: 'collection/add', 
        component: CollectionAddComponent
      },
      {
        path: 'collection/edit/:id', 
        component: CollectionEditComponent,
        canActivate: [HasCollectionGuard],
        resolve: { 
          collection: GetOneCollectionResolver 
        }
      },
      {
        path: 'format/list',
        component: FormatListComponent,
        resolve: { 
          formats: GetFormatsResolver 
        }
      },
      {
        path: 'format/add', 
        component: FormatAddComponent
      },
      {
        path: 'format/edit/:id', 
        component: FormatEditComponent,
        canActivate: [HasFormatGuard],
        resolve: { 
          format: GetOneFormatResolver 
        }
      },
      {
        path: 'editor/list',
        component: EditorListComponent,
        resolve: { 
          editors: GetEditorsResolver 
        }
      },
      {
        path: 'editor/add', 
        component: EditorAddComponent
      },
      {
        path: 'editor/edit/:id', 
        component: EditorEditComponent,
        canActivate: [HasEditorGuard],
        resolve: { 
          editor: GetOneEditorResolver 
        }
      },
      {
        path: 'edition/list',
        component: EditionListComponent,
        resolve: {
          editions: GetEditionsResolver
        }
      },
      {
        path: 'edition/add',
        component: EditionAddComponent,
        resolve: { 
          books: GetBooksResolver,
          authors: GetAuthorsResolver,
          editors: GetEditorsResolver,
          collections: GetCollectionsResolver,
          formats: GetFormatsResolver,
        }
      },
      {
        path: 'edition/edit/:id',
        component: EditionEditComponent,
        canActivate: [HasEditionGuard],
        resolve: { 
          edition: GetOneEditionResolver,
          books: GetBooksResolver,
          authors: GetAuthorsResolver,
          editors: GetEditorsResolver,
          collections: GetCollectionsResolver,
          formats: GetFormatsResolver,
        }
      },
      {
        path: '**',
        redirectTo: 'user/list',
        pathMatch: 'full'
      }
    ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Book, BookForm, putBook } from 'src/app/core/models/book';
import { BookService } from 'src/app/core/services/book.service';
import { ActivatedRoute } from '@angular/router';
import { Author } from 'src/app/core/models/author';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { AuthorAddComponent } from '../../author/author-add/author-add.component';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.scss']
})
export class BookEditComponent extends AbstractForm implements OnInit, AfterViewInit {

  error: {[key: string]: boolean} = {book: false};
  success: {[key: string]: boolean} = {book: false};
  show: {[key: string]: boolean} = {author: false};

  book: Book;
  authors: Array<Author>;

  filteredOptions$: Observable<Author[]>;

  @Output('add') onAddBook: EventEmitter<Book>;
  @ViewChild(AuthorAddComponent) authorComponent: AuthorAddComponent;
  @ViewChild('authorIdField') authorIdField: ElementRef;

  constructor(
    private bookService: BookService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef
  ) { super(); }

  ngOnInit(): void {
    try {
      this.authors = [];

      this.forms.book = this.formBuilder.group(putBook, {validator:this.validAutocomplete.bind(this)});
      this.forms.book.reset();
      
      this.activatedRoute.data.subscribe((data: {book: Book, authors: Array<Author> }) => {
        this.book = data.book;
        this.authors = data.authors
        this.filteredOptions$ = of(this.authors);
        this.forms.book.patchValue(
        {
          title: this.book.title,
          authorId: this.formatAuthor(this.book.author)
        });

        this.filteredOptions$ = this.forms.book.get('authorId').valueChanges
        .pipe(
          startWith(''),
          map(str => this.filterAuthor(str))
        );
      });
    } catch(e) {
      console.log(e);
    }
  }

  ngAfterViewInit(): void {
    this.authorIdField.nativeElement.blur();
    this.cdr.detectChanges();
  }

  private validAutocomplete(group: FormGroup) {
    if(!group.get('authorId').value) return null;
    if(!this.authors.find((o: Author) => { 
      const opt = this.formatAuthor(o).toLowerCase();
      return group.get('authorId').value.toString().toLowerCase().trim() === opt;
    })) group.get('authorId').setErrors({validAutocomplete: true});
  }

  private filterAuthor(value: string): Array<Author> {
    return this.authors.filter((o: Author) => { 
      const opt = this.formatAuthor(o).toLowerCase();
      return value && opt.includes(value.toString().toLowerCase());
    });
  }

  showAddAuthor(value: boolean) {
    this.show.author = value;
  }

  formatAuthor(author: Author): string {
    return `${author.first_name} ${author.last_name} (id#${author.id})`.replace(/\s{2,}/gms, ' ');
  }
  
  sendAuthor(author: Author) {
    try {
      this.authors.push(author);
      this.authorComponent.success.author = false;
      this.forms.book.controls.authorId.patchValue(this.formatAuthor(author));
      this.show.author = false;
    } catch(e) {
      console.log(e);
    }
  }
  
  send() {
    try {
      this.error.book = false;
      this.success.book = false;
      this.submitted = true;

      if(this.forms.book.valid) { 
        let book = <BookForm>this.forms.book.value;
        let matches = this.forms.book.get('authorId').value.match(/\(id\#([0-9]+)\)$/);
        if(!matches[1]) this.error.book = true;
        book = {...book, authorId: parseInt(matches[1])};
        this.bookService.editBook(this.book.id, book).subscribe(data => {
          this.success.book = true;
          this.submitted = false;
          this.book = data;
        }, error => this.error.book = true);
      }
    } catch(e) {
      console.log(e);
    }
  }
}

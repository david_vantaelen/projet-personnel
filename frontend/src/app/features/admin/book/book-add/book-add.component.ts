import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BookService } from 'src/app/core/services/book.service';
import { postBook, BookForm, Book } from 'src/app/core/models/book';
import { Author } from 'src/app/core/models/author';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { AuthorAddComponent } from '../../author/author-add/author-add.component';
import { AbstractForm } from 'src/app/core/abstracts/abstract.form';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.scss']
})
export class BookAddComponent extends AbstractForm implements OnInit {

  error: {[key: string]: boolean} = {book: false};
  success: {[key: string]: boolean} = {book: false};
  show: {[key: string]: boolean} = {author: false};

  authors: Array<Author>;

  filteredOptions$: Observable<Author[]>;

  @Output('add') onAddBook: EventEmitter<Book>;
  @ViewChild(AuthorAddComponent) authorComponent: AuthorAddComponent;

  constructor(
    private bookService: BookService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    super();
    this.onAddBook = new EventEmitter<Book>();
   }

  ngOnInit(): void {
    try {
      this.authors = [];
      this.activatedRoute.data.subscribe((data: { authors: Array<Author> }) => {
        this.authors = data.authors
        this.filteredOptions$ = of(this.authors);
      });

      this.forms.book = this.formBuilder.group(postBook, {validator:this.validAutocomplete.bind(this)});
      this.forms.book.reset();
      
      this.filteredOptions$ = this.forms.book.get('authorId').valueChanges
        .pipe(
          startWith(''),
          map(str => this.filterAuthor(str))
        );
    } catch(e) {
      console.log(e);
    }
  }

  private validAutocomplete(group: FormGroup) {
    if(!group.get('authorId').value) return null;
    if(!this.authors.find((o: Author) => { 
      const opt = this.formatAuthor(o).toLowerCase();
      return group.get('authorId').value.toString().toLowerCase().trim() === opt;
    })) group.get('authorId').setErrors({validAutocomplete: true});
  }

  private filterAuthor(value: string): Array<Author> {
    return this.authors.filter((o: Author) => { 
      const opt = this.formatAuthor(o).toLowerCase();
      return value && opt.includes(value.toString().toLowerCase());
    });
  }

  showAddAuthor(value: boolean) {
    this.show.author = value;
  }

  formatAuthor(author: Author): string {
    return `${author.first_name} ${author.last_name} (id#${author.id})`.replace(/\s{2,}/gms, ' ');
  }

  sendAuthor(author: Author) {
    try {
      this.authors.push(author);
      this.authorComponent.success.author = false;
      this.forms.book.controls.authorId.patchValue(this.formatAuthor(author));
      this.show.author = false;
    } catch(e) {
      console.log(e);
    }
  }

  send() {
    try {
      this.error.book = false;
      this.success.book = false;
      this.submitted = true;

      if(this.forms.book.valid) { 
        let book = <BookForm>this.forms.book.value;
        let matches = this.forms.book.get('authorId').value.match(/\(id\#([0-9]+)\)$/);
        if(!matches[1]) this.error.book = true;
        book = {...book, authorId: parseInt(matches[1])};
        this.bookService.createBook(book).subscribe(data => {
          this.success.book = true;
          this.forms.book.reset();
          this.forms.book.get('authorId').patchValue('');
          this.forms.book.get('authorId').markAsUntouched();
          this.submitted = false;
          this.onAddBook.emit(data as Book);
        }, error => this.error.book = true);
      }
    } catch(e) {
      console.log(e);
    }
  }
}

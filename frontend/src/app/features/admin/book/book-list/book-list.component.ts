import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/core/models/book';
import { NbMenuItem, NbDialogService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from 'src/app/core/services/book.service';
import { DialogBoxComponent } from 'src/app/shared/components/dialog-box/dialog-box.component';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {

  books: Array<Book>;
  columns: Array<string>;
  menu: Array<NbMenuItem>;
  
  constructor(
    private bookService: BookService,
    private nbDialogoService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {} 

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((data: {books: Array<Book>}) => this.books = data.books);

    this.menu = [{title: 'Add', link: '/admin/book/add'}]

    this.columns = [
      "id",
      "title",
      "author_name",
      "created_at",
      "updated_at"
    ];
  }

  delete(book: Book) {
    try {
      this.nbDialogoService.open(DialogBoxComponent, {
        closeOnBackdropClick: false,
        context: {
          title: `Delete Book`,
          message: `Are you sure you want to delete ${book.title} ?`,
          action: () => this.bookService.deleteBook(book.id).subscribe(() => { 
            this.router.navigate(['/', 'admin', 'book', 'list']);
            this.books = this.books.filter(b => b.id !== book.id)
          } )  
      }});
    } catch(e) {
      console.log(e);
    }
  }
}

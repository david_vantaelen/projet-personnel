import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { EditionListComponent } from './edition/edition-list/edition-list.component';
import { EditionAddComponent } from './edition/edition-add/edition-add.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CoreModule } from 'src/app/core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserAddComponent } from './user/user-add/user-add.component';
import { BookAddComponent } from './book/book-add/book-add.component';
import { BookListComponent } from './book/book-list/book-list.component';
import { AuthorAddComponent } from './author/author-add/author-add.component';
import { AuthorListComponent } from './author/author-list/author-list.component';
import { AuthorEditComponent } from './author/author-edit/author-edit.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { BookEditComponent } from './book/book-edit/book-edit.component';
import { CollectionAddComponent } from './collection/collection-add/collection-add.component';
import { CollectionListComponent } from './collection/collection-list/collection-list.component';
import { CollectionEditComponent } from './collection/collection-edit/collection-edit.component';
import { FormatAddComponent } from './format/format-add/format-add.component';
import { FormatListComponent } from './format/format-list/format-list.component';
import { FormatEditComponent } from './format/format-edit/format-edit.component';
import { EditorAddComponent } from './editor/editor-add/editor-add.component';
import { EditorListComponent } from './editor/editor-list/editor-list.component';
import { EditorEditComponent } from './editor/editor-edit/editor-edit.component';
import { EditionEditComponent } from './edition/edition-edit/edition-edit.component';


@NgModule({
  declarations: [
    AdminComponent,
    EditionListComponent,
    EditionAddComponent,
    UserListComponent,
    UserAddComponent,
    BookAddComponent,
    BookListComponent,
    AuthorAddComponent,
    AuthorListComponent,
    AuthorEditComponent,
    UserEditComponent,
    BookEditComponent,
    CollectionAddComponent,
    CollectionListComponent,
    CollectionEditComponent,
    FormatAddComponent,
    FormatListComponent,
    FormatEditComponent,
    EditorAddComponent,
    EditorListComponent,
    EditorEditComponent,
    EditionEditComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }

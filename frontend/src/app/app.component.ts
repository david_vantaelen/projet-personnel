import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TokenService } from './core/services/token.service';
import { AuthService } from './core/services/auth.service';
import { NbDialogService, NbSearchService, NbMenuItem, NbDialogRef } from '@nebular/theme';
import { ShoppingCartComponent } from './features/shopping-cart/shopping-cart.component';
import { CatalogService } from './core/services/catalog.service';
import { ShopItem } from './core/models/shop-item';
import { Location } from '@angular/common';
import { LoaderService } from './core/services/loader.service';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { filter, concatMap, tap, mergeMap, switchMap, exhaustMap, takeWhile, debounceTime, buffer } from 'rxjs/operators';
import { Observable, of, fromEvent } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  title = 'booktik-projet';

  shopCart: Array<ShopItem>;
  nItems:number = 0;

  adminMenu: Array<NbMenuItem>;
  spinner: NbDialogRef<LoaderComponent>;

  
  constructor(
    private loaderService: LoaderService,
    private tokenService: TokenService,
    private authService: AuthService,
    private nbSearchService: NbSearchService,
    private catalogService: CatalogService,
    private nbDialogService: NbDialogService,
    private router: Router,
    private location: Location
  ){}

  // testOBS(){
  //   const clicks$ = fromEvent(document.body, 'click');
   
  //   clicks$.pipe(
  //    buffer(clicks$.pipe(debounceTime(200))),
  //    concatMap(ev => of(ev)),
  //    //takeWhile(e => (<any>e).x < 100)
  //  ).subscribe(d => console.log(d));
  // }


  ngOnInit(): void {

    // this.testOBS();


    try {

      this.router.events.pipe(filter(o => o instanceof NavigationEnd)).subscribe(event => window.scrollTo(0, 0))

      this.loaderService.isLoading().subscribe(v => {
        if(v) this.spinner = this.nbDialogService.open(LoaderComponent, {closeOnBackdropClick: false});
        else if(this.spinner) this.spinner.close();
      });

      this.adminMenu = [
        { title: 'Users', link: '/admin/user/list' },
        { title: 'Authors', link: '/admin/author/list' },
        { title: 'Books', link: '/admin/book/list' },
        { title: 'Editors', link: '/admin/editor/list' },
        { title: 'Formats', link: '/admin/format/list' },
        { title: 'Collections', link: '/admin/collection/list' },
        { title: 'Editions', link: '/admin/edition/list' },
      ];
      
      this.catalogService.shopcart.subscribe(shopCart => { 
          this.shopCart = shopCart;
          this.nItems = this.shopCart.reduce((acc, curr) => acc + curr.quantity, 0);
        }
      );
        
      this.nbSearchService.onSearchSubmit()
      .subscribe(data => {
        this.router.navigate(['/', 'catalog', 'search', data.term])
      });
      
    } catch(e) {
      console.log(e);
    }
  }

  /**
   * @returns {boolean} true if user is logged else false 
   */
  isLogged(): boolean {
    return this.authService.isLogged();
  }

  /**
   * @returns {boolean} true if user is an admin else false 
   */
  isAdmin(): boolean {
    return this.authService.isAdmin();
  }

  showShoppingCart() {
    const ref = this.nbDialogService.open(ShoppingCartComponent, {closeOnBackdropClick: true})
  }

  goToDashboard() {
    this.router.navigateByUrl('/admin');
  }

  /**
   * @returns {boolean} true if route begins with /admin else false 
   */
  isInDashboard(): boolean {
    return this.location.path().startsWith('/admin');
  }

  login() {
    this.router.navigateByUrl('/auth/login');
  }

  logout() {
    this.tokenService.token = null;
    this.login();
  }

  /**
   * @returns {?string} username if user is logged else false
   */
  getAuthUsername(): string|null {
      return this.isLogged() ? this.tokenService.decode().username : null;
  }
}

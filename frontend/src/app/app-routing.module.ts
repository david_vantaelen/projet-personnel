import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsNotLoggedGuard } from './core/guards/is-not-logged.guard';
import { IsLoggedGuard } from './core/guards/is-logged.guard';
import { IsAdminGuard } from './core/guards/is-admin.guard';


const routes: Routes = [
  { path: '', redirectTo: 'catalog', pathMatch: 'full' },
  { path: 'auth', canActivate: [IsNotLoggedGuard], loadChildren: () => import('./features/auth/auth.module').then(m => m.AuthModule) },
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
  { path: 'admin', canActivate: [IsAdminGuard] , loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) },
  { path: 'register', canActivate: [IsNotLoggedGuard], loadChildren: () => import('./features/register/register.module').then(m => m.RegisterModule) },
  { path: 'command', loadChildren: () => import('./features/command/command.module').then(m => m.CommandModule) },
  { path: '**', redirectTo: 'catalog', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollOffset: [0, 0], scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

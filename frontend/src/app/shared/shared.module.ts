import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbAlertModule, NbInputModule, NbSelectModule, NbButtonModule, NbActionsModule, NbSearchModule, NbRouteTabsetModule, NbListModule, NbIconModule, NbContextMenuModule, NbMenuModule, NbAutocompleteModule, NbDatepickerModule, NbStepperModule, NbRadioModule } from '@nebular/theme';
import { LoaderComponent } from './components/loader/loader.component';
import { EuroPipe } from './pipes/euro.pipe';
import { CleanPipe } from './pipes/clean.pipe';
import { ListPipe } from './pipes/list.pipe';
import { NbMomentDateModule } from '@nebular/moment';
import { NbDateFnsDateModule } from '@nebular/date-fns';
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { SubMenuComponent } from './components/sub-menu/sub-menu.component';


@NgModule({
  declarations: [
    LoaderComponent,
    EuroPipe,
    CleanPipe,
    ListPipe,
    DialogBoxComponent,
    PaginatorComponent,
    SubMenuComponent
  ],
  entryComponents: [LoaderComponent, DialogBoxComponent],
  imports: [
    CommonModule,
    NbIconModule,
    NbCardModule,
    NbAlertModule,
    NbActionsModule,
    NbInputModule,
    NbSelectModule,
    NbSearchModule,
    NbButtonModule,
    NbRadioModule,
    NbRouteTabsetModule,
    NbListModule,
    NbContextMenuModule,
    NbMenuModule,
    NbAutocompleteModule,
    NbDatepickerModule,
    NbMomentDateModule,
    NbDateFnsDateModule,
    NbStepperModule,
  ],
  exports: [
    EuroPipe,
    CleanPipe,
    ListPipe,
    PaginatorComponent,
    NbIconModule,
    NbCardModule,
    NbAlertModule,
    NbActionsModule,
    NbInputModule,
    NbSelectModule,
    NbSearchModule,
    NbButtonModule,
    NbRadioModule,
    NbRouteTabsetModule,
    NbListModule,
    NbContextMenuModule,
    NbMenuModule,
    NbAutocompleteModule,
    NbDatepickerModule,
    NbMomentDateModule,
    NbDateFnsDateModule,
    NbStepperModule,
    SubMenuComponent
  ]
})
export class SharedModule { }

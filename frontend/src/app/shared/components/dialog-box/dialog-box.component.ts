import { Component, OnInit, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss']
})
export class DialogBoxComponent implements OnInit {

  @Input() title: string;
  @Input() message: string;
  @Input() action: Function;

  constructor(protected dialogRef: NbDialogRef<any>) { }

  ngOnInit(): void {
  
  }

  confirm() {
    this.action();
    this.dialogRef.close();
  }

  close() {
    this.dialogRef.close();
  }

}

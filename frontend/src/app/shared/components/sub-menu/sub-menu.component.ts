import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.scss']
})
export class SubMenuComponent implements OnInit {

  tabs: Array<{ title: string, route: string, icon: string, responsive: boolean }>;

  constructor() { }

  ngOnInit(): void {
    this.tabs = [
      {
        title: 'Catalog',
        route: '/catalog',
        icon: 'home',
        responsive: true
      },
      {
        title: 'Shopping Cart',
        route: '/command',
        icon: 'shopping-cart',
        responsive: true
      }
    ];
  }
}

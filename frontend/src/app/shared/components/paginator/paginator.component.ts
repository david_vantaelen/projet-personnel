import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Page } from 'src/app/core/models/page';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  @Input('page') page: Page<any>;
  @Output('no') pageEmitter: EventEmitter<number>;

  constructor() {
    this.pageEmitter = new EventEmitter<number>();
   }

  ngOnInit(): void {}

  previous() {
    this.pageEmitter.emit(this.page.page > 1 ? this.page.page - 1 : 1);
  }
  next() {
    this.pageEmitter.emit(this.page.page + 1 < this.page.nb_pages ? this.page.page + 1 : this.page.nb_pages);
  }
  first() {
    this.pageEmitter.emit(1);
  }
  last() {
    this.pageEmitter.emit(this.page.nb_pages);
  }
}

export const ERROR_MESSAGES: {[key: string]: Function} = {
    required: () => `Field is required`,
    minlength: () => `Too few charachters`,
    maxlength: () => `Too many charachters`,
    matchPassword: () => `Password doesn't match`,
    usernameExists: () => `Username exists`,
    emailExists: () => `Email exists`,
    formatExists: () => `Format exists`,
  }
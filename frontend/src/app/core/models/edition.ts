import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { Book } from './book';
import { Format } from './format';
import { Editor } from './editor';
import { Collection } from './collection';

export interface Edition {
    id: number;
    title: string;
    author: string;
    description?: string;
    price: number;
    image?: any;
    image_path: string;
    stock: number;
    release_at: Date;
    book?: Book;
    format?: Format;
    editor?: Editor;
    collection?: Collection;
    created_at: Date;
    updated_at?: Date;
}

export interface EditionForm {
    description?: string;
    price: number;
    image?: any;
    stock?: number;
    releaseAt?: Date;
    bookId: number;
    formatId: number;
    editorId: number;
    collectionId?: number;
}

export const postEdition: {[name: string]: AbstractControl} = {
    bookId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(1)
    ])),
    editorId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(1)
    ])),
    formatId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(1)
    ])),
    collectionId: new FormControl(null, Validators.compose([
        Validators.min(1)
    ])),
    price: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.pattern(/^\d*(?:(?:\.|,)\d+)?\s*\€?$/),
        Validators.min(0)
    ])),
    stock: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(0)
    ])),
    description: new FormControl(null),
    image: new FormControl(null),
    releaseAt: new FormControl(null, Validators.compose([
        Validators.required
    ])),
}

export const putEdition: {[name: string]: AbstractControl} = {...postEdition}
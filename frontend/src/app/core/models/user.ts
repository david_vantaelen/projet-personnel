import { Group } from './group';
import { UserRole } from './user-role';
import { AbstractControl, FormControl, Validators, FormGroup } from '@angular/forms';
import { Address, AddressForm } from './address';

export interface User{
    id: number;
    first_name: string;
    last_name: string;
    username: string;
    email: string;
    password: string;
    roles: string[];
    main_address?: Address;
    addresses?: Address[];
    group?: Group;
    group_name?: string;
    user_roles?: Array<UserRole>;
    created_at: Date;
    updated_at?: Date;
}

export interface UserForm{
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    password: string;
    groupId: number;
    address: AddressForm;
}

export const registerUser: {[name: string]: AbstractControl} = {
    firstName: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(63)
    ])),
    lastName: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(63)
    ])),
    username: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(63)
    ])),
    email: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(127),
        Validators.email
    ])),
    password: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(63)
    ])),
    confirmPassword: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(7),
        Validators.maxLength(63)
    ])),
    address: new FormGroup({
        street: new FormControl(null, Validators.compose([
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(127),
        ])),
        city: new FormControl(null, Validators.compose([
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(127),
        ])),
        postCode: new FormControl(null, Validators.compose([
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(15),
        ])),
        country: new FormControl(null, Validators.compose([
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(127),
        ]))
    })
}

export const postUser: {[name: string]: AbstractControl} = {...registerUser, 
    groupId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(0)
    ]))
};

export const putUser: {[name: string]: AbstractControl} = {
    firstName: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(63)
    ])),
    lastName: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(63)
    ])),
    email: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(127),
        Validators.email
    ])),
    address: new FormGroup({
        street: new FormControl(null, Validators.compose([
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(127),
        ])),
        city: new FormControl(null, Validators.compose([
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(127),
        ])),
        postCode: new FormControl(null, Validators.compose([
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(15),
        ])),
        country: new FormControl(null, Validators.compose([
            Validators.required,
            Validators.minLength(1),
            Validators.maxLength(127),
        ]))
    }),
    groupId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(0)
    ])),
    password: new FormControl(null, Validators.compose([
        Validators.minLength(7),
        Validators.maxLength(63)
    ])),
    confirmPassword: new FormControl(null, Validators.compose([
        Validators.minLength(7),
        Validators.maxLength(63)
    ]))
};
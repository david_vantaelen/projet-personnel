import { TranscodeEncoding } from 'buffer';

export interface Page<T> {
    page: number,
    per_page: number,
    nb_pages: number,
    count: number,
    total: number,
    data: Array<T>
}

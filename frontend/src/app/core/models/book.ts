import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { Author } from './author';

export interface Book {
    id: number;
    title: string;
    author_name: string;
    author?: Author;
    created_at: Date;
    updated_at?: Date;
}

export interface BookForm {
    title: string;
    authorId: number;
}

export const postBook: {[name: string]: AbstractControl} = {
    title: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(180)
    ])),
    authorId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(1),
    ]))
}

export const putBook: {[name: string]: AbstractControl} = {
    title: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(180)
    ])),
    authorId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(1),
    ]))
}
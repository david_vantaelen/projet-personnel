import { AbstractControl, FormControl, Validators } from '@angular/forms';

export interface Editor {
    id: number;
    editor: string;
    created_at: Date;
    updated_at?: Date;
}

export interface EditorForm {
    editor: string;
}

export const postEditor: {[name: string]: AbstractControl} = {
    editor: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(180)
    ]))
}

export const putEditor: {[name: string]: AbstractControl} = {...postEditor}
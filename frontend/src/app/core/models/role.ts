export interface Role {
    id: number;
    label: string;
    created_at: Date;
    updated_at?: Date;
}

export interface RoleForm {
    label: string;
}
export interface ShopItem {
    id: number;
    title: string;
    author: string;
    price: number;
    quantity: number;
    stock: number;
    image_path: string;
}
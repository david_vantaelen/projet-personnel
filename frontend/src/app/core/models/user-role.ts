import { User } from './user';
import { Role } from './role';

export interface UserRole {
    user: User;
    role: Role;
    begin_at: Date;
    end_at?: Date;
}

export interface UserRoleForm {
    beginAt: Date;
    endAt?: Date;
}
export interface Address {
    street: string;
    city: string;
    post_code: string;
    country: string;
    is_main: boolean
}

export interface AddressForm {
    street: string;
    city: string;
    postCode: string;
    country: string;
    isMain: boolean
}
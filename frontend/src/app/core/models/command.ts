import { AbstractControl, FormControl, Validators } from '@angular/forms'
import { Financial } from './financial'
import { User } from './user'
import { Courier } from './courier'

export interface Command {
    id: number;
    user?: User;
    courier?: Courier;
    financial?: Financial;
    status: string;
    created_at: Date;
    updated_at?: Date;
}

export interface CommandForm {
    userId: number;
    courierId: number;
    financialId: any;
    status: string;
}

export const postCommand: {[name: string]: AbstractControl} = {
    userId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(1)
    ])),
    courierId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(1)
    ])),
    financialId: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.min(1)
    ])),
    status: new FormControl(null, Validators.compose([
        Validators.required
    ]))
}

export const putCommand: {[name: string]: AbstractControl} = {...postCommand}

export enum STATUS {
    PENDING = 'pending',
    DELIVERY = 'deilvery',
    CANCELED = 'canceled',
}
export interface Group {
    id: number;
    label: string;
    created_at: Date;
    updated_at?: Date;
}

export interface GroupForm {
    label: string;
}
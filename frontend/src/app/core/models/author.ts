import { AbstractControl, FormControl, Validators } from '@angular/forms';

export interface Author {
    id: number;
    first_name: string;
    last_name: string;
    created_at: Date;
    updated_at?: Date;
}

export interface AuthorForm {
    firstName: string;
    lastName: string;
}

export const postAuthor: {[name: string]: AbstractControl} = {
    firstName: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(63)
    ])),
    lastName: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(63)
    ]))
}

export const putAthor: {[name: string]: AbstractControl} = {...postAuthor}
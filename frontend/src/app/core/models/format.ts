import { AbstractControl, FormControl, Validators } from '@angular/forms';

export interface Format {
    id: number;
    format: string;
    created_at: Date;
    updated_at?: Date;
}

export interface FormatForm {
    format: string;
}

export const postFormat: {[name: string]: AbstractControl} = {
    format: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(123)
    ])),
}

export const putFormat: {[name: string]: AbstractControl} = {...postFormat}
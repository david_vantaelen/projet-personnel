export interface Financial {
    id: number;
    name: string;
    description?: string;
    image?: string;
    created_at: Date;
    updated_at?: Date;
}
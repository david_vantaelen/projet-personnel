export interface Courier {
    id: number;
    name: string;
    description?: string;
    image?: string;
    created_at: Date;
    updated_at?: Date;
}
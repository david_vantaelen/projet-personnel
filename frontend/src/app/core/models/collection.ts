import { AbstractControl, FormControl, Validators } from '@angular/forms';

export interface Collection {
    id: number;
    collection: string;
    created_at: Date;
    updated_at?: Date;
}

export interface CollectionForm {
    collection: string;
}

export const postCollection: {[name: string]: AbstractControl} = {
    collection: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(123)
    ])),
}

export const putCollection: {[name: string]: AbstractControl} = {...postCollection}
import { AbstractControl, FormControl, Validators } from '@angular/forms'
import { Command } from './command'
import { Edition } from './edition'

export interface CommandEdition {
    command: Command;
    edition: Edition;
    price: number;
    quantity: number;
}

export interface CommandEditionForm {
    price: number;
    quantity: number;
}

export const postCommandEdition: {[name: string]: AbstractControl} = {
    price: new FormControl(null, Validators.compose([
        Validators.required
    ])),
    quantity: new FormControl(null, Validators.compose([
        Validators.required
    ]))
}

export const putCommandEdition: {[name: string]: AbstractControl} = {...postCommandEdition}
import { FormGroup, AbstractControl } from '@angular/forms';
import { ERROR_MESSAGES } from '../models/error-message';

/**
 * @typedef {Object.<string, boolean>} Dict
 */
/**
 * @typedef {Object.<string, Function>} DictFn
 */

export abstract class AbstractForm {
    /** @member {boolean} submitted */
    submitted: boolean;
    /** @member {Dict} error */
    error: {[key: string]: boolean} = {};
    /** @member {Dict} success */
    success: {[key: string]: boolean} = {};
    /** @member {Dict} forms */
    forms: {[key: string]: FormGroup} = {};
    /** @member {DictFn} error_messages */
    error_messages = ERROR_MESSAGES;

    /**
     * @param {string} key
     * @param {(string|AbstractControl)} form
     * @param {boolean} [acceptsEmpty = false]
     * @throws {Error} if form doesn't exists
     * @throws {Error} if key in form doesn't exists
     * @returns {boolean} true if form field is invalid else false
     */
    isInvalid(key: string, form:string|AbstractControl, acceptsEmpty: boolean = false): boolean {
        const o = form instanceof AbstractControl ? form : this.forms[form];
        if(!o) throw new Error(`form should exists`);
        if(!o.get(key)) throw new Error(`key in form should exists`);
        if((acceptsEmpty && !this.submitted) && o.get(key).value === "") return false; 
        return (o.get(key).dirty || this.submitted) 
        && !o.get(key).valid 
        && (o.get(key).touched || this.submitted);
    }
    
    /**
     * @param {string} key
     * @param {(string|AbstractControl)} form
     * @throws {Error} if form doesn't exists
     * @throws {Error} if key in form doesn't exists
     * @returns {?string} message if form field is invalid else false
     */
    displayErrorMessage(key: string, form:string|AbstractControl): string|null {
        const o = form instanceof AbstractControl ? form : this.forms[form];
        if(!o) throw new Error(`form should exists`);
        if(!o.get(key)) throw new Error(`key in form should exists`);
        const error_types = Object.keys(o.get(key).errors);
        if(error_types && error_types.length && o.get(key).getError(error_types[0])){
            return this.error_messages[error_types[0]] && this.error_messages[error_types[0]]();
        };
    }
}
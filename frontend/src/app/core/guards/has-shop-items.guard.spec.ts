import { TestBed } from '@angular/core/testing';

import { HasShopItemsGuard } from './has-shop-items.guard';

describe('HasShopItemsGuard', () => {
  let guard: HasShopItemsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasShopItemsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { IsNotLoggedGuard } from './is-not-logged.guard';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('IsNotLoggedGuard', () => {
  let guard: IsNotLoggedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
    });
    guard = TestBed.inject(IsNotLoggedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

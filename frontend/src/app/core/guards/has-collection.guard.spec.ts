import { TestBed } from '@angular/core/testing';

import { HasCollectionGuard } from './has-collection.guard';

describe('HasCollectionGuard', () => {
  let guard: HasCollectionGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasCollectionGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

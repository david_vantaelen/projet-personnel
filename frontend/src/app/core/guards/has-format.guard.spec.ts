import { TestBed } from '@angular/core/testing';

import { HasFormatGuard } from './has-format.guard';

describe('HasFormatGuard', () => {
  let guard: HasFormatGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasFormatGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { HasCourierGuard } from './has-courier.guard';

describe('HasCourierGuard', () => {
  let guard: HasCourierGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasCourierGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

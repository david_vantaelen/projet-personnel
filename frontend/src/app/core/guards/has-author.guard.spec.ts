import { TestBed } from '@angular/core/testing';

import { HasAuthorGuard } from './has-author.guard';

describe('HasAuthorGuard', () => {
  let guard: HasAuthorGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasAuthorGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

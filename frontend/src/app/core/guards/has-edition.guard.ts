import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CatalogService } from '../services/catalog.service';
import { map, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HasEditionGuard implements CanActivate {
  public constructor(
    private router: Router,
    private catalogService: CatalogService
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const param: any = next.paramMap.get('id');
    let redirect = false;
    
    return this.catalogService.getOneFromCatalog(<number>param).pipe(
      map(edition => { 
        if(!edition) {
          redirect = true;
          return false;
        }  
        return true;
      }),
      finalize(() => {
        if(redirect) this.router.navigateByUrl('/catalog');
      })
    )
  }
}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { map, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HasUserGuard implements CanActivate {
  public constructor(
    private router: Router,
    private userService: UserService
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      
      const param: any = next.paramMap.get('id');
      let redirect = false;
    
      return this.userService.getUser(<number>param).pipe(
        map(user => { 
          if(!user) {
            redirect = true;
            return false;
          }  
          return true;
        }),
        finalize(() => {
          if(redirect) this.router.navigateByUrl('/admin/user');
        })
      )

  }
  
}

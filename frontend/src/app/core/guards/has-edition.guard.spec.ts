import { TestBed } from '@angular/core/testing';

import { HasEditionGuard } from './has-edition.guard';

describe('HasEditionGuard', () => {
  let guard: HasEditionGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasEditionGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { HasFinancialGuard } from './has-financial.guard';

describe('HasFinancialGuard', () => {
  let guard: HasFinancialGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasFinancialGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

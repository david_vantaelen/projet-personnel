import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorService } from '../services/author.service';
import { map, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HasAuthorGuard implements CanActivate {
  public constructor(
    private router: Router,
    private authorService: AuthorService
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const param: any = next.paramMap.get('id');
      let redirect = false;
    
      return this.authorService.getAuthor(<number>param).pipe(
        map(author => { 
          if(!author) {
            redirect = true;
            return false;
          }  
          return true;
        }),
        finalize(() => {
          if(redirect) this.router.navigateByUrl('/admin/author');
        })
      )
  }
  
}

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { EditorService } from '../services/editor.service';
import { map, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HasEditorGuard implements CanActivate {
  public constructor(
    private router: Router,
    private editorService: EditorService
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const param: any = next.paramMap.get('id');
      let redirect = false;
    
      return this.editorService.getEditor(<number>param).pipe(
        map(editor => { 
          if(!editor) {
            redirect = true;
            return false;
          }  
          return true;
        }),
        finalize(() => {
          if(redirect) this.router.navigateByUrl('/admin/editor');
        })
      )
  }
}

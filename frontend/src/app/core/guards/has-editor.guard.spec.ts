import { TestBed } from '@angular/core/testing';

import { HasEditorGuard } from './has-editor.guard';

describe('HasEditorGuard', () => {
  let guard: HasEditorGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasEditorGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

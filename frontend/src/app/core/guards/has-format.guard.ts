import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormatService } from '../services/format.service';
import { finalize, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HasFormatGuard implements CanActivate {
  public constructor(
    private router: Router,
    private formatService: FormatService
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const param: any = next.paramMap.get('id');
      let redirect = false;
    
      return this.formatService.getFormat(<number>param).pipe(
        map(format => { 
          if(!format) {
            redirect = true;
            return false;
          }  
          return true;
        }),
        finalize(() => {
          if(redirect) this.router.navigateByUrl('/admin/format');
        })
      )
  }
}

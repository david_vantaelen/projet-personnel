import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HasCourierGuard implements CanActivate {
  public constructor(
    private router: Router,
    private courierService: CourierService
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const param: any = next.paramMap.get('id');
      let redirect = false;
    
      return this.courierService.getCourier(<number>param).pipe(
        map(courier => { 
          if(!courier) {
            redirect = true;
            return false;
          }  
          return true;
        }),
        finalize(() => {
          if(redirect) this.router.navigateByUrl('/admin');
        })
      )
  }
  
}

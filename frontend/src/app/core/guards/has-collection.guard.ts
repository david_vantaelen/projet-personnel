import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CollectionService } from '../services/collection.service';
import { finalize, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HasCollectionGuard implements CanActivate {
  public constructor(
    private router: Router,
    private collectionService: CollectionService
  ){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const param: any = next.paramMap.get('id');
      let redirect = false;
    
      return this.collectionService.getCollection(<number>param).pipe(
        map(collection => { 
          if(!collection) {
            redirect = true;
            return false;
          }  
          return true;
        }),
        finalize(() => {
          if(redirect) this.router.navigateByUrl('/admin/collection');
        })
      )
  }
}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Author, AuthorForm } from '../models/author';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(private httpClient: HttpClient) { }

  /**
   * @description get a list of authors from backend
   * @returns {Observable.<Array.<Author>>} ObservaHble of authors 
   */ 
  get authors(): Observable<Array<Author>> {
    return this.httpClient.get<Array<Author>>(`${this.endPoint}api/author`);
  }
  
  /**
   * @description get an author with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Author|null)>} Observable of an author if author found by id else Observable of null
   */ 
  getAuthor(id: number): Observable<(Author|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Author|null)>(`${this.endPoint}api/author/${parseInt(<any>id)}`);
  }

  /**
   * @description insert a new author
   * @param {AuthorForm} author
   * @returns {Observable.<(Author|*)>} Observable of the created author 
   */ 
  createAuthor(author: AuthorForm): Observable<(Author|any)> {
    return this.httpClient.post<(Author|any)>(`${this.endPoint}api/author`, author);
  }

  /**
   * @description edit an author found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @param {AuthorForm} author
   * @returns {Observable.<(Author|*)>} Observable of the edited author 
   */ 
  editAuthor(id: number, author: AuthorForm): Observable<(Author|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.put<(Author|any)>(`${this.endPoint}api/author/${parseInt(<any>id)}`, author);
  }

  /**
   * @description delete an author found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<*>} Observable of delete result
   */ 
  deleteAuthor(id: number): Observable<any> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.delete<any>(`${this.endPoint}api/author/${parseInt(<any>id)}`);
  }
}

import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  /**
   * @description set token value or null value
   * @param {string} value
   */
  set token(value: string) {
    if(value === null) localStorage.removeItem('TOKEN');
    else localStorage.setItem('TOKEN', value)
  }

  /**
   * @description get token value or null if token doesn't exist
   * @returns {?string} token if exists
   */
  get token(): string|null {
    return localStorage.getItem('TOKEN');
  }

  /**
   * @description get decoded token
   * @returns {*} decoded token
   */
  decode(): any {
    if(this.token) return jwt_decode(this.token);
  }
}
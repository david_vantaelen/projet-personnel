import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private loadNum: number = 0;
  private isLoading$: BehaviorSubject<boolean>;

  constructor() {
    this.isLoading$ = new BehaviorSubject<boolean>(false);
  }

  load() {
    this.loadNum++;
    this.evaluate();
  }

  unload() {
    this.loadNum = this.loadNum - 1 < 1 ? 0 :  this.loadNum - 1;
    this.evaluate();
  }

  evaluate() {
    if(this.loadNum > 0 && !this.isLoading$.value) this.isLoading$.next(true);
    else if(!this.loadNum && this.isLoading$.value) this.isLoading$.next(false);
  }

  get LoadNum (): number {
    return this.loadNum;
  }


  isLoading(): Observable<boolean> {
    return this.isLoading$.asObservable();
  }
}

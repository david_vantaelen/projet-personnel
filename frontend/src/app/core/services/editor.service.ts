import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Editor, EditorForm } from '../models/editor';

@Injectable({
  providedIn: 'root'
})
export class EditorService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(private httpClient: HttpClient) { }
  
  /**
   * @description get a list of editor from backend
   * @returns {Observable.<Array.<Editor>>} Observable of editors 
   */
  get editors(): Observable<Array<Editor>> {
    return this.httpClient.get<Array<Editor>>(`${this.endPoint}api/editor`);
  }

  /**
   * @description get an editor with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Editor|null)>} Observable of a editor if editor found by id else Observable of null
   */ 
  getEditor(id: number): Observable<(Editor|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Editor|null)>(`${this.endPoint}api/editor/${parseInt(<any>id)}`);
  }

  /**
   * @description insert a new editor
   * @param {EditorForm} editor
   * @returns {Observable.<(Editor|*)>} Observable of the created editor 
   */ 
  createEditor(editor: EditorForm): Observable<(Editor|any)> {
    return this.httpClient.post<(Editor|any)>(`${this.endPoint}api/editor`, editor);
  }

  /**
   * @description edit an editor found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @param {EditorForm} editor
   * @returns {Observable.<(Editor|*)>} Observable of the edited editor 
   */ 
  editEditor(id: number, editor: EditorForm): Observable<(Editor|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.put<(Editor|any)>(`${this.endPoint}api/editor/${parseInt(<any>id)}`, editor);
  }

  /**
   * @description delete an editor found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<*>} Observable of delete result
   */ 
  deleteEditor(id: number): Observable<any> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.delete<any>(`${this.endPoint}api/edition/${parseInt(<any>id)}`);
  }
}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Financial } from '../models/financial';

@Injectable({
  providedIn: 'root'
})
export class FinancialService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(private httpClient: HttpClient) { }

  /**
   * @description get a list of financials from backend
   * @returns {Observable.<Array.<Financial>>} Observable of financials 
   */
  get financials(): Observable<Array<Financial>> {
    return this.httpClient.get<Array<Financial>>(`${this.endPoint}api/financial`);
  }

  /**
   * @description get a financial with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Financial|null)>} Observable of a financial if financial found by id else Observable of null
   */ 
  getFinancial(id: number): Observable<(Financial|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Financial|null)>(`${this.endPoint}api/financial/${parseInt(<any>id)}`);
  }
}

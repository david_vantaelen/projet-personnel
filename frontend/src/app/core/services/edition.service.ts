import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpEvent, HttpEventType } from '@angular/common/http';
import { Edition, EditionForm } from '../models/edition';
import { Page } from '../models/page';
import { tap, filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EditionService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(
    private httpClient: HttpClient
  ) { }

  /**
   * @description get a list of editions from backend
   * @returns {Observable.<Array.<Edition>>} Observable of editions 
   */
  get editions (): Observable<Array<Edition>> {
    return this.httpClient.get<Array<Edition>>(`${this.endPoint}api/edition`);
  }

  /**
   * @description get a page of editions from backend
   * @param {number} [page=1] page > 0
   * @param {number} [perPage=20] perPage
   * @throws {Error} if page < 1
   * @returns {Observable.<Page.<Edition>} Observable of a page of editions 
   */ 
  getPage(page: number = 1, perPage: number = 20): Observable<Page<Edition>> {
    if(parseInt(<any>page) < 1) throw new Error(`page should be greater than 0`);
    return this.httpClient.get<Page<Edition>>(`${this.endPoint}api/edition/page?page=${parseInt(<any>page)}&perPage=${parseInt(<any>perPage)}`);
  }

  /**
   * @description get a list of editions by search value from backend
   * @param {string} search
   * @returns {Observable.<Array.<Edition>>} Observable of editions 
   */ 
  getEditionBySearch (search: string): Observable<Array<Edition>> {
    const value = encodeURI(search.replace(/(?!\s)[^\w]/g, ""));
    return this.httpClient.get<Array<Edition>>(`${this.endPoint}api/edition?search=${value}`);
  }

  /**
   * @description get a page of editions by search value from backend
   * @param {string} search
   * @param {number} [page=1] page > 0
   * @param {number} [perPage=20] perPage
   * @throws {Error} if page < 1
   * @returns {Observable.<Page.<Edition>>} Observable of a page of editions 
   */ 
  getPageBySearch(search: string, page: number = 1, perPage: number = 20): Observable<Page<Edition>> {
    const value = encodeURI(search.replace(/(?!\s)[^\w]/g, ""));
    if(parseInt(<any>page) < 1) throw new Error(`page should be greater than 0`);
    return this.httpClient.get<Page<Edition>>(`${this.endPoint}api/edition/page?search=${value}&page=${parseInt(<any>page)}&perPage=${parseInt(<any>perPage)}`);
  }

  /**
   * @description get an edition with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Edition|null)>} Observable of a edition if edition found by id else Observable of null
   */ 
  getEdition (id: number): Observable<(Edition|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Edition|null)>(`${this.endPoint}api/edition/${parseInt(<any>id)}`);
  }
  
  /**
   * @description insert a new edition
   * @param {FormData} formData
   * @returns {Observable.<(Edition|*)>} Observable of the created edition 
   */ 
  createEdition(formData: FormData): Observable<Edition> {
    return this.httpClient.post<HttpEvent<any>>(`${this.endPoint}api/edition`, formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(tap(event => {
      switch(event.type) {
        case HttpEventType.UploadProgress: 
          console.log(`Uploadeding: ${Math.round(event.loaded / event.total * 100)}%`)
          break;
        case HttpEventType.Response:
          console.log(`Uploaded Complete`);
          break;
      }
    }),
    filter(event => event.type === HttpEventType.Response),
    map(event => (<any>event).body as Edition)
    );
  }

  /**
   * @description edit an edition found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @param {EditionForm} edition
   * @returns {Observable.<(Edition|*)>} Observable of the edited edition 
   */ 
  editEdition(id: number, edition: EditionForm): Observable<(Edition|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.put<(Edition|any)>(`${this.endPoint}api/edition/${parseInt(<any>id)}`, edition);
  }

  /**
   * @description change the image of an edition found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @param {FormData} formData
   * @returns {Observable.<(Edition|*)>} Observable of the edited edition 
   */
  editImage(id: number, formData: FormData): Observable<(Edition|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.post<(Edition|any)>(`${this.endPoint}api/edition/${parseInt(<any>id)}/image`, formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(tap(event => {
      switch(event.type) {
        case HttpEventType.UploadProgress: 
          console.log(`Uploadeding: ${Math.round(event.loaded / event.total * 100)}%`)
          break;
        case HttpEventType.Response:
          console.log(`Uploaded Complete`);
          break;
      }
    }),
    filter(event => event.type === HttpEventType.Response),
    map(event => (<any>event).body as Edition)
    );;
  }

  /**
   * @description delete an edition found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<*>} Observable of delete result
   */ 
  deleteEdition(id: number): Observable<any> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.delete<any>(`${this.endPoint}api/edition/${parseInt(<any>id)}`);
  }
}

import { Injectable } from '@angular/core';
import { FormatForm, Format } from '../models/format';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(private httpClient: HttpClient) { }

  /**
   * @description get a list of formats from backend
   * @returns {Observable.<Array.<Format>>} Observable of formats 
   */
  get formats(): Observable<Array<Format>> {
    return this.httpClient.get<Array<Format>>(`${this.endPoint}api/format`);
  }

  /**
   * @description get a format with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Format|null)>} Observable of a format if format found by id else Observable of null
   */ 
  getFormat(id: number): Observable<(Format|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Format|null)>(`${this.endPoint}api/format/${parseInt(<any>id)}`);
  }

  /**
   * @description insert a new format
   * @param {FormatForm} format
   * @returns {Observable.<(Format|*)>} Observable of the created format 
   */ 
  createFormat(format: FormatForm): Observable<(Format|any)> {
    return this.httpClient.post<(Format|any)>(`${this.endPoint}api/format`, format);
  }

  /**
   * @description edit a format found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @param {FormatForm} format
   * @returns {Observable.<(Format|*)>} Observable of the edited format 
   */ 
  editFormat(id: number, format: FormatForm): Observable<(Format|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.put<(Format|any)>(`${this.endPoint}api/format/${parseInt(<any>id)}`, format);
  }

  /**
   * @description check if format exists in database
   * @param {string} format
   * @returns {Observable.<boolean>} Observable of true if format exists
   */
  hasFormat(format: string): Observable<boolean> {
    return this.httpClient.get<any[]>(`${this.endPoint}api/has_format/${format}`)
    .pipe(map((data: any) => data.format));

  }

  /**
   * @description delete a format found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<*>} Observable of delete result
   */ 
  deleteFormat(id: number): Observable<any> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.delete<any>(`${this.endPoint}api/format/${parseInt(<any>id)}`);
  }
}

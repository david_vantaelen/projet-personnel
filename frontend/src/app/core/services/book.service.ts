import { Injectable } from '@angular/core';
import { BookForm, Book } from '../models/book';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(private httpClient: HttpClient) { }

  /**
   * @description get a list of books from backend
   * @returns {Observable.<Array.<Book>>} Observable of books 
   */ 
  get books(): Observable<Array<Book>> {
    return this.httpClient.get<Array<Book>>(`${this.endPoint}api/book`);
  }

  /**
   * @description get a book with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Book|null)>} Observable of a book if book found by id else Observable of null
   */ 
  getBook(id: number): Observable<(Book|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Book|null)>(`${this.endPoint}api/book/${parseInt(<any>id)}`);
  }

  /**
   * @description insert a new book
   * @param {BookForm} book
   * @returns {Observable.<(Book|*)>} Observable of the created book 
   */ 
  createBook(book: BookForm): Observable<(Book|any)> {
    return this.httpClient.post<(Book|any)>(`${this.endPoint}api/book`, book);
  }
  
  /**
   * @description edit a book found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @param {BookForm} book
   * @returns {Observable.<(Book|*)>} Observable of the edited book 
   */ 
  editBook(id: number, book: BookForm): Observable<(Book|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.put<(Book|any)>(`${this.endPoint}api/book/${parseInt(<any>id)}`, book);
  }

  /**
   * @description delete a book found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<*>} Observable of delete result
   */ 
  deleteBook(id: number): Observable<any> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.delete<any>(`${this.endPoint}api/book/${parseInt(<any>id)}`);
  }
}

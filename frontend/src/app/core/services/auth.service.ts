import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Auth } from '../models/auth';
import { Token } from '../models/token';
import { TokenService } from './token.service';
import { UserService } from './user.service';
import { User } from '../models/user';
import { tap, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private auth$: BehaviorSubject<User>;

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(
    private httpClient: HttpClient,
    private userService: UserService,
    private tokenService: TokenService
  ) {
    this.auth$ = new BehaviorSubject<User>(null);
  }

  /**
   * @description Return an Observable of auth user
   * @returns {Observable.<User>} Observable of auth user
   */ 
  get auth(): Observable<User> {
    return this.auth$.asObservable();
  }

  /**
   * @description Return an Observable of auth user if user is logged
   * @returns {Observable.<(User|null)>} Observable of auth user
   */ 
  getAuthenticatedUser(): Observable<(User|null)> {
    if(this.isLogged()) {
      const decoded: any = this.tokenService.decode();
      return decoded.username ? this.userService.getUserByUsername(decoded.username)
      .pipe(tap(user => this.auth$.next(user)), catchError(error => of(null))) : null;
    }
    return of(null);
  }

  /**
   * @description Return an Observable with a token if login success
   * @param {Auth} auth
   * @returns {Observable.<Token>} Observable of token if credentials are valid  
   */  
  login(auth: Auth): Observable<Token> {
    return this.httpClient.post<Token>(`${this.endPoint}api/login_check`, auth);
  }

  /**
   * @description Check if an auth token exists
   * @returns {boolean} true if there is a token else false  
   */  
  isLogged(): boolean {
    return this.tokenService.token !== null;
  }

  /**
   * @description Check if the auth token has an admin role
   * @returns {boolean} true if there is a token with an admin role else false  
   */ 
  isAdmin(): boolean {
    const user = this.tokenService.decode()
    return user && !!user.roles.find(role => role === 'ROLE_ADMIN');
  }
}

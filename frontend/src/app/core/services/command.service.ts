import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Financial } from '../models/financial';
import { Courier } from '../models/courier';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Command, CommandForm } from '../models/command';
import { CommandEditionForm, CommandEdition } from '../models/command-edition';


@Injectable({
  providedIn: 'root'
})
export class CommandService {

  private courier$: BehaviorSubject<Courier>;
  private financial$: BehaviorSubject<Financial>;

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(
    private httpClient: HttpClient,
  ) {
    this.courier$ = new BehaviorSubject<Courier>(null);
    this.financial$ = new BehaviorSubject<Financial>(null);
  }

  /**
   * @description get a list of commands from backend
   * @returns {Observable.<Array.<Command>>} Observable of commands 
   */
  get commands(): Observable<Array<Command>> {
    return this.httpClient.get<Array<Command>>(`${this.endPoint}api/command`);
  }

  /**
   * @description get a command with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Command|null)>} Observable of a command if courier found by id else Observable of null
   */ 
  getCommand(id: number): Observable<(Command|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Command|null)>(`${this.endPoint}api/command/${parseInt(<any>id)}`);
  }

  /**
   * @description insert a new command
   * @param {CommandForm} command
   * @returns {Observable.<(Editor|*)>} Observable of the created command 
   */ 
  createCommand(command: CommandForm): Observable<(Command|any)> {
    return this.httpClient.post<(Command|any)>(`${this.endPoint}api/command`, command);
  }

  /**
   * @description edit a command found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @param {CommandForm} command
   * @returns {Observable.<(Command|*)>} Observable of the edited command 
   */ 
  editCommand(id: number, command: CommandForm): Observable<(Command|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.put<(Command|any)>(`${this.endPoint}api/command/${parseInt(<any>id)}`, command);
  }

  /**
   * @description delete a command found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<*>} Observable of delete result
   */ 
  deleteCommand(id: number): Observable<any> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.delete<any>(`${this.endPoint}api/edition/${parseInt(<any>id)}`);
  }

  /**
   * @description insert an edition in a command
   * @param {number} id != null && id > 0
   * @param {number} id_edition != null && id_edition > 0
   * @param {CommandEditionForm} commandEdition
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @throws {Error} if id_edition is null
   * @throws {Error} if id_edition < 1
   * @returns {Observable.<(Command|*)>} Observable of the created edition command 
   */ 
  addCommandEdition(id: number, id_edition: number, commandEdition: CommandEditionForm): Observable<(CommandEdition|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    if(typeof id_edition === "undefined" || id_edition == null) throw new Error(`edition id param should be not null`);
    if(parseInt(<any>id_edition) < 1) throw new Error(`edition id should be greater than 0`);
    return this.httpClient.patch<(CommandEdition|any)>(`${this.endPoint}api/command/${parseInt(<any>id)}/edition/${parseInt(<any>id_edition)}`, commandEdition);
  }

  /**
   * @description edit an edition command
   * @param {number} id != null && id > 0
   * @param {number} id_edition != null && id_edition > 0
   * @param {CommandEditionForm} commandEdition
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @throws {Error} if id_edition is null
   * @throws {Error} if id_edition < 1
   * @returns {Observable.<(Command|*)>} Observable of the edited edition command 
   */ 
  editCommandEdition(id: number, id_edition: number, commandEdition: CommandEditionForm): Observable<(CommandEdition|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    if(typeof id_edition === "undefined" || id_edition == null) throw new Error(`edition id param should be not null`);
    if(parseInt(<any>id_edition) < 1) throw new Error(`edition id should be greater than 0`);
    return this.httpClient.put<(CommandEdition|any)>(`${this.endPoint}api/command/${parseInt(<any>id)}/edition/${parseInt(<any>id_edition)}`, commandEdition);
  }

  /**
   * @description remove an edition command
   * @param {number} id != null && id > 0
   * @param {number} id_edition != null && id_edition > 0
   * @param {CommandEditionForm} commandEdition
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @throws {Error} if id_edition is null
   * @throws {Error} if id_edition < 1
   * @returns {Observable.<*>} Observable of remove result
   */ 
  removeCommandEdition(id: number, id_edition: number): Observable<any> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    if(typeof id_edition === "undefined" || id_edition == null) throw new Error(`edition id param should be not null`);
    if(parseInt(<any>id_edition) < 1) throw new Error(`edition id should be greater than 0`);
    return this.httpClient.delete<any>(`${this.endPoint}api/command/${parseInt(<any>id)}/edition/${parseInt(<any>id_edition)}`);
  }

  /**
   * @description return an observable with the selected courier
   * @returns {Observable.<Courier>}
   */
  get courier(): Observable<Courier> {
    return this.courier$.asObservable();
  }

  /**
   * @description return an observable with the selected financial
   * @returns {Observable.<Financial>}
   */
  get financial(): Observable<Financial> {
    return this.financial$.asObservable();
  }

  /**
   * Set a financial for command
   * @param {Financial} financial
   */
  setFinancial(financial: Financial) {
    this.financial$.next(financial);
  }

  /**
   * Set a courier for command
   * @param {Courier} courier
   */
  setCourier(courier: Courier) {
    this.courier$.next(courier);
  }

}

import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group } from '../models/group';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(private httpClient: HttpClient) { }

  /**
   * @description get a list of groups from backend
   * @returns {Observable.<Array.<Group>>} Observable of groups 
   */
  get groups(): Observable<Array<Group>> {
    return this.httpClient.get<Array<Group>>(`${this.endPoint}api/group`);
  }

  /**
   * @description get a group with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Group|null)>} Observable of a group if group found by id else Observable of null
   */ 
  getGroup(id: number): Observable<(Group|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Group|null)>(`${this.endPoint}api/group/${parseInt(<any>id)}`);
  }

  /**
   * @description get the default group for register
   * @returns {Observable.<Group>} Observable of default group
   */
  getDefaultGroup(): Observable<Group> {
    return this.httpClient.get<Group>(`${this.endPoint}api/group/GRP_USER`);
  }
}

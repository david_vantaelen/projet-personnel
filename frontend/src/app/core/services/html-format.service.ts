import { Injectable } from '@angular/core';

/**
 * @typedef {Object} TAGLIST
 * @property {string[]} begin
 * @property {string[]} end
 */

/**
 * @typedef {Object} REPLACEMENT
 * @property {string} replace
 * @property {string} by
 */

@Injectable({
  providedIn: 'root'
})
export class HtmlFormatService {

  /** @const {TAGLIST} TAGS */
  private readonly TAGS: {begin: string[], end: string[]} = {
    begin: ['\\[\\[', '\\]\\]'],
    end: ['\\[\\[/', '\\]\\]']
  };

  /** @const {REPLACEMENT[]} REPLACEMENTS */
  private readonly REPLACEMENTS: Array<{replace: string, by: string}> = [
    {replace: 'b', by: 'strong'},
    {replace: 'i', by: 'em'},
    {replace: 'p', by: 'p'},
    {replace: 'l', by: 'ul'},
    {replace: 'li', by: 'li'}
  ];

  /**
   * @description replace custom tags by html tags
   * @param {string} text
   * @returns {string}
   */
  format(text: string): string {
    const formatted = text.replace(/((\s*\n\s*)|(\s*\t\s*))/gs, "").trim();

    return this.REPLACEMENTS.reduce((acc, current) => {
      const pattern = new RegExp(this.TAGS.begin[0] + current.replace + this.TAGS.begin[1] + '(.*?)' + this.TAGS.end[0] + current.replace + this.TAGS.end[1], 'gims');
      // flags
      // g => global
      // i => insensitive
      // m => multiline
      // s => dotall
      while(pattern.test(acc)) 
        acc = acc.replace(pattern, '<' + current.by + '>$1</' + current.by + '>');
 
      return acc; 
    }, formatted).trim();

  }
}

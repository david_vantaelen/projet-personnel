import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { Edition } from '../models/edition';
import { ShopItem } from '../models/shop-item';
import { EditionService } from './edition.service';
import { Page } from '../models/page';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  /** @member {BehaviorSubject.<ShopItem[]>} shopcart$ */
  private shopCart$: BehaviorSubject<Array<ShopItem>>;

  constructor(
    private editionService: EditionService
  ) { 
    const shopCart = JSON.parse(localStorage.getItem("shop"));
    this.shopCart$ = new BehaviorSubject<Array<ShopItem>>(shopCart ? shopCart : []);
  }

  /**
   * @description get a list of editions from backend
   * @returns {Observable.<Array.<Edition>>} Observable of editions 
   */
  get catalog (): Observable<Array<Edition>> {
    return this.editionService.editions;
  }

  /**
   * @description get a page of editions
   * @param {number} [page=1] page > 0
   * @param {number} [perPage=20] perPage
   * @throws {Error} if page < 1
   * @returns {Observable.<Page.<Edition>>} Observable of page of editions
   */ 
  getPage (page: number = 1, perPage: number = 20): Observable<Page<Edition>> {
    return this.editionService.getPage(page, perPage);
  }

  /**
   * @description get an edition with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Edition|null)>} Observable of a edition if edition found by id else Observable of null
   */ 
  getOneFromCatalog (id: number): Observable<(Edition|null)> {
    return this.editionService.getEdition(id);
  }

  /**
   * @description get a list of editions by search value
   * @param {string} search
   * @returns {Observable.<Array.<Edition>>} Observable of editions
   */ 
  getCatalogBySearch (search: string): Observable<Array<Edition>> {
    return this.editionService.getEditionBySearch(search);
  }

  /**
   * @description get a page of editions by search value
   * @param {string} search
   * @param {number} [page=1] page > 0
   * @param {number} [perPage=20] perPage
   * @throws {Error} if page < 1
   * @returns {Observable.<Page.<Edition>>} Observable of page of editions
   */ 
  getPageBySearch (search: string, page: number = 1, perPage: number = 20): Observable<Page<Edition>> {
    return this.editionService.getPageBySearch(search, page, perPage);
  }

  fromStorage() {
    const shopCart = JSON.parse(localStorage.getItem("shop"));
    this.shopCart$.next(shopCart ? shopCart : []);
  }

  /**
   * @returns {Observable.<Array.<ShopItem>>} Observable of shop items 
   */
  get shopcart (): Observable<Array<ShopItem>> {
    return this.shopCart$.asObservable();
  }

  /**
   * @param {ShopItem[]} values 
   */
  setShopCart (values: Array<ShopItem>) {
    localStorage.setItem("shop", JSON.stringify(values));
    this.shopCart$.next(values);
  }

  /**
   * @param {ShopItem} item 
   */
  addToShopCart (item: ShopItem) {
    let values = this.shopCart$.getValue();

    if(values.find(e => e.id === item.id))
      values = values.map(e => e.id === item.id && e.quantity < item.stock ? {...e, quantity: e.quantity + 1 } : e);
    else if(item.stock) values = [...values, item];
    
    localStorage.setItem("shop", JSON.stringify(values));
    this.shopCart$.next(values);
  }

  /**
   * @param {ShopItem} item 
   */
  removeFromShopCart (item: ShopItem) {
    let values = this.shopCart$.getValue();
    let toRemove = values.find(e => e.id === item.id);
    if(toRemove){
      if(toRemove.quantity > 1) values = values.map(e => e.id === item.id ? {...e, quantity: e.quantity - 1} : e);
      else values = values.filter(e => e.id !== item.id);
      
      localStorage.setItem("shop", JSON.stringify(values));
      this.shopCart$.next(values);
    } 
  }

  /**
   * @param {ShopItem} item 
   */
  removeAllFromShopCart (item: ShopItem) {
    let values = this.shopCart$.getValue();
    if(values.find(e => e.id === item.id)){
      values = values.filter(e => e.id !== item.id);
      localStorage.setItem("shop", JSON.stringify(values));
      this.shopCart$.next(values);
    } 
  }
}



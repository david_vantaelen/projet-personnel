import { Injectable } from '@angular/core';
import { CollectionForm, Collection } from '../models/collection';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  private readonly endPoint: string = environment.domain;

  constructor(private httpClient: HttpClient) { }

  /**
   * @description get a list of collections from backend
   * @returns {Observable.<Array.<Collection>>} Observable of collections 
   */
  get collections(): Observable<Array<Collection>> {
    return this.httpClient.get<Array<Collection>>(`${this.endPoint}api/collection`);
  }

  /**
   * @description get a collection with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Collection|null)>} Observable of a collection if collection found by id else Observable of null
   */ 
  getCollection(id: number): Observable<(Collection|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Collection|null)>(`${this.endPoint}api/collection/${parseInt(<any>id)}`);
  }

  /**
   * @description insert a new collection
   * @param {CollectionForm} collection
   * @returns {Observable.<(Collection|*)>} Observable of the created collection 
   */ 
  createCollection(collection: CollectionForm): Observable<(Collection|any)> {
    return this.httpClient.post<(Collection|any)>(`${this.endPoint}api/collection`, collection);
  }

  /**
   * @description edit a collection found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @param {CollectionForm} collection
   * @returns {Observable.<(Collection|*)>} Observable of the edited collection 
   */ 
  editCollection(id: number, collection: CollectionForm): Observable<(Collection|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.put<(Collection|any)>(`${this.endPoint}api/collection/${parseInt(<any>id)}`, collection);
  }

  /**
   * @description delete a collection found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<*>} Observable of delete result
   */ 
  deleteCollection(id: number): Observable<any> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.delete<any>(`${this.endPoint}api/collection/${parseInt(<any>id)}`);
  }
}

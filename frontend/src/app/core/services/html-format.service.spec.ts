import { TestBed } from '@angular/core/testing';

import { HtmlFormatService } from './html-format.service';

describe('HtmlFormatService', () => {
  let service: HtmlFormatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HtmlFormatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should render formatted html', () => {
    let transformed = service.format("[[p]]test[[/p]]");
    expect(transformed).toBe('<p>test</p>');
  });

  it('should render formatted html with bad entry', () => {
    let transformed = service.format("[[p]]test [[b]]with bad[[/p]] entry[[/b]]");
    expect(transformed).toBe('<p>test <strong>with bad</p> entry</strong>');
  });

  it('should render formatted html with multiple p tags', () => {
    let transformed = service.format("[[p]]test [[/p]]with multi[[p]] p[[/p]]");
    expect(transformed).toBe('<p>test </p>with multi<p> p</p>');
  });

  it('should render formatted html with multiple strong tags inside each strong tag', () => {
    let transformed = service.format("[[b]]test [[b]]with multi[[/b]] strong[[/b]]");
    expect(transformed).toBe('<strong>test <strong>with multi</strong> strong</strong>');
  });

  it('should render formatted html with a list in multiline', () => {
    let transformed = service.format(`[[l]]
    
    [[li]]test [[b]]1[[/b]][[/li]]
    
      [[li]]test [[i]]2[[/i]][[/li]]
    [[/l]]`);

    expect(transformed).toBe('<ul><li>test <strong>1</strong></li><li>test <em>2</em></li></ul>');
  });
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User, UserForm } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { Address } from '../models/address';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(private httpClient: HttpClient) { }
  
  /**
   * @description get a list of users from backend
   * @returns {Observable.<Array.<User>>} Observable of users 
   */
  get users(): Observable<Array<User>> {
    return this.httpClient.get<Array<User>>(`${this.endPoint}api/user`);
  }

  /**
   * @description get a user with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(User|null)>} Observable of a user if user found by id else Observable of null
   */
  getUser(id: number): Observable<(User|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(User|null)>(`${this.endPoint}api/user/${parseInt(<any>id)}`)
    .pipe(map((user: User) => {
      const main = user.addresses.reduce((acc: Address|null, curr: Address) =>  curr.is_main ? curr : acc)
      user.main_address = main;
      return user;
    }));
  }

  /**
   * @description get a user with a username from backend
   * @param {string} username != null && username !== ''
   * @throws {Error} if username is null
   * @throws {Error} if username is empty
   * @returns {Observable.<(User|null)>} Observable of a user if user found by id else Observable of null
   */
  getUserByUsername(username: string): Observable<(User|null)> {
    if(typeof username === "undefined" || username == null) throw new Error(`username param should be not null`);
    if(username.trim() === '') throw new Error(`username shouldn't be empty`);
    return this.httpClient.get<(User|null)>(`${this.endPoint}api/username/${username.trim()}`)
    .pipe(catchError(error => of(null)), map((user: User) => {
      const main = user.addresses.reduce((acc: Address|null, curr: Address) =>  curr.is_main ? curr : acc)
      user.main_address = main;
      return user;
    }));
  }

  /**
   * @description get a user with an email from backend
   * @param {string} email != null && email !== ''
   * @throws {Error} if email is null
   * @throws {Error} if email is empty
   * @returns {Observable.<(User|null)>} Observable of a user if user found by id else Observable of null
   */
  getUserByEmail(email: string): Observable<(User|null)> {
    if(typeof email === "undefined" || email == null) throw new Error(`email param should be not null`);
    if(email.trim() === '') throw new Error(`email shouldn't be empty`);
    return this.httpClient.get<(User|null)>(`${this.endPoint}api/email/${email.trim()}`)
    .pipe(catchError(error => of(null)), map((user: User) => {
      const main = user.addresses.reduce((acc: Address|null, curr: Address) =>  curr.is_main ? curr : acc)
      user.main_address = main;
      return user;
    }));
  }

  /**
   * @description insert a new user
   * @param {UserForm} user
   * @returns {Observable.<(User|*)>} Observable of the created user 
   */ 
  createUser(user: UserForm): Observable<(User|any)> {
    return this.httpClient.post<(User|any)>(`${this.endPoint}api/user`, user);
  }

  /**
   * @description edit a user found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @param {UserForm} user
   * @returns {Observable.<(User|*)>} Observable of the edited user 
   */ 
  editUser(id: number, user: UserForm): Observable<(User|any)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    if(user.password === null) delete user.password;
    return this.httpClient.put<(User|any)>(`${this.endPoint}api/user/${parseInt(<any>id)}`, user);
  }

  /**
   * @description check if email exists in database
   * @param {string} email
   * @returns {Observable.<boolean>} Observable of true if email exists
   */
  hasEmail(email: string): Observable<boolean> {
    return this.httpClient.get<any[]>(`${this.endPoint}api/has_email/${email}`)
    .pipe(map((data: any) => data.email));
  }

  /**
   * @description check if username exists in database
   * @param {string} username
   * @returns {Observable.<boolean>} Observable of true if username exists
   */
  hasUsername(username: string): Observable<boolean> {
    return this.httpClient.get<any[]>(`${this.endPoint}api/has_username/${username}`)
    .pipe(map((data: any) => data.username));
  }

  /**
   * @description delete a user found by id
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<*>} Observable of delete result
   */ 
  deleteUser(id: number): Observable<any> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.delete<any>(`${this.endPoint}api/user/${parseInt(<any>id)}`);
  }
}

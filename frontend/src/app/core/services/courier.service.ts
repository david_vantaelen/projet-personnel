import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Courier } from '../models/courier';

@Injectable({
  providedIn: 'root'
})
export class CourierService {

  /** @const {string} endPoint */
  private readonly endPoint: string = environment.domain;

  constructor(private httpClient: HttpClient) { }

  /**
   * @description get a list of couriers from backend
   * @returns {Observable.<Array.<Courier>>} Observable of couriers 
   */
  get couriers(): Observable<Array<Courier>> {
    return this.httpClient.get<Array<Courier>>(`${this.endPoint}api/courier`);
  }

  /**
   * @description get a courier with an id from backend
   * @param {number} id != null && id > 0
   * @throws {Error} if id is null
   * @throws {Error} if id < 1
   * @returns {Observable.<(Courier|null)>} Observable of a courier if courier found by id else Observable of null
   */ 
  getCourier(id: number): Observable<(Courier|null)> {
    if(typeof id === "undefined" || id == null) throw new Error(`id param should be not null`);
    if(parseInt(<any>id) < 1) throw new Error(`id should be greater than 0`);
    return this.httpClient.get<(Courier|null)>(`${this.endPoint}api/courier/${parseInt(<any>id)}`);
  }
}

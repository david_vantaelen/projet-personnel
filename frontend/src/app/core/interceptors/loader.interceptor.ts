import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { NbDialogService } from '@nebular/theme';
import { finalize } from 'rxjs/operators';
import { LoaderService } from '../services/loader.service';
import { LoaderComponent } from 'src/app/shared/components/loader/loader.component';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(
    private loaderService: LoaderService,
    private nbDialogService: NbDialogService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    this.loaderService.load();
    return next.handle(request).pipe(finalize(() => setTimeout(() => this.loaderService.unload(), 500)));
    // const ref = this.nbDialogService.open(LoaderComponent, { closeOnBackdropClick: false });
    // return next.handle(request).pipe(finalize(() => ref.close()));
  }
}
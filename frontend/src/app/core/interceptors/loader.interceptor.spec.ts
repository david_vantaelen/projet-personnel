import { TestBed } from '@angular/core/testing';

import { LoaderInterceptor } from './loader.interceptor';
import { NbDialogModule, NbDialogService } from '@nebular/theme';

describe('LoaderInterceptor', () => {

  const spyNbDialogService = jasmine.createSpyObj('spyNbDialogService', ['open', 'close']);
  spyNbDialogService.open.and.returnValue(null);
  spyNbDialogService.close.and.returnValue(null);

  beforeEach(() => TestBed.configureTestingModule({
    imports: [NbDialogModule],
    providers: [
      LoaderInterceptor,
      { provide: NbDialogService, useValue: spyNbDialogService}
      
      ]
  }));

  it('should be created', () => {
    const interceptor: LoaderInterceptor = TestBed.inject(LoaderInterceptor);
    expect(interceptor).toBeTruthy();
  });
});

import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorService } from '../services/author.service';
import { Author } from '../models/author';

@Injectable({
    providedIn: 'root'
})
export class GetAuthorsResolver implements Resolve<Author[]>{
    public constructor(private authorService: AuthorService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Author[] | Observable<Author[]> | Promise<Author[]> {
            return this.authorService.authors;
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetOneAuthorResolver implements Resolve<Author>{
    public constructor(private authorService: AuthorService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Author | Observable<Author> | Promise<Author> {
            const param: any = route.paramMap.get('id');
            return this.authorService.getAuthor(<number>param);
        
    }
}
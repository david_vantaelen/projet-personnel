import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { CatalogService } from '../services/catalog.service';
import { Edition } from '../models/edition';
import { Page } from '../models/page';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class GetEditionsResolver implements Resolve<Edition[]>{
    public constructor(private catalogService: CatalogService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Edition[] | Observable<Edition[]> | Promise<Edition[]> {
            return this.catalogService.catalog
            .pipe(catchError(error => of(null)));
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetEditionsBySearchResolver implements Resolve<Edition[]>{
    public constructor(private catalogService: CatalogService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Edition[] | Observable<Edition[]> | Promise<Edition[]> {
            const param: any = route.paramMap.get('search');
            return this.catalogService
            .getCatalogBySearch(param).pipe(catchError(error => of(null)));
    } 
}


@Injectable({
    providedIn: 'root'
})
export class GetPageOfEditionsResolver implements Resolve<Page<Edition>>{
    public constructor(private catalogService: CatalogService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Page<Edition> | Observable<Page<Edition>> | Promise<Page<Edition>> {
            const page: any = route.paramMap.get('page');
            return this.catalogService
            .getPage(parseInt(<any>page), environment.perPage).pipe(catchError(error => of(null)));
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetPageOfEditionsBySearchResolver implements Resolve<Page<Edition>>{
    public constructor(private catalogService: CatalogService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Page<Edition> | Observable<Page<Edition>> | Promise<Page<Edition>> {
            const search: any = route.paramMap.get('search');
            const page: any = route.paramMap.get('page');
            return this.catalogService
            .getPageBySearch(search, parseInt(<any>page), environment.perPage).pipe(catchError(error => of(null)));
    } 
}


@Injectable({
    providedIn: 'root'
})
export class GetOneEditionResolver implements Resolve<Edition>{
    public constructor(private catalogService: CatalogService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Edition | Observable<Edition> | Promise<Edition> {
            const param: any = route.paramMap.get('id');
            return this.catalogService.getOneFromCatalog(<number>param);
        
    }
}
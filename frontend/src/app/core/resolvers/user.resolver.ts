import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Injectable({
    providedIn: 'root'
})
export class GetUsersResolver implements Resolve<User[]>{
    public constructor(private userService: UserService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): User[] | Observable<User[]> | Promise<User[]> {
            return this.userService.users;
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetOneUserResolver implements Resolve<User>{
    public constructor(private userService: UserService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): User | Observable<User> | Promise<User> {
            const param: any = route.paramMap.get('id');
            return this.userService.getUser(<number>param);
        
    }
}
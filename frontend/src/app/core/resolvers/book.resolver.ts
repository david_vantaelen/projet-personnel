import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { BookService } from '../services/book.service';
import { Book } from '../models/book';

@Injectable({
    providedIn: 'root'
})
export class GetBooksResolver implements Resolve<Book[]>{
    public constructor(private bookService: BookService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Book[] | Observable<Book[]> | Promise<Book[]> {
            return this.bookService.books;
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetOneBookResolver implements Resolve<Book>{
    public constructor(private bookService: BookService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Book | Observable<Book> | Promise<Book> {
            const param: any = route.paramMap.get('id');
            return this.bookService.getBook(<number>param);
        
    }
}
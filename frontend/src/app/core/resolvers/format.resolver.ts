import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Format } from '../models/format';
import { FormatService } from '../services/format.service';

@Injectable({
    providedIn: 'root'
})
export class GetFormatsResolver implements Resolve<Format[]>{
    public constructor(private formatService: FormatService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Format[] | Observable<Format[]> | Promise<Format[]> {
            return this.formatService.formats;
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetOneFormatResolver implements Resolve<Format>{
    public constructor(private formatService: FormatService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Format | Observable<Format> | Promise<Format> {
            const param: any = route.paramMap.get('id');
            return this.formatService.getFormat(<number>param);
        
    }
}
import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { GroupService } from '../services/group.service';
import { Group } from '../models/group';

@Injectable({
    providedIn: 'root'
})
export class GetGroupsResolver implements Resolve<Group[]>{
    public constructor(private groupService: GroupService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Group[] | Observable<Group[]> | Promise<Group[]> {
            return this.groupService.groups;
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetOneGroupResolver implements Resolve<Group>{
    public constructor(private groupService: GroupService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Group | Observable<Group> | Promise<Group> {
            const param: any = route.paramMap.get('id');
            return this.groupService.getGroup(<number>param);
        
    }
}
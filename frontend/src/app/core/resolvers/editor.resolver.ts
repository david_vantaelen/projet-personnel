import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Editor } from '../models/editor';
import { EditorService } from '../services/editor.service';

@Injectable({
    providedIn: 'root'
})
export class GetEditorsResolver implements Resolve<Editor[]>{
    public constructor(private editorService: EditorService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Editor[] | Observable<Editor[]> | Promise<Editor[]> {
            return this.editorService.editors;
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetOneEditorResolver implements Resolve<Editor>{
    public constructor(private editorService: EditorService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Editor | Observable<Editor> | Promise<Editor> {
            const param: any = route.paramMap.get('id');
            return this.editorService.getEditor(<number>param);
        
    }
}
import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Courier } from '../models/courier';
import { CourierService } from '../services/courier.service';

@Injectable({
    providedIn: 'root'
})
export class GetCouriersResolver implements Resolve<Courier[]>{
    public constructor(private courierService: CourierService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Courier[] | Observable<Courier[]> | Promise<Courier[]> {
            return this.courierService.couriers;
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetOneCourierResolver implements Resolve<Courier>{
    public constructor(private courierService: CourierService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Courier | Observable<Courier> | Promise<Courier> {
            const param: any = route.paramMap.get('id');
            return this.courierService.getCourier(<number>param);
        
    }
}
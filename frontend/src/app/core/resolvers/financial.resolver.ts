import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Financial } from '../models/financial';
import { FinancialService } from '../services/financial.service';

@Injectable({
    providedIn: 'root'
})
export class GetFinancialsResolver implements Resolve<Financial[]>{
    public constructor(private financialService: FinancialService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Financial[] | Observable<Financial[]> | Promise<Financial[]> {
            return this.financialService.financials;
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetOneFinancialResolver implements Resolve<Financial>{
    public constructor(private financialService: FinancialService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Financial | Observable<Financial> | Promise<Financial> {
            const param: any = route.paramMap.get('id');
            return this.financialService.getFinancial(<number>param);
        
    }
}
import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Collection } from '../models/collection';
import { CollectionService } from '../services/collection.service';

@Injectable({
    providedIn: 'root'
})
export class GetCollectionsResolver implements Resolve<Collection[]>{
    public constructor(private collectionService: CollectionService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Collection[] | Observable<Collection[]> | Promise<Collection[]> {
            return this.collectionService.collections;
        
    } 
}

@Injectable({
    providedIn: 'root'
})
export class GetOneCollectionResolver implements Resolve<Collection>{
    public constructor(private collectionService: CollectionService){}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Collection | Observable<Collection> | Promise<Collection> {
            const param: any = route.paramMap.get('id');
            return this.collectionService.getCollection(<number>param);
        
    }
}